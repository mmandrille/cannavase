#Imports de python

#Import Standard de Django
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse, HttpResponse
from django.forms import inlineformset_factory
#Import Personales
from django.views.generic import CreateView

from .models import Noticia, Foto
from .forms import SearchForm, NoticiaForm, FotoForm, NoticiaFotoForm
from core.functions import is_related

# Create your views here.
from core.models import Home


def ver_noticias(request):
    mostrar = Home.objects.get(id=1)
    etiquetas = Noticia.etiquetas.most_common()[:5]
    noticias = Noticia.objects.all().order_by('-fecha')[:10]
    return render(request, 'noticias.html', { 'mostrar': mostrar,
                                              'noticias': noticias,
                                              'form': SearchForm(),
                                              'boton': 'Buscar',
                                              'etiquetas': etiquetas, })

def buscar_noticias(request):
    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            buscar = request.POST['buscar']
            noticias = Noticia.objects.all()
            noticias = noticias.filter(titulo__icontains=buscar)
            return render(request, 'noticias.html', { 'noticias': noticias, 'form': form, })
    #Si no siempre devolvemos la normal
    return ver_noticias(request)

def buscar_etiqueta(request, tag_id):
    etiquetas = Noticia.etiquetas.most_common()[:5]
    noticias = Noticia.objects.filter(etiquetas__id=tag_id)[:10]
    return render(request, 'noticias.html', { 'noticias': noticias, 'form': SearchForm(), 'boton': 'Buscar', 'etiquetas': etiquetas, })

def ver_noticia(request, noticia_id):
    mostrar = Home.objects.get(id=1)
    noticia = Noticia.objects.get(pk=noticia_id)
    return render(request, 'noticia.html', {'mostrar': mostrar, 'noticia': noticia, 'form': SearchForm(), })


#Vistas del menu del Administrador
@permission_required('operadores.wservices')
def menu(request):
    return redirect('core:menu_opciones')

@permission_required('operadores.wservices')
def lista_noticias(request):
    noticias = Noticia.objects.all().order_by('-id')
    return render(request, 'lista_noticias.html', {
        'noticias': noticias,
        'has_table': True,
    })

@permission_required('operadores.wservices')
def crear_noticias(request, noticia_id=None):
    noticia = None
    if noticia_id:
        noticia = Noticia.objects.get(pk=noticia_id)
        form = NoticiaForm(instance=noticia)
        FotoFormset = inlineformset_factory(Noticia, Foto, 
                                            fields=('tipo', 'titulo', 'epigrafe', 'archivo'),
                                            extra=2)
        formset = FotoFormset(instance=form.instance)
        if request.method == 'POST':
            form = NoticiaForm(request.POST, request.FILES, instance=noticia)
            if request.FILES:
                formset = FotoFormset(request.POST, request.FILES, instance=form.instance)
                if form.is_valid():
                    form.save()
                    formset.save()
                    return redirect('noticias:lista_noticias')
            else:
                formset = FotoFormset(request.POST, instance=form.instance)
                if form.is_valid():
                    form.save()
                    for i in range(len(formset)):
                        if formset[i]['DELETE'].data == True:
                            foto = Foto.objects.filter(noticia=noticia_id, titulo=formset[i]['titulo'].data)
                            foto.delete()
                    formset.save()
                    return redirect('noticias:lista_noticias')
    else:
        form = NoticiaForm(instance=noticia)
        FotoFormset = inlineformset_factory(Noticia, Foto, 
                                            fields=('tipo', 'titulo', 'epigrafe', 'archivo'),
                                            extra=2)
        formset = FotoFormset()
        if request.method == 'POST':
            form = NoticiaForm(request.POST, request.FILES, instance=noticia)
            formset = FotoFormset(request.POST, request.FILES, instance=form.instance)
            if form.is_valid() and formset.is_valid():
                form.save()
                formset.save()
                return redirect('noticias:lista_noticias')
    return render(request, "extras/noticia-form.html", {'form': form,'formset': formset,'agregar': 'Agregar', 'boton': 'Guardar'})


@permission_required('operadores.wservices')
def delete_noticias(request, noticia_id):
    noticia = Noticia.objects.get(pk=noticia_id)
    if is_related(noticia):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Noticia',
            'error': "La noticia no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        if request.method == 'POST':
            noticia.delete()
            return redirect('noticias:lista_noticias')
        return render(request, 'extras/confirmar.html', {'titulo': 'Eliminar Noticia',
                                                         'message': 'Esta acción eliminará la Noticia seleccionada.'+'<br>'+
                                                                    'Se recomienda desactivar la misma.',
                                                         'has_form': True})

