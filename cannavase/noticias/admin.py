
#Import Standards
from django.contrib import admin
#Incluimos modelos
from .models import Noticia, Foto

# Register your models here.
class FotoInline(admin.TabularInline):
    model = Foto
    insert_after = 'copete'
    extra = 0

class NoticiaAdmin(admin.ModelAdmin):
    model = Noticia
    list_filter = ['destacada', 'activa', 'etiquetas']
    ordering = ['fecha']
    search_fields = ['titulo', 'copete']
    inlines = [FotoInline]
    #Para poder cambiar el orden de los campos:
    change_form_template = 'admin/custom/change_form.html'
    class Media:
        css = {
            'all': (
                'css/admin.css',
            )
        }
#Los mandamos al admin
admin.site.register(Noticia, NoticiaAdmin)