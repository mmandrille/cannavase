from __future__ import unicode_literals
#Imports Standard de python
#Import standard de Djagno
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User

#Import Extras
from tinymce.models import HTMLField
from taggit.managers import TaggableManager
from embed_video.fields import EmbedVideoField

#Imports de choices
from .choices import TIPO_FOTO

#Create your models here.
class Noticia(models.Model):
    titulo = models.CharField('Titulo', max_length=200)
    copete = models.CharField('Copete', max_length=400)
    video_online = EmbedVideoField(blank=True, null=True)
    video_archivo = models.FileField(upload_to='videos/', blank=True, null=True)
    etiquetas = TaggableManager()
    cuerpo = HTMLField(null=True, blank=True, default='')
    fuente = models.URLField('Url a la Noticia Fuente', null=True, blank=True)
    fecha = models.DateTimeField('Fecha', default=timezone.now)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    destacada = models.BooleanField(default=False)
    activa = models.BooleanField(default=False)
    def __str__(self):
        return self.titulo
    def as_dict(self):
        return {
            'id': self.id,
            'titulo':  self.titulo,
            'portada': str(self.portada),
            'epigrafe': self.epigrafe,
            'etiquetas': str(self.etiquetas.all()),
            'cuerpo': self.cuerpo,
            'fecha': str(self.fecha),
            'autor': str(self.autor),
            'destacada': self.destacada,
            'video_archivo': self.video_archivo,
            'video_online': self.video_online,
        }
    def get_portada(self):
        if self.fotos.filter(tipo='P').first():
            return self.fotos.filter(tipo='P').first().archivo.url
        else:
            if self.fotos.first():
                return self.fotos.first().archivo.url
            else:
                return None


class Foto(models.Model):
    noticia = models.ForeignKey(Noticia, related_name="fotos", null=True, on_delete=models.CASCADE)
    tipo = models.CharField('Tipo', max_length=1, choices=TIPO_FOTO)
    titulo = models.CharField('Titulo', max_length=50)
    epigrafe = models.CharField('epigrafe', default='imagen' ,max_length=100, blank=True)
    archivo = models.ImageField(upload_to='novedades/', default='/archivos/defaults/noimage.gif')

