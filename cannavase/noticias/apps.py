from django.apps import AppConfig

# Imports del Proyecto
from core.functions import agregar_menu


class NoticiasConfig(AppConfig):
    name = 'noticias'
    verbose_name = "Comunicación"

    def ready(self):
        agregar_menu(self)
