from django.urls import path
#Import de modulos personales
from . import views

app_name = 'noticias'
urlpatterns = [
    path('public', views.ver_noticias, name='ver_noticias'),
    path('public/<int:noticia_id>', views.ver_noticia, name='ver_noticia'),

    #Utilidades
    path('public/buscar', views.buscar_noticias, name='buscar_noticias'),
    path('public/tags/<int:tag_id>', views.buscar_etiqueta, name='buscar_etiqueta'),

    #Administracion
    path('', views.menu, name='menu_comunicaciones'),
    path('lista', views.lista_noticias, name='lista_noticias'),
    path('crear', views.crear_noticias, name='crear_noticias'),
    path('mod/<int:noticia_id>', views.crear_noticias, name='mod_noticias'),
    path('del/<int:noticia_id>', views.delete_noticias, name='delete_noticias'),
]