# Import Standard de django
from django import forms
from betterforms.multiform import MultiModelForm
# imports del proyecto
from .models import Noticia, Foto
from core.models import Home


# Definimos forms
class SearchForm(forms.Form):
    buscar = forms.CharField(label="", required=True)


class NoticiaForm(forms.ModelForm):
    class Meta:
        model = Noticia
        fields = '__all__'


class FotoForm(forms.ModelForm):
    class Meta:
        model = Foto
        fields = ['tipo', 'titulo', 'epigrafe', 'archivo']

    def __init__(self, *args, **kwargs):
        super(FotoForm, self).__init__(*args, **kwargs)
        self.fields['archivo'].widget.attrs['multiple'] = True
        if self.instance.pk:
            self.fields['noticia'].widget.attrs.update()


class NoticiaFotoForm(MultiModelForm):
    form_classes = {
        'nota': NoticiaForm,
        'imagen': FotoForm,
    }


class HomeForm(forms.ModelForm):
    class Meta:
        model = Home
        fields = '__all__'
