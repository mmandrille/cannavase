#Choice Fields
TIPO_EVENTO = (
    ('I', 'Ingreso'),
    ('E', 'Egreso'),
)

NIVELES_SEGURIDAD = (
    ('B', 'Blanco - Proveedores y Externos'),
    ('V', 'Verde - Personal de Servicio'),
    ('A', 'Amarillo - Tecnicos y Administrativos'),
    ('R', 'Rojo - Personal Jerarquico'),
)