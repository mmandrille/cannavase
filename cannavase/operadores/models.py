#Imports de python
import qrcode
#Realizamos imports de Django
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
#Imports de paquetes extras
from auditlog.registry import auditlog
from tinymce.models import HTMLField
#Imports del proyecto:
from cannavase.settings import MEDIA_ROOT, LOADDATA
from cannavase.constantes import NOIMAGE
from core.choices import TIPO_DOCUMENTOS
#improts de la app
from .choices import NIVELES_SEGURIDAD

# Create your models here.
class Operador(models.Model):
    nivel_acceso = models.CharField("Acceso de Seguridad", max_length=1, choices=NIVELES_SEGURIDAD, default='B')
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="operador")
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.CharField('Numero de Documento', unique=True, max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres  = models.CharField('Nombres', max_length=100)
    email = models.EmailField('Correo Electronico')
    telefono = models.CharField('Telefono', max_length=20, default='+549388')
    fotografia = models.FileField('Fotografia', upload_to='operadores/', null=True, blank=True)
    qrpath = models.CharField('qrpath', max_length=100, null=True, blank=True)
    class Meta:
        verbose_name_plural = 'Operadores'
        ordering = ['apellidos', 'nombres']
        permissions = (
            #Operadores:
            ("operador_admin", "Administrar de Operadores."),
            #Georef
            ("georef_admin", "Administrar GeoRefs"),
            #Inventario
            ("inventario_admin", "Administrar Inventario"),
            #Produccion
            ("produccion", "Operador de Cultivo"),
            ("produccion_admin", "Encargado de Trazabilidad de Cultivo"),
            #Reportes
            ("reportes", "Puede Obtener Reportes"),
            #Wservices
            ("wservices", "WebServices del Sistema"),
            #Comunicacion:
            ("comunicacion", "Administrador de Comunicaciones"),
        )
    def __str__(self):
        return self.apellidos + ', ' + self.nombres
    def get_foto(self):
        if self.fotografia:
            return self.fotografia.url
        else:
            return NOIMAGE
    def get_qrimage(self):
        if self.qrpath:
            return self.qrpath
        else:
            path = MEDIA_ROOT + '/operadores/qrcode-'+ str(self.num_doc)
            img = qrcode.make(str(self.num_doc))
            img.save(path)
            relative_path = '/archivos/operadores/qrcode-'+ str(self.num_doc)
            self.qrpath = relative_path
            self.save()
            return self.qrpath

if not LOADDATA:
    #Auditoria
    auditlog.register(Operador)
    auditlog.register(User)
