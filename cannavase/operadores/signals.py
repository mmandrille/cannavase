#Imports Python
import logging
#Imports Django
from django.dispatch import receiver
from django.db.models.signals import post_save
#imports Extras
#Imports del proyecto
#Imports de la app
from .models import Operador

#Logger
logger = logging.getLogger('signals')

#Definimos nuestra señales
