##Imports Django
from django.db import models
#Import Extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Imports del proyeecto
from cannavase.settings import LOADDATA
#Imports de app
from .validators import validate_file_extension
from .choices import TIPO_ALIANZA, NACIONALIDADES

# Create your models here.
class Video(models.Model):
    titulo = models.CharField('Titulo', max_length=100)
    descripcion = HTMLField(blank=True)
    archivo = models.FileField(upload_to='videos/', validators=[validate_file_extension])
    fecha = models.DateTimeField(auto_now_add=True)
    actual = models.BooleanField('Actual', default=False)
    def __str__(self):
        return self.titulo + ':' + str(self.actual)

class Director(models.Model):
    nombres = models.CharField('Nombres', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    foto = models.FileField(upload_to='directores/')
    orden = models.IntegerField(unique=True)
    cargo = models.CharField('Titulo', max_length=100)
    email = email = models.EmailField('Correo Electronico')
    linkedin = models.URLField('Direccion de Linkedin')
    descripcion_high = HTMLField(blank=True)
    descripcion_low = HTMLField(blank=True)
    activo = models.BooleanField('Activa', default=False)
    def __str__(self):
        return self.nombres + ' ' + self.apellidos

class Alianza(models.Model):
    tipo = models.IntegerField(choices=TIPO_ALIANZA, default=1)
    orden = models.IntegerField(unique=True)
    nombre = models.CharField('Nombre', max_length=100)
    nacionalidad = models.IntegerField(choices=NACIONALIDADES, default=200)
    descripcion = HTMLField(blank=True)
    logo = models.FileField(upload_to='alianzas/')
    activa = models.BooleanField('Activa', default=False)
    def __str__(self):
        return self.nombre

class Home(models.Model):
    noticia = models.BooleanField('noticias', default=False)
    fecha = models.DateTimeField(auto_now_add=True)

if not LOADDATA:
    #Auditoria
    auditlog.register(Video)
    auditlog.register(Director)
    auditlog.register(Alianza)
    auditlog.register(Home)