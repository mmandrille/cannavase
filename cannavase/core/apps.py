from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    ADMIN_MENU = [('Semillas', 'cultivo/semilla')]
    ADMIN_MODELS = {}
    def ready(self):
        #Background Jobs
        #signals
        pass
