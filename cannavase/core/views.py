#Imports Django
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from noticias.models import Noticia, Foto
from noticias.forms import HomeForm
#Imports de la app
from .apps import CoreConfig
from .choices import TIPO_ALIANZA, NACIONALIDADES
from .models import Video, Alianza, Director, Home
from .functions import get_home_config
from .forms import AlianzaForm, DirectivoForm, VideoForm

# Create your views here.

def home(request):
    video = Video.objects.filter(actual=True).order_by('-fecha').first()
    alianzas = Alianza.objects.filter(activa=True)
    noticias = Noticia.objects.all().filter(destacada=True).order_by("-fecha")[:3]
    return render(request, 'home.html', {
        'video':video,
        'alianzas': alianzas,
        'noticias': noticias,
        'mostrar': get_home_config(),
        }
    )

def cooperamos(request):
    mostrar = Home.objects.last()
    return render(request, 'cooperamos.html', {
        'mostrar': get_home_config(),
        }
    )

def hacemos(request):
    mostrar = Home.objects.last()
    return render(request, 'hacemos.html', {
        'mostrar': get_home_config(),
        }
    )

def somos(request):
    mostrar = Home.objects.get(id=1)
    categorias = []
    for tipo in TIPO_ALIANZA:
        if Alianza.objects.filter(tipo=tipo[0], activa=True).exists():
            categorias.append(tipo)
    alianzas = Alianza.objects.filter(activa=True)
    directores = Director.objects.filter(activo=True).order_by('orden')
    return render(request, 'somos.html', {
        'mostrar': mostrar,
        'alianzas': alianzas,
        'categorias': categorias,
        'directores': directores
        }
    )

def contactos(request):
    mostrar = Home.objects.get(id=1)
    return render(request, 'contactos.html', {'mostrar':mostrar})

#PRIVADO
@staff_member_required
def menu(request):
    return render(request, 'menu_principal.html', {
        'opciones': CoreConfig.ADMIN_MENU,
    })

@permission_required('operadores.wservices')
def mostrar(request):
    mostrar = get_home_config()
    form = HomeForm(instance=mostrar)
    if request.method == 'POST':
        form = HomeForm(request.POST, instance=mostrar)
        if form.is_valid():
            form.save()
            return redirect('noticias:menu_comunicaciones')
    return render(request, "extras/generic_form.html", {
        'titulo': "Opciones del Home",
        'form': form,
        'boton': "Guardar",
        }
    )

@permission_required('operadores.wservices')
def menu_opciones(request):
    return render(request, 'opciones/menu_opciones.html', {})

@permission_required('operadores.wservices')
def lista_alianzas(request):
    alianzas = Alianza.objects.all()
    for i in alianzas:
        for j in NACIONALIDADES:
            if i.nacionalidad == j[0]:
                i.nacionalidad = j[1]
    return render(request, 'opciones/lista_alianzas.html', {
        'alianzas':alianzas,
        'has_table': True,
        })

@permission_required('operadores.wservice')
def cargar_alianza(request, alianza_id=None):
    if alianza_id:
        alianza = Alianza.objects.get(pk=alianza_id)
        form = AlianzaForm(instance=alianza)
        if request.method == "POST":
            form = AlianzaForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('core:lista_alianzas')
    else:
        form = AlianzaForm()
        if request.method == "POST":
            form = AlianzaForm(request.POST, request.FILES, instance=alianza)
            if form.is_valid():
                form.save()
                return redirect('core:lista_alianzas')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Crear Nueva Alianza", 'form': form, 'boton': "Agregar", })

def desactivar_alianza(request, alianza_id):
    alianza = Alianza.objects.get(pk=alianza_id)
    if alianza.activa:
        alianza.activa = False
    else:
        alianza.activa = True
    alianza.save()
    return redirect('core:lista_alianzas')


@permission_required('operadores.wservices')
def lista_directivos(request):
    directivos = Director.objects.all()
    return render(request, 'opciones/lista_directivos.html', {'directivos':directivos})

@permission_required('operadores.wservice')
def cargar_directivo(request, directivo_id=None):
    if directivo_id:
        directivo = Director.objects.get(pk=directivo_id)
        form = DirectivoForm(instance=directivo)
        if request.method == "POST":
            form = DirectivoForm(request.POST, request.FILES, instance=directivo)
            if form.is_valid():
                form.save()
                return redirect('core:lista_directivos')
    else:
        form = DirectivoForm()
        if request.method == "POST":
            form = DirectivoForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('core:lista_directivos')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Cargar Nuevo Directivo", 'form': form, 'boton': "Guardar", })

def desactivar_directivo(request, directivo_id):
    directivo = Director.objects.get(pk=directivo_id)
    if directivo.activo:
        directivo.activo = False
    else:
        directivo.activo = True
    directivo.save()
    return redirect('core:lista_directivos')


@permission_required('operadores.wservices')
def lista_videos(request):
    videos = Video.objects.all()
    return render(request, 'opciones/lista_videos.html', {'videos':videos})

@permission_required('operadores.wservice')
def cargar_video(request, video_id=None):
    if video_id:
        video = Video.objects.get(pk=video_id)
        form = VideoForm(instance=video)
        if request.method == "POST":
            form = VideoForm(request.POST, request.FILES, instance=video)
            if form.is_valid():
                form.save()
                return redirect('core:lista_videos')
    else:
        form = VideoForm()
        if request.method == "POST":
            form = VideoForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('core:lista_videos')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Cargar Nuevo Video", 'form': form, 'boton': "Guardar", })

def desactivar_video(request, video_id):
    video = Video.objects.get(pk=video_id)
    if video.actual:
        video.actual = False
    else:
        video.actual = True
    video.save()
    return redirect('core:lista_videos')


#Activar mails
def activar_usuario_mail(request, usuario_id, token):
    try:
        usuario = User.objects.get(pk=usuario_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        usuario = None
    if usuario and account_activation_token.check_token(usuario, token):
        usuario.is_active = True
        usuario.save()
        texto = 'Excelente! Su correo electronico fue validada.'
    else:
        texto = 'El link de activacion es invalido!'
    return render(request, 'extras/resultado.html', {'texto': texto, })

def construccion(request):
    return render(request, 'extras/construccion.html', {})