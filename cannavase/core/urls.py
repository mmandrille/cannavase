#Imports de Django
from django.conf.urls import url
from django.urls import path
#Imports de la app
from . import views
from . import autocomplete
#Definimos nuestros Paths

app_name = 'core'
urlpatterns = [
    url(r'^$', views.home, name='home'),
    path('cooperamos', views.cooperamos, name='cooperamos'),
    path('hacemos', views.hacemos, name='hacemos'),
    path('somos', views.somos, name='somos'),
    path('contactos', views.contactos, name='contactos'),

    path('menu', views.menu, name='menu'),

    path('mostrar', views.mostrar, name='mostrar'),

    path('menu_opciones', views.menu_opciones, name='menu_opciones'),
    path('alianzas', views.lista_alianzas, name='lista_alianzas'),
    path('alianzas/crear', views.cargar_alianza, name='crear_alianza'),
    path('alianzas/mod/<int:alianza_id>', views.cargar_alianza, name='modificar_alianza'),
    path('alianzas/del/<int:alianza_id>', views.desactivar_alianza, name='desactivar_alianza'),
    path('directivos', views.lista_directivos, name='lista_directivos'),
    path('directivos/crear', views.cargar_directivo, name='crear_directivo'),
    path('directivos/mod/<int:directivo_id>', views.cargar_directivo, name='modificar_directivo'),
    path('directivos/del/<int:directivo_id>', views.desactivar_directivo, name='desactivar_directivo'),
    path('videos', views.lista_videos, name='lista_videos'),
    path('videos/crear', views.cargar_video, name='cargar_video'),
    path('videos/mod/<int:video_id>', views.cargar_video, name='modificar_video'),
    path('videos/del/<int:video_id>', views.desactivar_video, name='desactivar_video'),

    #Validaciones:
    url(r'^act_usuario/(?P<usuario_id>[0-9]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activar_usuario_mail, name='activar_usuario_mail'),
    #Autocomplete
    url(r'^usuarios-autocomplete/$', autocomplete.UsuariosAutocomplete.as_view(), name='usuarios-autocomplete',),

    path('construccion', views.construccion, name='construccion'),
]