#Imports django
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Proveedor


class ProveedorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Proveedor.objects.all()
        if self.q:
            qs = qs.filter(identificador__icontains=self.q)
        return qs