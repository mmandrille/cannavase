from django.db import models


# Create your models here.

class Proveedor(models.Model):
    nombre = models.CharField('Nombre', max_length=60)
    observaciones = models.CharField('Observaciones', max_length=400)

    def __str__(self):
        return str(self.nombre)
