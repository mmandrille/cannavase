# Imports Django
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.decorators import permission_required

# API
from rest_framework import generics
from .serializer import *

# Imports app
from .models import Proveedor
from .forms import ProveedorForm


# Create your views here.

@permission_required('operadores.inventario_admin')
def menu(request):
    return render(request, 'menu_comercial.html', {})


# Proveedores
@permission_required('operadores.inventario_admin')
def lista_proveedores(request):
    proveedores = Proveedor.objects.all()
    return render(request, 'proveedores/lista_proveedores.html', {
        'proveedores': proveedores,
        'has_table': True,
    })


@permission_required('operadores.inventario_admin')
def crear_proveedor(request, proveedor_id=None):
    if proveedor_id:
        proveedor = Proveedor.objects.get(pk=proveedor_id)
        form = ProveedorForm(instance=proveedor)
    else:
        proveedor = None
        form = ProveedorForm()
    if request.method == "POST":
        form = ProveedorForm(request.POST, instance=proveedor)
        if form.is_valid():
            form.save()
            return redirect('comercial:lista_proveedores')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Crear Nuevo Proveedor", 'form': form, 'boton': "Agregar", })


@permission_required('operadores:inventario_admin')
def del_proveedor(request, proveedor_id):
    proveedor = Proveedor.objects.get(pk=proveedor_id)
    if request.method == 'POST':
        proveedor.delete()
        return redirect('comercial:lista_proveedores')
    return render(request, 'extras/confirmar.html', {'titulo': 'Eliminar Proveedor',
                                                     'message': 'Esta acción eliminará al Proveedor seleccionado',
                                                     'has_form': True})


# API
# Proveedores
class ProveedoresList(generics.ListAPIView):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer


class ProveedorCreate(generics.CreateAPIView):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer


class ProveedorUpdate(generics.UpdateAPIView):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer


class ProveedorDelete(generics.DestroyAPIView):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer
