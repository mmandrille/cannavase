from django.apps import AppConfig

from core.functions import agregar_menu

class ComercialConfig(AppConfig):
    name = 'comercial'
    verbose_name = 'Comercial'
    def ready(self):
        agregar_menu(self)