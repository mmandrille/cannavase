#imports django
from django.conf.urls import url
from django.urls import path
#imports extra
from dal import autocomplete
#Import de modulos personales
from .views import *
from .autocomplete import *

#Definimos los paths de la app
app_name = 'comercial'
urlpatterns = [

    path('', menu, name='menu_comercial'),

    #Proveedores
    path('lista_proveedores', lista_proveedores, name='lista_proveedores'),
    path('crear_proveedores', crear_proveedor, name='crear_proveedores'),
    path('mod_proveedores/<int:proveedor_id>', crear_proveedor, name='mod_proveedores'),
    path('del_proveedores/<int:proveedor_id>', del_proveedor, name='del_proveedores'),

    # API
    path('proveedores', ProveedoresList.as_view()),
    path('proveedores/create', ProveedorCreate.as_view()),
    path('proveedores/update/<int:pk>', ProveedorUpdate.as_view()),
    path('proveedores/delete/<int:pk>', ProveedorDelete.as_view()),

    # Autocomplete
    url(r'^proveedor-autocomplete/$', ProveedorAutocomplete.as_view(), name='proveedorAutocomplete',),
]
