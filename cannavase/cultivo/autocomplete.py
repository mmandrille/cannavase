﻿#Imports django
from django.contrib.auth.models import User
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import *


class SemillaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Semilla.objects.filter(activo=True).order_by('codigo')
        return qs


class LoteSemillaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = LoteSemilla.objects.filter(activo=True, lote__estado='Disponible').order_by('codigo')
        return qs


class LoteGerminacionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = LoteGerminacion.objects.filter(activo=True, lote__estado='Activo').order_by('codigo')
        return qs


class LoteVegetativoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = LoteVegetativo.objects.filter(activo=True, lote__estado='Activo').order_by('codigo')
        return qs


class LoteFloracionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = LoteFloracion.objects.filter(activo=True, lote__estado='Activo').order_by('codigo')
        return qs


class LoteSecadoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = LoteSecado.objects.filter(activo=True, lote__estado='Activo').order_by('codigo')
        return qs