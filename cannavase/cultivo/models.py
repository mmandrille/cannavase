import requests
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils import timezone
from django.db import models
from tinymce.models import HTMLField
from comercial.models import Proveedor
from georef.models import Predio, Instalacion, Division, Almacen, Recipiente
from operadores.models import Operador
from operadores.functions import usuarioActual
from .choices import SEXO


class Semilla(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True)
    variedad = models.CharField('Variedad', max_length=50, default='')
    origen = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    poderGerminativo = models.IntegerField('Poder Germinativo', validators=[MinValueValidator(0), MaxValueValidator(100)], null=True, blank=True)
    cbd = models.DecimalField('% CBD', max_digits=5, decimal_places=2, validators=[MinValueValidator(0), MaxValueValidator(100)], null=True, blank=True)
    thc = models.DecimalField('% THC', max_digits=5, decimal_places=2, validators=[MinValueValidator(0), MaxValueValidator(100)], null=True, blank=True)
    radio = models.CharField('Radio Potencial THC CBD', max_length=10, null=True, blank=True, default='0:0')
    morfologia = models.CharField('Morfologia', max_length=50, default='', null=True, blank=True)
    sexo = models.CharField('Sexo', max_length=9, choices=SEXO, default='Femenino', null=True, blank=True)
    diasFloracion = models.IntegerField('Dias Floracion', null=True, blank=True)
    rendimientoEsperado = models.CharField('Rendimiento Esperado', max_length=10, default='', null=True, blank=True)
    alturaPromedio = models.DecimalField('Altura Promedio', max_digits=10, decimal_places=3, null=True, blank=True)
    responsable = models.ForeignKey(Operador, on_delete=models.CASCADE)
    fechaAlta = models.DateField('Fecha de Alta', default=timezone.now, null=True, blank=True)
    informacionDePadres = HTMLField('Informacion de Padres', null=True, blank=True)
    observaciones = HTMLField(null=True, blank=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo + ' - ' + self.variedad


class Lote(models.Model):
    # codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    predio = models.ForeignKey(Predio, verbose_name='Establecimiento / Predio', on_delete=models.CASCADE, null=False, blank=False, default='')
    instalacion = models.ForeignKey(Instalacion, on_delete=models.CASCADE, null=False, blank=False, default='')
    division = models.ForeignKey(Division, verbose_name='Sub-Instalacion / Division', on_delete=models.CASCADE, null=True, blank=True)
    almacen = models.ForeignKey(Almacen, on_delete=models.CASCADE, null=True, blank=True)
    recipiente = models.ForeignKey(Recipiente, on_delete=models.CASCADE, null=False, blank=False, default='')
    tipo = models.CharField('Tipo', max_length=30, default='', null=True, blank=True)
    estado = models.CharField('Estado', max_length=30, default='', null=True, blank=True)
    fechaMovimiento = models.DateField('Fecha Movimiento', null=False, blank=False, default=timezone.now)
    fechaAlta = models.DateField('Fecha Alta', default=timezone.now)
    fechaFinalizacion = models.DateField('Fecha Finalizacion', null=True, blank=True)
    observaciones = HTMLField(null=True, blank=True)


class LoteSemilla(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    semilla = models.ForeignKey(Semilla, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    pesoInicial = models.DecimalField('Peso Inicial', null=False, blank=False, max_digits=10, decimal_places=3, default=0)
    pesoActual = models.DecimalField('Peso Actual', max_digits=10, decimal_places=3, default=0)
    cantidadEstimada = models.IntegerField('Cantidad Estimada', null=True, blank=True)
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteGerminacion(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteSemilla = models.ForeignKey(LoteSemilla, verbose_name='Lote de Semillas', on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    cantidadGramos = models.DecimalField('Cantidad en Gramos', max_digits=10, decimal_places=3)
    totalSemillas = models.IntegerField('Total de Semillas')
    noGerminadas = models.IntegerField('Semillas no Germinadas', null=True, blank=True, default=0)
    cantidadRecipientes = models.IntegerField('Cantidad de Recipientes')
    plantasLogradas = models.IntegerField('Plantas logradas', null=True, blank=True)
    plantasActuales = models.IntegerField('Plantas actuales', null=True, blank=True)
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteVegetativo(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteGerminacion = models.ForeignKey(LoteGerminacion, verbose_name='Lote Germinacion', on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas')
    stockActual = models.IntegerField('Plantas disponibles')
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteMadre(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteVegetativo = models.ForeignKey(LoteVegetativo, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas')
    stockActual = models.IntegerField('Stock Actual')
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteEsqueje(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteMadre = models.ForeignKey(LoteMadre, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas')
    stockActual = models.IntegerField('Stock Actual')
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteVegetativoEsquejes(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteEsquejes = models.ForeignKey(LoteEsqueje, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas')
    stockActual = models.IntegerField('Stock Actual')
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteFloracion(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteVegetativo = models.ForeignKey(LoteVegetativo, verbose_name='Lote de Vegetacion', on_delete=models.CASCADE, null=True)
    loteVegetativoEsquejes = models.ForeignKey(LoteVegetativoEsquejes, on_delete=models.CASCADE, null=True)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas')
    stockActual = models.IntegerField('Stock Actual')
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class LoteSecado(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteFloracion = models.ForeignKey(LoteFloracion, verbose_name='Lote Floracion', on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas Cosechadas')
    stockActual = models.IntegerField('Stock Actual')
    estado = models.CharField('Estado', max_length=10)
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)
    inicioTrimming = models.DateTimeField('Inicio de Trimming', default=None, null=True, blank=True)
    inicioSecado = models.DateTimeField('Inicio de Secado', default=None, null=True, blank=True)
    finTrimming = models.DateTimeField('Final de Trimming', default=None, null=True, blank=True)
    finSecado = models.DateTimeField('Final de Secado', default=None, null=True, blank=True)

    def __str__(self):
        return self.codigo


class LoteCosechaMaterial(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteSecado = models.ForeignKey(LoteSecado, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    totalPlantas = models.IntegerField('Total de Plantas Cosechadas')
    stockActual = models.IntegerField('Stock Actual')
    pesoSeco = models.DecimalField('Peso en Seco (Kg.)', max_digits=10, decimal_places=3)
    porcentajeHumedad = models.DecimalField('% Humedad', max_digits=10, decimal_places=2)
    estado = models.CharField('Estado', max_length=50)
    responsable = models.CharField('Responsable', max_length=50, default='')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo


class DestruccionPlantas(models.Model):
    codigo = models.CharField('Codigo Identificador', max_length=20, null=False, blank=False, unique=True, default='')
    loteV = models.ForeignKey(LoteVegetativo, verbose_name='Lote', on_delete=models.CASCADE, null=True, blank=True)
    loteF = models.ForeignKey(LoteFloracion, verbose_name='Lote', on_delete=models.CASCADE, null=True, blank=True)
    loteG = models.ForeignKey(LoteGerminacion, verbose_name='Lote', on_delete=models.CASCADE, null=True, blank=True)
    cantidad = models.IntegerField('Cantidad')
    observaciones = HTMLField()
    responsable = models.CharField('Responsable', max_length=50, default='')
    fecha = models.DateField('Fecha de movimiento', default=timezone.now, null=True, blank=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.codigo
