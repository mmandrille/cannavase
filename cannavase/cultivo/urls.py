﻿from django.conf.urls import url
from django.urls import path

from . import views, autocomplete

app_name = 'cultivo'
urlpatterns = [
    path('', views.menu, name='menu'),

    # Semillas
    path('semilla', views.SemillaList.as_view(), name='semillaList'),
    path('semilla/create', views.SemillaCreate.as_view(), name='semillaCreate'),
    path('semilla/update/<int:pk>', views.SemillaUpdate.as_view(), name='semillaUpdate'),
    path('semilla/delete/<int:pk>', views.SemillaDelete.as_view(), name='semillaDelete'),
    path('semilla/detail/<int:pk>', views.SemillaDetalle.as_view(), name='semillaDetalle'),

    # Lote de Semillas
    path('loteSemillas', views.LoteSemillasList.as_view(), name='loteSemillasList'),
    path('loteSemillas/create', views.LoteSemillasCreate.as_view(), name='loteSemillasCreate'),
    path('loteSemillas/update/<int:pk>', views.LoteSemillasUpdate.as_view(), name='loteSemillasUpdate'),
    path('loteSemillas/delete/<int:pk>', views.LoteSemillasDelete.as_view(), name='loteSemillasDelete'),
    path('loteSemillas/detail/<int:pk>', views.LoteSemillaDetalle.as_view(), name='loteSemillasDetalle'),

    # Lote de Germinacion
    path('loteGerminacion', views.LoteGerminacionList.as_view(), name='loteGerminacionList'),
    path('loteGerminacion/create/<int:pk>', views.LoteGerminacionCreate.as_view(), name='loteGerminacionCreate'),
    path('loteGerminacion/update/<int:pk>', views.LoteGerminacionUpdate.as_view(), name='loteGerminacionUpdate'),
    path('loteGerminacion/delete/<int:pk>', views.LoteGerminacionDelete.as_view(), name='loteGerminacionDelete'),
    path('loteGerminacion/detail/<int:pk>', views.LoteGerminacionDetalle.as_view(), name='loteGerminacionDetalle'),
    path('loteGerminacion/plantas/<int:pk>', views.PlantasLogradas.as_view(), name='plantasLogradas'),

    # Lote Vegetativo
    path('loteVegetativo', views.LoteVegetativoList.as_view(), name='loteVegetativoList'),
    path('loteVegetativo/create/<int:pk>', views.LoteVegetativoCreate.as_view(), name='loteVegetativoCreate'),
    path('loteVegetativo/update/<int:pk>', views.LoteVegetativoUpdate.as_view(), name='loteVegetativoUpdate'),
    path('loteVegetativo/delete/<int:pk>', views.LoteVegetativoDelete.as_view(), name='loteVegetativoDelete'),
    path('loteVegetativo/detail/<int:pk>', views.LoteVegetativoDetalle.as_view(), name='loteVegetativoDetalle'),

    # Lote de Floracion
    path('loteFloracion', views.LoteFloracionList.as_view(), name='loteFloracionList'),
    path('loteFloracion/create/<int:pk>', views.LoteFloracionCreate.as_view(), name='loteFloracionCreate'),
    path('loteFloracion/update/<int:pk>', views.LoteFloracionUpdate.as_view(), name='loteFloracionUpdate'),
    path('loteFloracion/delete/<int:pk>', views.LoteFloracionDelete.as_view(), name='loteFloracionDelete'),
    path('loteFloracion/detail/<int:pk>', views.LoteFloracionDetalle.as_view(), name='loteFloracionDetalle'),
    
    # Lote de Secado
    path('loteSecado', views.LoteSecadoList.as_view(), name='loteSecadoList'),
    path('loteSecado/create/<int:pk>', views.LoteSecadoCreate.as_view(), name='loteSecadoCreate'),
    path('loteSecado/update/<int:pk>', views.LoteSecadoUpdate.as_view(), name='loteSecadoUpdate'),
    path('loteSecado/delete/<int:pk>', views.LoteSecadoDelete.as_view(), name='loteSecadoDelete'),
    path('loteSecado/detail/<int:pk>', views.LoteSecadoDetalle.as_view(), name='loteSecadoDetalle'),
    path('loteSecado/trimming/<int:pk>', views.Trimming.as_view(), name='loteSecadoTrimming'),
    path('loteSecado/secado/<int:pk>', views.Secado.as_view(), name='loteSecadoSecado'),

    # Lote de Flores Deshidratadas
    path('loteMaterial', views.LoteMaterialList.as_view(), name='loteMaterialList'),
    path('loteMaterial/create/<int:pk>', views.LoteMaterialCreate.as_view(), name='loteMaterialCreate'),
    path('loteMaterial/update/<int:pk>', views.LoteMaterialUpdate.as_view(), name='loteMaterialUpdate'),
    path('loteMaterial/delete/<int:pk>', views.LoteMaterialDelete.as_view(), name='loteMaterialDelete'),
    path('loteMaterial/detail/<int:pk>', views.LoteMaterialDetalle.as_view(), name='loteMaterialDetalle'),

    # Destruccion Plantas
    path('loteDestruccion', views.DestruccionList.as_view(), name='destruccionList'),
    path('loteDestruccion/vegetativo/create/<int:pk>', views.DestruccionVegetativoCreate.as_view(), name='destruccionVegetativoCreate'),
    path('loteDestruccion/floracion/create/<int:pk>', views.DestruccionFloracionCreate.as_view(), name='destruccionFloracionCreate'),
    path('loteDestruccion/germinacion/create/<int:pk>', views.DestruccionGerminacionCreate.as_view(), name='destruccionGerminacionCreate'),

    # Lote de Plantas Madre
    path('loteMadre', views.LoteMadreList.as_view(), name='loteMadreList'),
    path('loteMadre/create/<int:pk>', views.LoteMadreCreate.as_view(), name='loteMadreCreate'),
    path('loteMadre/update/<int:pk>', views.LoteMadreUpdate.as_view(), name='loteMadreUpdate'),
    path('loteMadre/delete/<int:pk>', views.LoteMadreDelete.as_view(), name='loteMadreDelete'),
    path('loteMadre/detail/<int:pk>', views.LoteMadreDetalle.as_view(), name='loteMadreDetalle'),

#Autocomplete
    url(r'^semillas-autocomplete/$', autocomplete.SemillaAutocomplete.as_view(), name='semillas-autocomplete',),
    url(r'^loteSemillas-autocomplete/$', autocomplete.LoteSemillaAutocomplete.as_view(), name='loteSemillas-autocomplete',),
    url(r'^loteGerminacion-autocomplete/$', autocomplete.LoteGerminacionAutocomplete.as_view(), name='loteGerminacion-autocomplete',),
    url(r'^loteVegetativo-autocomplete/$', autocomplete.LoteVegetativoAutocomplete.as_view(), name='loteVegetativo-autocomplete',),
    url(r'^loteFloracion-autocomplete/$', autocomplete.LoteFloracionAutocomplete.as_view(), name='loteFloracion-autocomplete',),
    url(r'^loteSecado-autocomplete/$', autocomplete.LoteSecadoAutocomplete.as_view(), name='loteSecado-autocomplete',),
]
