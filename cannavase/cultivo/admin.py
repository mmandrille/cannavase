from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Semilla)
admin.site.register(Lote)
admin.site.register(LoteSemilla)
admin.site.register(LoteGerminacion)
admin.site.register(LoteVegetativo)
admin.site.register(LoteMadre)
admin.site.register(LoteEsqueje)
admin.site.register(LoteVegetativoEsquejes)
admin.site.register(LoteFloracion)
admin.site.register(LoteSecado)
admin.site.register(LoteCosechaMaterial)
admin.site.register(DestruccionPlantas)