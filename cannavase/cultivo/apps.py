from django.apps import AppConfig
from core.functions import agregar_menu


class CultivoConfig(AppConfig):
    name = 'cultivo'
    verbose_name = 'Lotes de Cultivo'

    def ready(self):
        agregar_menu(self)

