from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .models import *
from .forms import *
from django.http.response import BadHeaderError
from operadores.functions import obtener_operador


# Vista para el menu
@permission_required('operadores.inventario_admin')
def menu(request):
    return render(request, 'MenuCultivo.html', {})


# Vistas para Semillas
class SemillaList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'
    model = Semilla

    def get(self, request, *args, **kwargs):
        semillas = Semilla.objects.filter(activo=True)
        context = {
            'semillas': semillas,
            'has_table': True,
        }
        return render(request, 'Semilla.html', context)


class SemillaCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        context = {
            'form': SemillaForm(),
            'titulo': 'Alta de Semilla',
            'boton': 'Guardar',
            'botonc': 'Cancelar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = SemillaForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:semillaList'))
        return render(request, 'extras/error.html', {})


class SemillaUpdate(PermissionRequiredMixin, generic.UpdateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return Semilla.objects.get(pk=pk)
        except Semilla.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        semilla = self.get_object(pk)
        context = {
            'form': SemillaForm(instance=semilla),
            'titulo': 'Modificar Semilla',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        semilla = self.get_object(pk)
        form = SemillaForm(request.POST, instance=semilla)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:semillaList'))
        return render(request, 'extras/error.html', {})


class SemillaDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return Semilla.objects.get(pk=pk)
        except Semilla.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        semilla = self.get_object(pk)
        context = {
            'form': SemillaForm(instance=semilla),
            'titulo': 'Eliminar Semilla',
            'boton': 'Eliminar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        semilla = self.get_object(pk)
        semilla.activo = False
        semilla.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:semillaList'))


class SemillaDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return Semilla.objects.get(pk=pk)
        except Semilla.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        semilla = self.get_object(pk)
        try:
            return LoteSemilla.objects.filter(semilla_id=semilla.id).values()
        except LoteSemilla.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        semilla = self.get_object(pk)
        son = self.get_son(pk)
        context = {
            'semilla': semilla,
            'hijos': son,
        }
        return render(request, 'verDetallesSemillas.html', context)


# Vistas para Lote de Semillas
class LoteSemillasList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        semillas = LoteSemilla.objects.filter(activo=True)
        context = {
            'semillas': semillas,
            'has_table': True,
        }
        return render(request, 'LoteSemilla.html', context)


class LoteSemillasCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        context = {
            'form': LoteSemillasLoteForm(),
            'titulo': 'Cargar Lote de Semillas',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteSemillasLoteForm(request.POST)
        if form.is_valid():
            lote = form['lote'].save(commit=False)
            lote.tipo = "Lote de Semillas"
            lote.estado = "Disponible"
            lote = form['lote'].save()
            semillas = form['loteSemillas'].save(commit=False)
            semillas.lote = lote
            semillas.pesoActual = semillas.pesoInicial
            semillas.responsable = str(obtener_operador(request))
            semillas.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:loteSemillasList'))
        return render(request, 'extras/error.html', {
            'titulo': "Error al intertar guardar",
            'error': "Codigo de error: ",
        })


class LoteSemillasUpdate(PermissionRequiredMixin, generic.UpdateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSemilla.objects.get(pk=pk)
        except LoteSemilla.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteSemillasLoteForm(instance={
                'lote': lote.lote,
                'loteSemillas': lote,
            }),
            'titulo': 'Modificar Lote Semilla',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteSemillasLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteSemillas': lote,
        })
        if form.is_valid():
            lote = form['lote'].save()
            semillas = form['loteSemillas'].save(commit=False)
            semillas.lote = lote
            if semillas.pesoActual == 0:
                semillas.lote.estado = 'Finalizado'
            else:
                semillas.lote.estado = 'Disponible'
            semillas.responsable = str(obtener_operador(request))
            semillas.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:loteSemillasList'))
        return render(request, 'extras/error.html', {
            'titulo': "Error al intertar guardar",
            'error': "Codigo de error: ",
        })


class LoteSemillasDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSemilla.objects.get(pk=pk)
        except LoteSemilla.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteSemillasLoteForm(instance={
                'lote': lote.lote,
                'loteSemillas': lote,
            }),
            'titulo': 'Modificar Lote Semilla',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteSemilla = self.get_object(pk)
        loteSemilla.activo = False
        loteSemilla.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteSemillasList'))


class LoteSemillaDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSemilla.objects.get(pk=pk)
        except LoteSemilla.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        loteSemilla = self.get_object(pk)
        try:
            return LoteGerminacion.objects.filter(loteSemilla_id=loteSemilla.id)
        except LoteGerminacion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        son = self.get_son(pk)
        context = {
            'semilla': lote.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            'hijos': son,
        }
        return render(request, 'verDetalles.html', context)


# Vistas para Lote de Germinacion
class LoteGerminacionList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        germinacion = LoteGerminacion.objects.filter(activo=True)
        context = {
            'germinaciones': germinacion,
            'has_table': True,
        }
        return render(request, 'LoteGerminacion.html', context)


class LoteGerminacionCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteSemilla = LoteSemilla.objects.get(pk=pk)
            context = {
                'form': LoteGerminacionLoteForm(initial={
                    'loteGerminacion': {
                        'loteSemilla': loteSemilla,
                        'cantidadGramos': int(loteSemilla.pesoActual)
                    },
                    'lote': {
                        'predio': loteSemilla.lote.predio,
                        'instalacion': loteSemilla.lote.instalacion,
                        'division': loteSemilla.lote.division,
                        'almacen': loteSemilla.lote.almacen,
                        'recipiente': loteSemilla.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Germinacion',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteGerminacionLoteForm(),
                'titulo': 'Crear Lote de Germinacion',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        cantidadGramos = 0
        form = LoteGerminacionLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteGerminacion-loteSemilla':
                codigo = i[1]
            if i[0] == 'loteGerminacion-cantidadGramos':
                cantidadGramos = i[1]
        loteSemilla = LoteSemilla.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteSemilla.lote_id)
        pesoDisponible = loteSemilla.pesoActual
        if form.is_valid():
            if float(cantidadGramos) <= float(pesoDisponible):
                loteSemilla.pesoActual = float(pesoDisponible) - float(cantidadGramos)
                lote = form['lote'].save(commit=False)
                if float(loteSemilla.pesoActual) == 0.000:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Disponible'
                # loteLote.fechaMovimiento = timezone.now()
                loteSemilla.save()
                loteLote.save()
                lote.tipo = "Lote de Germinacion"
                lote.estado = "Activo"
                lote.save()
                germinacion = form['loteGerminacion'].save(commit=False)
                germinacion.lote = lote
                germinacion.responsable = str(obtener_operador(request))
                germinacion.plantasActuales = germinacion.plantasLogradas
                germinacion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteGerminacionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Germinacion',
                    'error': 'La cantidad de Semillas a germinar no puede ser mayor a la cantidad disponible.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Germinacion',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteGerminacionUpdate(PermissionRequiredMixin, generic.UpdateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteGerminacion.objects.get(pk=pk)
        except LoteGerminacion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteGerminacionLoteForm(instance={
                'lote': lote.lote,
                'loteGerminacion': lote,
            }),
            'titulo': 'Modificar Lote de Germinacion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        cantidadGramos = 0
        form = LoteGerminacionLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteGerminacion': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteGerminacion-loteSemilla':
                codigo = i[1]
            if i[0] == 'loteGerminacion-cantidadGramos':
                cantidadGramos = i[1]
        loteSemilla = LoteSemilla.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteSemilla.lote_id)
        pesoDisponible = loteSemilla.pesoActual + lote.cantidadGramos
        if form.is_valid():
            if float(cantidadGramos) <= float(pesoDisponible):
                loteSemilla.pesoActual = float(pesoDisponible) - float(cantidadGramos)
                lote = form['lote'].save(commit=False)
                if float(loteSemilla.pesoActual) == 0.000:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Diponible'
                loteSemilla.save()
                loteLote.save()
                lote.tipo = "Lote de Germinacion"
                lote.save()
                germinacion = form['loteGerminacion'].save(commit=False)
                germinacion.lote = lote
                germinacion.responsable = str(obtener_operador(request))
                germinacion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteGerminacionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Germinacion',
                    'error': 'La cantidad de Semillas a germinar no puede ser mayor a la cantidad disponible.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Germinacion',
            'error': 'Los datos del Lote son incorrectos.'
        })


class LoteGerminacionDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteGerminacion.objects.get(pk=pk)
        except LoteGerminacion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteGerminacionLoteForm(instance={
                'lote': lote.lote,
                'loteGerminacion': lote,
            }),
            'titulo': 'Modificar Lote Germinacion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteGerminacion = self.get_object(pk)
        loteGerminacion.activo = False
        loteGerminacion.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteGerminacionList'))


class LoteGerminacionDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteGerminacion.objects.get(pk=pk)
        except LoteGerminacion.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        loteGerminacion = self.get_object(pk)
        try:
            return LoteVegetativo.objects.filter(loteGerminacion_id=loteGerminacion.id)
        except LoteVegetativo.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        son = self.get_son(pk)
        context = {
            'semilla': lote.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            'hijos': son,
        }
        return render(request, 'verDetalles.html', context)


class PlantasLogradas(PermissionRequiredMixin, generic.UpdateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteGerminacion.objects.get(pk=pk)
        except LoteGerminacion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        totalSemillas = lote.totalSemillas
        context = {
            'form': PlantasLogradasForm(instance=lote, initial={'plantasLogradas': totalSemillas}),
            'titulo': 'Ingrese Plantas Logradas',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = PlantasLogradasForm(request.POST, instance=lote)
        if lote.totalSemillas >= int(form['plantasLogradas'].value()) > 0:
            if form.is_valid():
                plantas = form.save(commit=False)
                plantas.plantasActuales = plantas.plantasLogradas
                plantas.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteGerminacionDetalle', args=[pk]))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error al ingresar Plantas Logradas',
                    'error': 'Se produjo un error al cargar las Plantas Logradas.'
                })
        else:
            return render(request, 'extras/error.html', {
                'titulo': 'Error al ingresar Plantas Logradas',
                'error': 'La cantidad de Plantas Logradas debe ser entre 1 y el total de Semillas Disponibles.'
            })


# Vistas para Lote vegetativo
class LoteVegetativoList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        vegetativo = LoteVegetativo.objects.filter(activo=True)
        context = {
            'vegetativos': vegetativo,
            'has_table': True,
        }
        return render(request, 'LoteVegetativo.html', context)


class LoteVegetativoCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteGerminacion = LoteGerminacion.objects.get(pk=pk)
            context = {
                'form': LoteVegetativoLoteForm(initial={
                    'loteVegetativo': {
                        'loteGerminacion': loteGerminacion,
                        'totalPlantas': loteGerminacion.plantasActuales
                    },
                    'lote': {
                        'predio': loteGerminacion.lote.predio,
                        'instalacion': loteGerminacion.lote.instalacion,
                        'division': loteGerminacion.lote.division,
                        'almacen': loteGerminacion.lote.almacen,
                        'recipiente': loteGerminacion.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Vegetacion',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteVegetativoLoteForm(),
                'titulo': 'Crear Lote de Vegetacion',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteVegetativoLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteVegetativo-loteGerminacion':
                codigo = i[1]
            if i[0] == 'loteVegetativo-totalPlantas':
                totalPlantas = i[1]
        loteGerminacion = LoteGerminacion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteGerminacion.lote_id)
        plantasActuales = loteGerminacion.plantasActuales
        if form.is_valid():
            if int(totalPlantas) <= int(plantasActuales):
                lote = form['lote'].save(commit=False)
                loteGerminacion.plantasActuales = int(plantasActuales) - int(totalPlantas)
                if float(loteGerminacion.plantasActuales) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                # loteLote.fechaMovimiento = timezone.now()
                loteGerminacion.save()
                loteLote.save()
                lote.tipo = "Lote de Vegetacion"
                lote.estado = "Activo"
                lote.save()
                vegetativo = form['loteVegetativo'].save(commit=False)
                vegetativo.lote = lote
                vegetativo.responsable = str(obtener_operador(request))
                vegetativo.stockActual = vegetativo.totalPlantas
                vegetativo.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteVegetativoList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Vegetacion',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Logradas.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Vegetacion',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteVegetativoUpdate(PermissionRequiredMixin, generic.UpdateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteVegetativo.objects.get(pk=pk)
        except LoteVegetativo.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteVegetativoLoteForm(instance={
                'lote': lote.lote,
                'loteVegetativo': lote,
            }),
            'titulo': 'Actualizar Lote de Vegetacion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteVegetativoLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteVegetativo': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteVegetativo-loteGerminacion':
                codigo = i[1]
            if i[0] == 'loteVegetativo-totalPlantas':
                totalPlantas = i[1]
        loteGerminacion = LoteGerminacion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteGerminacion.lote_id)
        plantasActuales = loteGerminacion.plantasActuales + int(totalPlantas)
        totalActual = lote.totalPlantas
        if form.is_valid():
            if int(totalPlantas) <= int(plantasActuales):
                formlote = form['lote'].save(commit=False)
                loteGerminacion.plantasActuales = int(plantasActuales) - int(totalPlantas)
                if int(loteGerminacion.plantasActuales) <= 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                lote.stockActual = int(totalPlantas) - int(totalActual) + int(lote.stockActual)
                loteGerminacion.save()
                loteLote.save()
                formlote.tipo = "Lote de Vegetacion"
                if lote.stockActual == 0:
                    formlote.estado = "Finalizado"
                else:
                    formlote.estado = "Activo"
                formlote.save()
                vegetativo = form['loteVegetativo'].save(commit=False)
                vegetativo.lote = formlote
                vegetativo.responsable = str(obtener_operador(request))
                vegetativo.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteVegetativoList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Vegetacion',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Logradas.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Vegetacion',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteVegetativoDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteVegetativo.objects.get(pk=pk)
        except LoteVegetativo.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteVegetativoLoteForm(instance={
                'lote': lote.lote,
                'loteVegetativo': lote,
            }),
            'titulo': 'Eliminar Lote de Vegetacion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteVegetativo = self.get_object(pk)
        loteVegetativo.activo = False
        loteVegetativo.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteVegetativoList'))


class LoteVegetativoDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteVegetativo.objects.get(pk=pk)
        except LoteVegetativo.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        loteVegetativo = self.get_object(pk)
        try:
            return LoteFloracion.objects.filter(loteVegetativo_id=loteVegetativo.id)
        except LoteFloracion.DoesNotExist:
            raise Http404

    def get_son2(self, pk):
        loteVegetativo = self.get_object(pk)
        try:
            return LoteMadre.objects.filter(loteVegetativo_id=loteVegetativo.id)
        except LoteMadre.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        son = self.get_son(pk)
        son2 = self.get_son2(pk)
        context = {
            'semilla': lote.loteGerminacion.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            'hijos': son,
            'hijos2': son2,
        }
        return render(request, 'verDetalles.html', context)


# Vistas para Lotes de Floracion
class LoteFloracionList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        floracion = LoteFloracion.objects.filter(activo=True)
        context = {
            'floraciones': floracion,
            'has_table': True,
        }
        return render(request, 'LoteFloracion.html', context)


class LoteFloracionCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteVegetacion = LoteVegetativo.objects.get(pk=pk)
            context = {
                'form': LoteFloracionLoteForm(initial={
                    'loteFloracion': {
                        'loteVegetativo': loteVegetacion,
                        'totalPlantas': loteVegetacion.stockActual
                    },
                    'lote': {
                        'predio': loteVegetacion.lote.predio,
                        'instalacion': loteVegetacion.lote.instalacion,
                        'division': loteVegetacion.lote.division,
                        'almacen': loteVegetacion.lote.almacen,
                        'recipiente': loteVegetacion.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Floracion',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteFloracionLoteForm(),
                'titulo': 'Crear Lote de Floracion',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteFloracionLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteFloracion-loteVegetativo':
                codigo = i[1]
            if i[0] == 'loteFloracion-totalPlantas':
                totalPlantas = i[1]
        loteVegetativo = LoteVegetativo.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteVegetativo.lote_id)
        plantasDisponibles = loteVegetativo.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteVegetativo.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteVegetativo.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                # loteLote.fechaMovimiento = timezone.now()
                loteVegetativo.save()
                loteLote.save()
                lote.tipo = "Lote de Floracion"
                lote.estado = "Activo"
                lote.save()
                floracion = form['loteFloracion'].save(commit=False)
                floracion.lote = lote
                floracion.responsable = str(obtener_operador(request))
                floracion.stockActual = floracion.totalPlantas
                floracion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteFloracionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Floracion',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Floracion',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteFloracionUpdate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteFloracion.objects.get(pk=pk)
        except LoteFloracion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteFloracionLoteForm(instance={
                'lote': lote.lote,
                'loteFloracion': lote,
            }),
            'titulo': 'Actualizar Lote de Floracion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteFloracionLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteFloracion': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteFloracion-loteVegetativo':
                codigo = i[1]
            if i[0] == 'loteFloracion-totalPlantas':
                totalPlantas = i[1]
        loteVegetativo = LoteVegetativo.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteVegetativo.lote_id)
        plantasDisponibles = loteVegetativo.stockActual + int(totalPlantas)
        totalActual = lote.totalPlantas
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                formlote = form['lote'].save(commit=False)
                loteVegetativo.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteVegetativo.stockActual) <= 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                lote.stockActual = int(totalPlantas) - int(totalActual) + int(lote.stockActual)
                loteVegetativo.save()
                loteLote.save()
                formlote.tipo = "Lote de Floracion"
                if lote.stockActual == 0:
                    formlote.estado = "Finalizado"
                else:
                    formlote.estado = "Activo"
                formlote.save()
                floracion = form['loteFloracion'].save(commit=False)
                floracion.lote = formlote
                floracion.responsable = str(obtener_operador(request))
                floracion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteFloracionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Floracion',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Floracion',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteFloracionDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteFloracion.objects.get(pk=pk)
        except LoteFloracion.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteFloracionLoteForm(instance={
                'lote': lote.lote,
                'loteFloracion': lote,
            }),
            'titulo': 'Eliminar Lote de Floracion',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteFloracion = self.get_object(pk)
        loteFloracion.activo = False
        loteFloracion.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteFloracionList'))


class LoteFloracionDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteFloracion.objects.get(pk=pk)
        except LoteFloracion.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        loteFloracion = self.get_object(pk)
        try:
            return LoteSecado.objects.filter(loteFloracion_id=loteFloracion.id)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        son = self.get_son(pk)
        context = {
            'semilla': lote.loteVegetativo.loteGerminacion.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            'hijos': son,
        }
        return render(request, 'verDetalles.html', context)


# Vistas para Lotes de Secado
class LoteSecadoList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        secado = LoteSecado.objects.filter(activo=True)
        context = {
            'secados': secado,
            'has_table': True,
        }
        return render(request, 'LoteSecado.html', context)


class LoteSecadoCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteFloracion = LoteFloracion.objects.get(pk=pk)
            context = {
                'form': LoteSecadoLoteForm(initial={
                    'loteSecado': {
                        'loteFloracion': loteFloracion,
                        'totalPlantas': loteFloracion.stockActual
                    },
                    'lote': {
                        'predio': loteFloracion.lote.predio,
                        'instalacion': loteFloracion.lote.instalacion,
                        'division': loteFloracion.lote.division,
                        'almacen': loteFloracion.lote.almacen,
                        'recipiente': loteFloracion.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Trimming y Secado',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteSecadoLoteForm(),
                'titulo': 'Crear Lote de Trimming y Secado',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteSecadoLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteSecado-loteFloracion':
                codigo = i[1]
            if i[0] == 'loteSecado-totalPlantas':
                totalPlantas = i[1]
        loteFloracion = LoteFloracion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteFloracion.lote_id)
        plantasDisponibles = loteFloracion.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteFloracion.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteFloracion.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                # loteLote.fechaMovimiento = timezone.now()
                loteFloracion.save()
                loteLote.save()
                lote.tipo = "Lote de Secado"
                lote.estado = "Activo"
                lote.save()
                secado = form['loteSecado'].save(commit=False)
                secado.lote = lote
                secado.responsable = str(obtener_operador(request))
                secado.stockActual = secado.totalPlantas
                secado.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteSecadoList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Trimming y Secado',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Trimming y Secado',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteSecadoUpdate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSecado.objects.get(pk=pk)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteSecadoLoteForm(instance={
                'lote': lote.lote,
                'loteSecado': lote,
            }),
            'titulo': 'Actualizar Lote de Trimming y Secado',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteSecadoLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteSecado': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteSecado-loteFloracion':
                codigo = i[1]
            if i[0] == 'loteSecado-totalPlantas':
                totalPlantas = i[1]
        loteFloracion = LoteFloracion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteFloracion.lote_id)
        plantasDisponibles = loteFloracion.stockActual + int(totalPlantas)
        totalActual = lote.totalPlantas
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                formlote = form['lote'].save(commit=False)
                loteFloracion.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteFloracion.stockActual) <= 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                lote.stockActual = int(totalPlantas) - int(totalActual) + int(lote.stockActual)
                loteFloracion.save()
                loteLote.save()
                formlote.tipo = "Lote de Secado"
                if lote.stockActual == 0:
                    formlote.estado = "Finalizado"
                else:
                    formlote.estado = "Activo"
                formlote.save()
                secado = form['loteSecado'].save(commit=False)
                secado.lote = formlote
                secado.responsable = str(obtener_operador(request))
                secado.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteSecadoList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Trimming y Secado',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Trimming y Secado',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteSecadoDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSecado.objects.get(pk=pk)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteSecadoLoteForm(instance={
                'lote': lote.lote,
                'loteFloracion': lote,
            }),
            'titulo': 'Eliminar Lote de Trimming y Secado',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteSecado = self.get_object(pk)
        loteSecado.activo = False
        loteSecado.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteSecadoList'))


class LoteSecadoDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSecado.objects.get(pk=pk)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get_son(self, pk):
        loteSecado = self.get_object(pk)
        try:
            return LoteCosechaMaterial.objects.filter(loteSecado_id=loteSecado.id)
        except LoteCosechaMaterial.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        son = self.get_son(pk)
        context = {
            'semilla': lote.loteFloracion.loteVegetativo.loteGerminacion.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            'hijos': son,
        }
        return render(request, 'verDetalles.html', context)


class Trimming(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSecado.objects.get(pk=pk)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': TrimmingForm(instance=lote),
            'titulo': 'Registro de Trimming',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        loteSecado = self.get_object(pk)
        form = TrimmingForm(request.POST, instance=loteSecado)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:loteSecadoDetalle', args=[pk]))
        else:
            return render(request, 'extras/error.html', {
                'titulo': 'Error en Registro de Trimming',
                'error': 'Se produjo un error en el Registro de Trimming.'
            })


class Secado(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteSecado.objects.get(pk=pk)
        except LoteSecado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': SecadoForm(instance=lote),
            'titulo': 'Registro de Secado',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        loteSecado = self.get_object(pk)
        form = SecadoForm(request.POST, instance=loteSecado)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('cultivo:loteSecadoDetalle', args=[pk]))
        else:
            return render(request, 'extras/error.html', {
                'titulo': 'Error en Registro de Secado',
                'error': 'Se produjo un error en el Registro de Secado.'
            })


# Vistas para Lotes de Flores Deshidratadas
class LoteMaterialList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        material = LoteCosechaMaterial.objects.filter(activo=True)
        context = {
            'materiales': material,
            'has_table': True,
        }
        return render(request, 'LoteCosechaMaterial.html', context)


class LoteMaterialCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteSecado = LoteSecado.objects.get(pk=pk)
            # pesoSeco = loteSecado.stockActual/2
            pesoSeco = 0.000
            context = {
                'form': LoteMaterialLoteForm(initial={
                    'loteMaterial': {
                        'loteSecado': loteSecado,
                        'totalPlantas': loteSecado.stockActual,
                        'pesoSeco': pesoSeco
                    },
                    'lote': {
                        'predio': loteSecado.lote.predio,
                        'instalacion': loteSecado.lote.instalacion,
                        'division': loteSecado.lote.division,
                        'almacen': loteSecado.lote.almacen,
                        'recipiente': loteSecado.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Flores Deshidratadas',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteMaterialLoteForm(),
                'titulo': 'Crear Lote de Flores Deshidratadas',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteMaterialLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteMaterial-loteSecado':
                codigo = i[1]
            if i[0] == 'loteMaterial-totalPlantas':
                totalPlantas = i[1]
        loteSecado = LoteSecado.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteSecado.lote_id)
        plantasDisponibles = loteSecado.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteSecado.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteSecado.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                loteSecado.save()
                loteLote.save()
                lote.tipo = "Lote de Flores Deshidratadas"
                lote.estado = "Activo"
                lote.save()
                material = form['loteMaterial'].save(commit=False)
                material.lote = lote
                material.responsable = str(obtener_operador(request))
                material.stockActual = material.totalPlantas
                material.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteMaterialList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Flores Deshidratadas',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Flores Deshidratadas',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteMaterialUpdate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteCosechaMaterial.objects.get(pk=pk)
        except LoteCosechaMaterial.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteMaterialLoteForm(instance={
                'lote': lote.lote,
                'loteMaterial': lote,
            }),
            'titulo': 'Actualizar Lote de Flores Deshidratadas',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteMaterialLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteMaterial': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteMaterial-loteSecado':
                codigo = i[1]
            if i[0] == 'loteMaterial-totalPlantas':
                totalPlantas = i[1]
        loteSecado = LoteSecado.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteSecado.lote_id)
        plantasDisponibles = loteSecado.stockActual + int(totalPlantas)
        totalActual = lote.totalPlantas
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                formlote = form['lote'].save(commit=False)
                loteSecado.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteSecado.stockActual) <= 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                lote.stockActual = int(totalPlantas) - int(totalActual) + int(lote.stockActual)
                loteSecado.save()
                loteLote.save()
                formlote.tipo = "Lote de Flores Deshidratadas"
                if lote.stockActual == 0:
                    formlote.estado = "Finalizado"
                else:
                    formlote.estado = "Activo"
                formlote.save()
                material = form['loteMaterial'].save(commit=False)
                material.lote = formlote
                material.responsable = str(obtener_operador(request))
                material.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteMaterialList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Flores Deshidratadas',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Flores Deshidratadas',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteMaterialDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteCosechaMaterial.objects.get(pk=pk)
        except LoteCosechaMaterial.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteMaterialLoteForm(instance={
                'lote': lote.lote,
                'loteMaterial': lote,
            }),
            'titulo': 'Eliminar Lote de Flores Deshidratadas',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteMaterial = self.get_object(pk)
        loteMaterial.activo = False
        loteMaterial.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteMaterialList'))


class LoteMaterialDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteCosechaMaterial.objects.get(pk=pk)
        except LoteCosechaMaterial.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'semilla': lote.loteSecado.loteFloracion.loteVegetativo.loteGerminacion.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote
        }
        return render(request, 'verDetalles.html', context)


# Vistas para Lotes de Flores Deshidratadas
class LoteMadreList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        madre = LoteMadre.objects.filter(activo=True)
        context = {
            'madres': madre,
            'has_table': True,
        }
        return render(request, 'LoteMadre.html', context)


class LoteMadreCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteVegetacion = LoteVegetativo.objects.get(pk=pk)
            context = {
                'form': LoteMadreLoteForm(initial={
                    'loteMadre': {
                        'loteVegetativo': loteVegetacion,
                        'totalPlantas': loteVegetacion.stockActual
                    },
                    'lote': {
                        'predio': loteVegetacion.lote.predio,
                        'instalacion': loteVegetacion.lote.instalacion,
                        'division': loteVegetacion.lote.division,
                        'almacen': loteVegetacion.lote.almacen,
                        'recipiente': loteVegetacion.lote.recipiente
                    }
                }),
                'titulo': 'Crear Lote de Plantas Madre',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': LoteMadreLoteForm(),
                'titulo': 'Crear Lote de Plantas Madre',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = LoteMadreLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteMadre-loteVegetativo':
                codigo = i[1]
            if i[0] == 'loteMadre-totalPlantas':
                totalPlantas = i[1]
        loteVegetativo = LoteVegetativo.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteVegetativo.lote_id)
        plantasDisponibles = loteVegetativo.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteVegetativo.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteVegetativo.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                loteVegetativo.save()
                loteLote.save()
                lote.tipo = "Lote de Plantas Madre"
                lote.estado = "Activo"
                lote.save()
                madre = form['loteMadre'].save(commit=False)
                madre.lote = lote
                madre.responsable = str(obtener_operador(request))
                madre.stockActual = madre.totalPlantas
                madre.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteMadreList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Plantas Madre',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Plantas Madre',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteMadreUpdate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteMadre.objects.get(pk=pk)
        except LoteMadre.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteMadreLoteForm(instance={
                'lote': lote.lote,
                'loteMadre': lote,
            }),
            'titulo': 'Actualizar Lote de Plantas Madre',
            'boton': 'Guardar',
        }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, pk):
        lote = self.get_object(pk)
        form = LoteMadreLoteForm(request.POST, instance={
            'lote': lote.lote,
            'loteMadre': lote,
        })
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'loteMadre-loteVegetativo':
                codigo = i[1]
            if i[0] == 'loteMadre-totalPlantas':
                totalPlantas = i[1]
        loteVegetativo = LoteVegetativo.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteVegetativo.lote_id)
        plantasDisponibles = loteVegetativo.stockActual + int(totalPlantas)
        totalActual = lote.totalPlantas
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                formlote = form['lote'].save(commit=False)
                loteVegetativo.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteVegetativo.stockActual) <= 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                lote.stockActual = int(totalPlantas) - int(totalActual) + int(lote.stockActual)
                loteVegetativo.save()
                loteLote.save()
                formlote.tipo = "Lote de Plantas Madre"
                if lote.stockActual == 0:
                    formlote.estado = "Finalizado"
                else:
                    formlote.estado = "Activo"
                formlote.save()
                madre = form['loteMadre'].save(commit=False)
                madre.lote = formlote
                madre.responsable = str(obtener_operador(request))
                madre.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:loteMadreList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en Lote de Plantas Madre',
                    'error': 'La cantidad de Total de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Plantas Madre',
            'error': 'Los datos ingresados son incorrectos.'
        })


class LoteMadreDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteMadre.objects.get(pk=pk)
        except LoteMadre.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        context = {
            'form': LoteMadreLoteForm(instance={
                'lote': lote.lote,
                'loteMadre': lote,
            }),
            'titulo': 'Eliminar Lote de Plantas Madre',
            'boton': 'Guardar',
        }
        return render(request, 'extras/confirmar.html', context)

    def post(self, request, pk):
        loteMadre = self.get_object(pk)
        loteMadre.activo = False
        loteMadre.save()
        return HttpResponseRedirect(reverse_lazy('cultivo:loteMadreList'))


class LoteMadreDetalle(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.produccion'

    def get_object(self, pk):
        try:
            return LoteMadre.objects.get(pk=pk)
        except LoteMadre.DoesNotExist:
            raise Http404

    # def get_son(self, pk):
    #     loteMadre = self.get_object(pk)
    #     try:
    #         return LoteSecado.objects.filter(loteFloracion_id=loteMadre.id)
    #     except LoteSecado.DoesNotExist:
    #         raise Http404

    def get(self, request, pk):
        lote = self.get_object(pk)
        # son = self.get_son(pk)
        context = {
            'semilla': lote.loteVegetativo.loteGerminacion.loteSemilla.semilla,
            'lote': lote,
            'loteCultivo': lote.lote,
            # 'hijos': son,
        }
        return render(request, 'verDetalles.html', context)

# Vistas para Lotes de Plantas destruidas
class DestruccionList(PermissionRequiredMixin, generic.ListView):
    permission_required = 'operadores.produccion'

    def get(self, request, *args, **kwargs):
        destruccion = DestruccionPlantas.objects.filter(activo=True)
        context = {
            'destrucciones': destruccion,
            'has_table': True,
        }
        return render(request, 'DestruccionPlantas.html', context)


class DestruccionVegetativoCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteVegetativo = LoteVegetativo.objects.get(pk=pk)
            stockActual = loteVegetativo.stockActual
            context = {
                'form': DestruccionVegetativoLoteForm(initial={
                    'vegetativo': {
                        'codigo': loteVegetativo,
                    },
                    'destruccion': {
                        'loteV': loteVegetativo,
                        'cantidad': stockActual,
                    },
                    'lote': {
                        'predio': loteVegetativo.lote.predio,
                        'instalacion': loteVegetativo.lote.instalacion,
                        'division': loteVegetativo.lote.division,
                        'almacen': loteVegetativo.lote.almacen,
                        'recipiente': loteVegetativo.lote.recipiente
                    },

                }),
                'titulo': 'Registrar Baja de Plantas',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': DestruccionVegetativoLoteForm(),
                'titulo': 'Registrar Baja de Plantas',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = DestruccionVegetativoLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'destruccion-loteV':
                codigo = i[1]
            if i[0] == 'destruccion-cantidad':
                totalPlantas = i[1]
        loteVegetativo = LoteVegetativo.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteVegetativo.lote_id)
        plantasDisponibles = loteVegetativo.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteVegetativo.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteVegetativo.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                loteVegetativo.save()
                loteLote.save()
                lote.save()
                destruccion = form['destruccion'].save(commit=False)
                destruccion.loteV = loteVegetativo
                destruccion.responsable = str(obtener_operador(request))
                destruccion.cantidad = int(totalPlantas)
                destruccion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:destruccionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en la carga',
                    'error': 'La cantidad de Total de Baja de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Baja de Plantas',
            'error': 'Los datos ingresados son incorrectos.'
        })


class DestruccionFloracionCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteFloracion = LoteFloracion.objects.get(pk=pk)
            stockActual = loteFloracion.stockActual
            context = {
                'form': DestruccionFloracionLoteForm(initial={
                    'floracion': {
                        'codigo': loteFloracion,
                    },
                    'destruccion': {
                        'loteF': loteFloracion,
                        'cantidad': stockActual,
                    },
                    'lote': {
                        'predio': loteFloracion.lote.predio,
                        'instalacion': loteFloracion.lote.instalacion,
                        'division': loteFloracion.lote.division,
                        'almacen': loteFloracion.lote.almacen,
                        'recipiente': loteFloracion.lote.recipiente
                    },

                }),
                'titulo': 'Registrar Baja de Plantas',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': DestruccionFloracionLoteForm(),
                'titulo': 'Registrar Baja de Plantas',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = DestruccionFloracionLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'destruccion-loteF':
                codigo = i[1]
            if i[0] == 'destruccion-cantidad':
                totalPlantas = i[1]
        loteFloracion = LoteFloracion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteFloracion.lote_id)
        plantasDisponibles = loteFloracion.stockActual
        if form.is_valid():
            if int(totalPlantas) <= int(plantasDisponibles):
                lote = form['lote'].save(commit=False)
                loteFloracion.stockActual = int(plantasDisponibles) - int(totalPlantas)
                if int(loteFloracion.stockActual) == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                loteFloracion.save()
                loteLote.save()
                lote.save()
                destruccion = form['destruccion'].save(commit=False)
                destruccion.loteF = loteFloracion
                destruccion.responsable = str(obtener_operador(request))
                destruccion.cantidad = int(totalPlantas)
                destruccion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:destruccionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en la carga',
                    'error': 'La cantidad de Total de Baja de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Baja de Plantas',
            'error': 'Los datos ingresados son incorrectos.'
        })


class DestruccionGerminacionCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'operadores.produccion'

    def get(self, request, pk):
        if pk>0:
            loteGerminacion = LoteGerminacion.objects.get(pk=pk)
            totalSemillas = loteGerminacion.totalSemillas
            context = {
                'form': DestruccionGerminacionLoteForm(initial={
                    'germinacion': {
                        'codigo': loteGerminacion,
                    },
                    'destruccion': {
                        'loteG': loteGerminacion,
                        'cantidad': totalSemillas,
                    },
                    'lote': {
                        'predio': loteGerminacion.lote.predio,
                        'instalacion': loteGerminacion.lote.instalacion,
                        'division': loteGerminacion.lote.division,
                        'almacen': loteGerminacion.lote.almacen,
                        'recipiente': loteGerminacion.lote.recipiente
                    },

                }),
                'titulo': 'Registrar Baja de Plantas',
                'boton': 'Guardar',
            }
        else:
            context = {
                'form': DestruccionGerminacionLoteForm(),
                'titulo': 'Registrar Baja de Germinacion',
                'boton': 'Guardar',
            }
        return render(request, 'extras/generic_form.html', context)

    def post(self, request, *args, **kwargs):
        form = DestruccionGerminacionLoteForm(request.POST)
        aux = request.POST.items()
        for i in aux:
            if i[0] == 'destruccion-loteG':
                codigo = i[1]
            if i[0] == 'destruccion-cantidad':
                totalPlantas = i[1]
        loteGerminacion = LoteGerminacion.objects.get(pk=codigo)
        loteLote = Lote.objects.get(pk=loteGerminacion.lote_id)
        total = loteGerminacion.totalSemillas
        noGerminadas = loteGerminacion.noGerminadas
        if form.is_valid():
            if int(totalPlantas) <= loteGerminacion.plantasActuales:
                lote = form['lote'].save(commit=False)
                loteGerminacion.noGerminadas = int(noGerminadas) + int(totalPlantas)
                diferenciaGerminadas = loteGerminacion.plantasLogradas + loteGerminacion.noGerminadas - total
                loteGerminacion.plantasActuales = loteGerminacion.plantasActuales - int(totalPlantas)
                if loteGerminacion.plantasActuales == 0:
                    loteLote.estado = 'Finalizado'
                    loteLote.fechaFinalizacion = timezone.now()
                else:
                    loteLote.estado = 'Activo'
                loteGerminacion.save()
                loteLote.save()
                lote.save()
                destruccion = form['destruccion'].save(commit=False)
                destruccion.loteG = loteGerminacion
                destruccion.responsable = str(obtener_operador(request))
                destruccion.cantidad = int(totalPlantas)
                destruccion.save()
                return HttpResponseRedirect(reverse_lazy('cultivo:destruccionList'))
            else:
                return render(request, 'extras/error.html', {
                    'titulo': 'Error en la carga',
                    'error': 'La cantidad de Total de Baja de Plantas no puede ser mayor a la cantidad de Plantas Disponibles.'
                })
        return render(request, 'extras/error.html', {
            'titulo': 'Error en Lote de Baja de Germinacion',
            'error': 'Los datos ingresados son incorrectos.'
        })