﻿from betterforms.multiform import MultiModelForm
from django import forms
from .models import *
from dal import autocomplete
from core.widgets import XDSoftDateTimePickerInput
from comercial.autocomplete import *
from core.widgets import XDSoftDatePickerInput


class SemillaForm(forms.ModelForm):
    class Meta:
        model = Semilla
        fields = '__all__'
        exclude = ['activo']
        widgets = {
            'fechaAlta': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'origen': autocomplete.ModelSelect2(url='comercial:proveedorAutocomplete'),
            'responsable': autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
        }


class LoteForm(forms.ModelForm):
    class Meta:
        model = Lote
        fields = '__all__'
        exclude = ['codigo', 'tipo', 'fechaAlta', 'fechaFinalizacion', 'estado']
        widgets = {
            'predio': autocomplete.ModelSelect2(url='georef:predioAutocomplete'),
            'instalacion': autocomplete.ModelSelect2(url='georef:instalacionAutocomplete'),
            'division': autocomplete.ModelSelect2(url='georef:divisionAutocomplete'),
            'almacen': autocomplete.ModelSelect2(url='georef:almacenAutocomplete'),
            'recipiente': autocomplete.ModelSelect2(url='georef:recipienteAutocomplete'),
            'fechaMovimiento': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'fechaAlta': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'fechaFinalizacion': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }


class LoteSemillasForm(forms.ModelForm):
    class Meta:
        model = LoteSemilla
        fields = '__all__'
        exclude = ['lote', 'activo', 'pesoActual', 'responsable']
        widgets = {
            'responsable': autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
            'semilla': autocomplete.ModelSelect2(url='cultivo:semillas-autocomplete'),
        }


class LoteSemillasLoteForm(MultiModelForm):
    form_classes = {
        'loteSemillas': LoteSemillasForm,
        'lote': LoteForm,
    }


class LoteGerminacionForm(forms.ModelForm):
    class Meta:
        model = LoteGerminacion
        fields = '__all__'
        exclude =['lote', 'activo', 'responsable', 'plantasActuales']
        widgets = {
            'responsable': autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
            'loteSemilla': autocomplete.ModelSelect2(url='cultivo:loteSemillas-autocomplete'),
        }


class LoteGerminacionLoteForm(MultiModelForm):
    form_classes = {
        'loteGerminacion': LoteGerminacionForm,
        'lote': LoteForm,
    }


class PlantasLogradasForm(forms.ModelForm):
    class Meta:
        model = LoteGerminacion
        fields = ['plantasLogradas']


class LoteVegetativoForm(forms.ModelForm):
    class Meta:
        model = LoteVegetativo
        fields = '__all__'
        exclude = ['lote', 'activo', 'responsable', 'stockActual']
        widgets = {
            'loteGerminacion': autocomplete.ModelSelect2(url='cultivo:loteGerminacion-autocomplete'),
        }


class LoteVegetativoLoteForm(MultiModelForm):
    form_classes = {
        'loteVegetativo': LoteVegetativoForm,
        'lote': LoteForm,
    }


class LoteMadreForm(forms.ModelForm):
    class Meta:
        model = LoteMadre
        fields = '__all__'


class LoteEsquejeForm(forms.ModelForm):
    class Meta:
        model = LoteEsqueje
        fields = '__all__'


class LoteVegetativoEsquejesForm(forms.ModelForm):
    class Meta:
        model = LoteVegetativoEsquejes
        fields = '__all__'


class LoteFloracionForm(forms.ModelForm):
    class Meta:
        model = LoteFloracion
        fields = '__all__'
        exclude = ['lote', 'activo', 'responsable', 'stockActual', 'loteVegetativoEsquejes']
        widgets = {
            'loteVegetativo': autocomplete.ModelSelect2(url='cultivo:loteVegetativo-autocomplete'),
        }


class LoteFloracionLoteForm(MultiModelForm):
    form_classes = {
        'loteFloracion': LoteFloracionForm,
        'lote': LoteForm,
    }


class LoteSecadoForm(forms.ModelForm):
    class Meta:
        model = LoteSecado
        fields = '__all__'
        exclude = ['lote', 'activo', 'responsable', 'estado', 'stockActual', 'inicioTrimming', 'inicioSecado', 'finTrimming', 'finSecado',]
        widgets = {
            'loteFloracion': autocomplete.ModelSelect2(url='cultivo:loteFloracion-autocomplete'),
        }


class TrimmingForm(forms.ModelForm):
    class Meta:
        model = LoteSecado
        fields = ['inicioTrimming', 'finTrimming']
        widgets = {
            'inicioTrimming': XDSoftDateTimePickerInput(attrs={'autocomplete': 'off'}),
            'finTrimming': XDSoftDateTimePickerInput(attrs={'autocomplete': 'off'}),
        }


class SecadoForm(forms.ModelForm):
    class Meta:
        model = LoteSecado
        fields = ['inicioSecado', 'finSecado']
        widgets = {
            'inicioSecado': XDSoftDateTimePickerInput(attrs={'autocomplete': 'off'}),
            'finSecado': XDSoftDateTimePickerInput(attrs={'autocomplete': 'off'}),
        }


class LoteSecadoLoteForm(MultiModelForm):
    form_classes = {
        'loteSecado': LoteSecadoForm,
        'lote': LoteForm,
    }


class LoteCosechaMaterialForm(forms.ModelForm):
    class Meta:
        model = LoteCosechaMaterial
        fields = '__all__'
        exclude = ['lote', 'activo', 'responsable', 'estado', 'stockActual']
        widgets = {
            'loteSecado': autocomplete.ModelSelect2(url='cultivo:loteSecado-autocomplete'),
        }


class LoteMaterialLoteForm(MultiModelForm):
    form_classes = {
        'loteMaterial': LoteCosechaMaterialForm,
        'lote': LoteForm,
    }


class LoteMadreForm(forms.ModelForm):
    class Meta:
        model = LoteMadre
        fields = '__all__'
        exclude = ['lote', 'activo', 'responsable', 'stockActual']
        widgets = {
            'loteVegetativo': autocomplete.ModelSelect2(url='cultivo:loteVegetativo-autocomplete'),
        }


class LoteMadreLoteForm(MultiModelForm):
    form_classes = {
        'loteMadre': LoteMadreForm,
        'lote': LoteForm,
    }


class LoteDForm(forms.ModelForm):
    class Meta:
        model = Lote
        fields = '__all__'
        exclude = ['codigo', 'tipo', 'fechaAlta', 'fechaFinalizacion', 'estado', 'fechaMovimiento']
        widgets = {
            'predio': autocomplete.ModelSelect2(url='georef:predioAutocomplete'),
            'instalacion': autocomplete.ModelSelect2(url='georef:instalacionAutocomplete'),
            'division': autocomplete.ModelSelect2(url='georef:divisionAutocomplete'),
            'almacen': autocomplete.ModelSelect2(url='georef:almacenAutocomplete'),
            'recipiente': autocomplete.ModelSelect2(url='georef:recipienteAutocomplete'),
            'fechaMovimiento': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'fechaAlta': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'fechaFinalizacion': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }


class DestruccionVForm(forms.ModelForm):
    class Meta:
        model = DestruccionPlantas
        fields = '__all__'
        exclude = ['activo', 'observaciones', 'responsable', 'loteF', 'loteG']
        widgets = {
            'loteV': autocomplete.ModelSelect2(url='cultivo:loteVegetativo-autocomplete'),
            'fecha': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }


class DestruccionVegetativoForm(forms.ModelForm):
    class Meta:
        model = LoteVegetativo
        fields = []


class DestruccionVegetativoLoteForm(MultiModelForm):
    form_classes = {
        'destruccion': DestruccionVForm,
        'vegetativo': DestruccionVegetativoForm,
        'lote': LoteDForm,
    }


class DestruccionFForm(forms.ModelForm):
    class Meta:
        model = DestruccionPlantas
        fields = '__all__'
        exclude = ['activo', 'observaciones', 'responsable', 'loteV', 'loteG']
        widgets = {
            'loteF': autocomplete.ModelSelect2(url='cultivo:loteFloracion-autocomplete'),
            'fecha': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }


class DestruccionFloracionForm(forms.ModelForm):
    class Meta:
        model = LoteFloracion
        fields = []


class DestruccionFloracionLoteForm(MultiModelForm):
    form_classes = {
        'destruccion': DestruccionFForm,
        'floracion': DestruccionFloracionForm,
        'lote': LoteDForm,
    }


class DestruccionGForm(forms.ModelForm):
    class Meta:
        model = DestruccionPlantas
        fields = '__all__'
        exclude = ['activo', 'observaciones', 'responsable', 'loteV', 'loteF']
        widgets = {
            'loteG': autocomplete.ModelSelect2(url='cultivo:loteGerminacion-autocomplete'),
            'fecha': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }


class DestruccionGerminacionForm(forms.ModelForm):
    class Meta:
        model = LoteGerminacion
        fields = []


class DestruccionGerminacionLoteForm(MultiModelForm):
    form_classes = {
        'destruccion': DestruccionGForm,
        'germinacion': DestruccionGerminacionForm,
        'lote': LoteDForm,
    }
