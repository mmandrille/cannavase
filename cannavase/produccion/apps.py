#Imports Django
from django.apps import AppConfig
#Imports del Proyecto
from core.functions import agregar_menu

class ProduccionConfig(AppConfig):
    name = 'produccion'
    verbose_name = 'Lotes de Cultivo'
    # def ready(self):
    #     agregar_menu(self)