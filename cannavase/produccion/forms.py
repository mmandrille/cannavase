#Imports Python
#Imports Django
from django import forms
from django.utils import timezone
#Imports extra
from dal import autocomplete
from tinymce.widgets import TinyMCE
#Imports del proyecto
from core.widgets import XDSoftDatePickerInput, XDSoftDateTimePickerInput
from georef.models import Ubicacion
from georef.functions import germinadores_disponibles, ubicaciones_plantables
from inventario.models import Item, Almacen
from inventario.functions import semillas_disponibles
#Imports de la app
from .models import Objeto
from .models import Accion, UbicacionHistorica, Caracteristica

#Definimos nuestros forms
class ObjetoForm(forms.ModelForm):
    class Meta:
        model = Objeto
        fields= '__all__'

class GerminarForm(forms.ModelForm):
    semilla = forms.ModelChoiceField(queryset=semillas_disponibles())
    ubicaciones = forms.MultipleChoiceField(choices=germinadores_disponibles)#Excluir las que estan siendo usadas
    cantidad = forms.DecimalField(label="Gramos Germinados", required=False)
    class Meta:
        model = Objeto
        fields= ('identificador', 'plan_productivo', 'semilla', 'cantidad', 'ubicaciones', 'fecha')
        widgets = {
            'fecha': XDSoftDateTimePickerInput(attrs={'autocomplete':'off'}),
        }

class AgregarSemillasForm(forms.Form):
    semilla = forms.ModelChoiceField(queryset=semillas_disponibles())
    cantidad = forms.DecimalField(label="Gramos Germinada")
    fecha = forms.DateTimeField(initial=timezone.now, widget=XDSoftDateTimePickerInput(attrs={'autocomplete':'off'}))

class EvolucionarForm(forms.ModelForm):
    ubicaciones = forms.MultipleChoiceField(choices=ubicaciones_plantables())#Excluir las que estan siendo usadas
    vivas = forms.IntegerField(label="Cantidad Vivas", required=True)
    muertas = forms.IntegerField(label="Cantidad Muertas", required=False, initial=0)
    class Meta:
        model = Objeto
        fields= '__all__'
        exclude = ('semillas', 'padres', 'plan_productivo', 'tipo', 'estado', 'activo', )
        widgets = {
            'fecha': XDSoftDateTimePickerInput(attrs={'autocomplete':'off'}),
        }
    #Podriamos pedir que nos genere los datos.

class UtilizarItemForm(forms.ModelForm):
    item = forms.ModelChoiceField(queryset=Item.objects.filter(tipo="APL"))
    cantidad = forms.DecimalField(label="Cantidad Utilizada")
    class Meta:
        model = Accion
        fields = ('objeto', 'item', 'cantidad', 'aclaracion')

class UbicacionHistoricaForm(forms.ModelForm):
    class Meta:
        model = UbicacionHistorica
        fields= '__all__'
        exclude = ('activa', )

class CambiarUbicacionForm(forms.Form):
    ubicacion = forms.ChoiceField(choices=ubicaciones_plantables())#DEBE SER MULTIPLECHOICE
    fecha = forms.DateTimeField(initial=timezone.now, widget=XDSoftDateTimePickerInput(attrs={'autocomplete':'off'}))

class CaracteristicaForm(forms.ModelForm):
    class Meta:
        model = Caracteristica
        fields= '__all__'
        exclude = ('objeto', )

class RegarForm(forms.ModelForm):
    class Meta:
        model = Accion
        fields = ('objeto', 'aclaracion')

class IndividualizarForm(forms.Form):
    ubicacion = forms.ChoiceField(choices=ubicaciones_plantables())#DEBE SER MULTIPLECHOICE
    plantadas = forms.IntegerField(label="Cantidad Individualizadas")
    muertas = forms.IntegerField(label="Cantidad Muertas")
    finalizar = forms.BooleanField(label="Lote Completo Finalizado", required=False)