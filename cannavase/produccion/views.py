#Imports Django
from django.utils import timezone
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from core.forms import PeriodoForm
from georef.models import Ubicacion
from inventario.models import Item, EventoItem
from operadores.functions import obtener_operador
#Imports de la app
from .models import Objeto, Accion, UbicacionHistorica, Caracteristica
from .forms import ObjetoForm
from .forms import UtilizarItemForm
from .forms import UbicacionHistoricaForm, CambiarUbicacionForm
from .forms import CaracteristicaForm
from .forms import GerminarForm, AgregarSemillasForm
from .forms import EvolucionarForm
from .forms import IndividualizarForm
from .forms import RegarForm
from .functions import optimizar_objetos, acciones_validas

# Create your views here.
@permission_required('operadores.produccion')
def menu(request):
    return render(request, 'menu_produccion.html', {})

@permission_required('operadores.produccion')
def lista_objetos(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo)
    objetos = optimizar_objetos(objetos)
    #No mostramos los eliminados?
    #Mostramos lista
    return render(request, 'lista_objetos.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def lista_germinacion(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo, fase=0)
    objetos = optimizar_objetos(objetos)
    return render(request, 'lista_germinacion.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def lista_vegetacion(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo, fase=20)
    objetos = optimizar_objetos(objetos)
    return render(request, 'lista_vegetacion.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def lista_floracion(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo, fase=40)
    objetos = optimizar_objetos(objetos)
    return render(request, 'lista_floracion.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def lista_cosecha(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo, fase=50)
    objetos = optimizar_objetos(objetos)
    return render(request, 'lista_cosecha.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def lista_finalizados(request, tipo):
    objetos = Objeto.objects.filter(tipo=tipo, fase=90)
    objetos = optimizar_objetos(objetos)
    return render(request, 'lista_finalizados.html', {
        'objetos': objetos,
        'has_table': True,
    })

@permission_required('operadores.produccion')
def ver_objeto(request, objeto_id):
    objetos = Objeto.objects.all()
    objetos = optimizar_objetos(objetos)
    #Identificamos
    objeto = objetos.get(pk=objeto_id)
    #Mostramos planilla
    return render(request, 'ver_objeto.html', {
        'objeto': objeto,
        'acciones': acciones_validas(objeto),
    })

@permission_required('operadores.produccion')
def mod_objeto(request, objeto_id):
    objeto = Objeto.objects.get(pk=objeto_id)
    form = ObjetoForm(instance=objeto)
    if request.method == "POST":#Al procesar
        form = ObjetoForm(request.POST, instance=objeto)
        if form.is_valid():#si es valido
            form.save()
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Modificar " + objeto.get_tipo_display(), 'form': form, 'boton': "Modificar", })            

@permission_required('operadores.produccion')
def cargar_caracteristica(request, objeto_id=None, caracteristica_id=None):
    #Cargamos inicial
    if objeto_id:
        objeto = Objeto.objects.get(pk=objeto_id)
        caracteristica = None
    else:
        caracteristica = Caracteristica.objects.get(pk=caracteristica_id)
        objeto = caracteristica.objeto
    #generamos form
    form = CaracteristicaForm(instance=caracteristica)
    if request.method == "POST":#Al procesar
        form = CaracteristicaForm(request.POST, instance=caracteristica)
        if form.is_valid():#ssi es valido
            caracteristica = form.save(commit=False)
            caracteristica.objeto = objeto
            try:
                caracteristica.save()
            except:
                return render(request, 'extras/error.html', {
                    'titulo': 'Caracteristica Ya creada',
                    'error': "No puede volver a crear esta Caracteristica, edite la existente.",
                })
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Cargar Caracteristica", 'form': form, 'boton': "Cargar", }) 

@permission_required('operadores.produccion')
def germinar(request, semilla_id):
    semilla = Item.objects.get(pk=semilla_id)

    #Generamos form de germinacion
    form = GerminarForm(initial={
        'semilla': semilla,
    })
    if request.method == "POST":#Al procesar
        form = GerminarForm(request.POST)
        if form.is_valid():#ssi es valido
            #Obtenemos datos
            operador = obtener_operador(request)
            semilla = form.cleaned_data["semilla"]
            #Generamos lote
            objeto = form.save(commit=False)
            objeto.save()
            objeto.semillas.add(semilla)
            #Generamos evento de salida: Cant Semillas
            evento = EventoItem(item=semilla)
            evento.accion = "R"
            evento.cantidad = form.cleaned_data["cantidad"]
            evento.aclaracion = "Germinacion Lote: " + objeto.identificador
            evento.operador = operador
            evento.save()
            #Generamos accionlote: germinar (guardamos operador)
            accion = Accion(objeto=objeto)
            accion.tipo = "GER"
            accion.aclaracion = "Germinacion Semillas: " + str(semilla)
            accion.evento_item = evento
            accion.operador = operador
            accion.save()
            #Generamos cantidad germinada:
            grx100sem = [c for c in semilla.caracteristicas.all() if c.tipo == 'PXS']
            if grx100sem:#Si existe caracteristica: 'PXS' > 'Peso 100 Semillas'
                grx100sem = grx100sem[0]
                cantidad_plantas = Caracteristica(
                    objeto= objeto,
                    tipo='CNT',
                    valor= (100 / grx100sem.valor) * evento.cantidad,
                    medida='UN',
                    aclaracion = "Estimacion basada en caracteristica de Semilla",
                )
                cantidad_plantas.save()
            #Generamos UbicaciónLote
            # for ubicacion_id in form.cleaned_data["ubicaciones"]:
            #     actual = UbicacionHistorica(objeto=objeto)
            #     actual.ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
            #     actual.aclaracion = "Germinacion Lote: " + objeto.identificador + " con " + str(semilla)
            #     actual.save()
            #Enviamos a ver el lote
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Germinar Semillas", 'form': form, 'boton': "Realizar", })

@permission_required('operadores.produccion')
def agregar_semillas(request, objeto_id):
    objeto = Objeto.objects.get(pk=objeto_id)
    form = AgregarSemillasForm()
    #Al procesar
    if request.method == "POST":
        form = AgregarSemillasForm(request.POST)
        if form.is_valid():#si es valido
            operador = obtener_operador(request)
            semilla = form.cleaned_data["semilla"]
            #Agregamos la semilla:
            objeto.semillas.add(semilla)
            #Generamos evento de salida: Cant Semillas
            evento = EventoItem(item=semilla)
            evento.accion = "R"
            evento.cantidad = form.cleaned_data["cantidad"]
            evento.aclaracion = "Germinacion Lote: " + objeto.identificador
            evento.operador = operador
            evento.save()
            #Generamos accionlote: germinar (guardamos operador)
            accion = Accion(objeto=objeto)
            accion.tipo = "GER"
            accion.aclaracion = "Germinacion Adicional de Semillas: " + str(semilla)
            accion.evento_item = evento
            accion.operador = operador
            accion.save()
            #Agregamos cantidad germinada:
            grx100sem = [c for c in semilla.caracteristicas.all() if c.tipo == 'PXS']
            if grx100sem:#Si existe caracteristica: 'PXS' > 'Peso 100 Semillas'
                grx100sem = grx100sem[0]
                cantidad = Caracteristica.objects.get_or_create(
                    objeto= objeto,
                    tipo='CNT',
                    medida='UN',
                )[0]
                cantidad.valor += (100 / grx100sem.valor) * evento.cantidad
                cantidad.save()
            #mostramos objeto            
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Agregar Semillas", 'form': form, 'boton': "Agregar", })

@permission_required('operadores.produccion')
def evolucionar_objeto(request, objeto_id):
    objeto = Objeto.objects.get(pk=objeto_id)
    #Chequeamos que haya cantidad cargada
    if not objeto.caracteristicas.filter(tipo="CNT").exists():
        return render(request, 'extras/error.html', {
                'titulo': 'Cantidad no informada',
                'error': "Para generar SubLotes se debe informar cantidad de plantas.",
            })
    #Iniciamos formulario
    form = EvolucionarForm(
        instance=objeto,
        initial={
            'fecha': timezone.now(),
        }
    )
    #Generamos form
    if request.method == "POST":#Al procesar
        form = EvolucionarForm(request.POST, 
            instance=objeto,
        )
        if form.is_valid():
            objeto = Objeto.objects.get(pk=objeto_id)#Refresh
            new_object = form.save(commit=False)
            #Chequeamos que haya puesto nuevo nombre:
            if new_object.identificador == objeto.identificador:
                form.add_error("identificador", "Se debe generar un nuevo identificador.")
            if new_object.fase == objeto.fase:
                form.add_error("fase", "Se debe seleccionar una nueva fase.")
            else:#Generamos nuevo objeto:
                new_object.id = None
                #Guardamos
                new_object.save()
                #Generamos relacion
                new_object.padres.clear()
                new_object.padres.add(objeto)#Le agregamos su padre
                #Traemos las semillas:
                for sem in objeto.semillas.all():
                    new_object.semillas.add(sem)
                #Generamos las caracteristicas nuevas:
                for car in objeto.caracteristicas.all():
                    car.id = None
                    car.objeto = new_object
                    car.save()
                #Obtenemos ubicaciones:
                for ubicacion_id in form.cleaned_data["ubicaciones"]:
                #     #Obtenemos ubicacion
                    ubi = Ubicacion.objects.get(pk=ubicacion_id)
                    #Desactivamos si ya esta usado:
                    # UbicacionHistorica.objects.filter(ubicacion=ubi).update(activa=False)
                    #Generamos la nueva ubicacion:
                    actual = UbicacionHistorica(objeto=new_object)
                    actual.ubicacion = ubi
                    actual.aclaracion = "Evolucion desde Lote: " + objeto.identificador
                    actual.save()
                #Obtenemos/creamos cantidad
                car = Caracteristica.objects.get_or_create(
                    objeto = new_object,
                    tipo = 'CNT',
                    medida = 'UN',
                )[0]
                car.valor = form.cleaned_data['vivas']
                car.save()
                #Creamos la accion
                operador = obtener_operador(request)
                accion = Accion(objeto=new_object)
                accion.tipo = 'EVO'
                accion.aclaracion = "Se evolucionaron {0} Plantas de {1}, Se informaron {2} muertas.".format(str(form.cleaned_data['vivas']), objeto.get_fase_display(), str(form.cleaned_data['muertas']))
                accion.operador = operador
                accion.save()
                #Registramos las plantas muertas
                accion = Accion(objeto=new_object)
                accion.tipo = 'DEL'
                accion.cantidad = form.cleaned_data['muertas']
                accion.aclaracion = "Se informaron {0} plantas muertas durante el cambio de Estado.".format(str(accion.cantidad), )
                accion.operador = operador
                accion.save()
                #Modificamos existencia de lote padre:
                cantidad = Caracteristica.objects.filter(
                    objeto=objeto,
                    tipo = 'CNT',
                ).last()#Obtenemos cantidad del lote anterior
                if cantidad:
                    total_plantas = form.cleaned_data['vivas'] + form.cleaned_data['muertas']
                    cantidad.valor -= total_plantas
                    cantidad.save()
                    if cantidad.valor <= 0:
                        #Damos de baja el anterior:
                        objeto.activo = False
                        objeto.estado = 'T'
                        objeto.save()
                #Generamos nuevo objeto:
                return redirect('produccion:ver_objeto', objeto_id=new_object.id)                
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Evolucionar", 'form': form, 'boton': "Confirmar", }) 


@permission_required('operadores.produccion')
def trasplantar(request, objeto_id):
    objeto = Objeto.objects.get(pk=objeto_id)
    form = UbicacionHistoricaForm(
        initial={'objeto': objeto, }
    )
    #Generamos form
    if request.method == "POST":#Al procesar
        form = UbicacionHistoricaForm(request.POST)
        if form.is_valid():
            #Damos de baja todas las ubicaciones actuales:
            for ubicacion in objeto.ubicaciones_activas():
                ubicacion.activa = False
                ubicacion.save()
            #Cambiamos la ubicacion
            nueva_ubicacion = form.save()
            #generamos la accion:
            accion = Accion(objeto=objeto)
            accion.tipo = "TRP"
            accion.aclaracion = nueva_ubicacion.aclaracion
            accion.operador = obtener_operador(request)
            accion.save()
            #mostramos objeto            
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Transplantar", 'form': form, 'boton': "Realizar", })

@permission_required('operadores.produccion')
def trasplantar_ubicacion(request, ubicacion_id):
    ubicacion_actual = UbicacionHistorica.objects.get(pk=ubicacion_id)
    form = CambiarUbicacionForm()
    #Generamos form
    if request.method == "POST":#Al procesar
        form = UbicacionHistoricaForm(request.POST)
        if form.is_valid():
            objeto = ubicacion_actual.objeto
            #Desactivamos ubicacion previa:
            ubicacion_actual.activa = False
            ubicacion_actual.save()
            #Generamos nueva ubicacion:
            actual = UbicacionHistorica(objeto=objeto)
            actual.ubicacion = form.cleaned_data["ubicacion"]
            actual.fecha = form.cleaned_data["fecha"]
            actual.aclaracion = "Traspaso desde: " + str(ubicacion_actual) + " a " + str(actual.ubicacion)
            actual.save()
            #mostramos objeto            
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Transplantar " + str(ubicacion_actual), 'form': form, 'boton': "Trasplantar", })

@permission_required('operadores.produccion')
def aplicar_insumo(request, objeto_id, item_id=None):
    #Obtenemos objetos
    item = None
    objeto = Objeto.objects.get(pk=objeto_id)
    if item_id:
        item = Item.objects.get(pk=item_id)
    #generar form
    form = UtilizarItemForm(initial={
        'item': item,
        'objeto': objeto,
    })
    if request.method == "POST":#Al procesar
        form = UtilizarItemForm(request.POST)#esto genera una accion
        if form.is_valid():
            operador = obtener_operador(request)
            item = form.cleaned_data["item"]
            #Generamos evento de salida
            evento = EventoItem(item=item)
            evento.accion = "R"
            evento.cantidad = form.cleaned_data["cantidad"]
            evento.aclaracion = "Accion sobre: " + str(item)
            evento.operador = operador
            evento.save()
            #Generamos la accion
            accion = form.save(commit=False)
            accion.operador = operador
            accion.evento_item = evento
            accion.tipo = item.tipo
            accion.save()
            #vamos al visualizador
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Aplicar Insumo", 'form': form, 'boton': "Realizar", })

@permission_required('operadores.produccion')
def regar(request, objeto_id):
    objeto = Objeto.objects.get(pk=objeto_id)
    form = RegarForm(initial={'objeto': objeto})
    if request.method == "POST":#Al procesar
        form = RegarForm(request.POST)#esto genera una accion
        if form.is_valid():
            #Generamos la accion
            accion = form.save(commit=False)
            accion.tipo = 'REG'
            accion.operador = obtener_operador(request)
            accion.save()
            #vamos al visualizador
            return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Regar " + str(objeto), 'form': form, 'boton': "Confirmar", })

@permission_required('operadores.produccion')
def individualizar(request, objeto_id, ubicacion_id=None):
    lote = Objeto.objects.get(pk=objeto_id)
    form = IndividualizarForm()
    if request.method == "POST":#Al procesar
        form = IndividualizarForm(request.POST)#esto genera una accion
        if form.is_valid():#si esta todo legal:
            #Obtenemos datos:
            ubicacion = Ubicacion.objects.get(pk=form.cleaned_data["ubicacion"])
            plantadas = form.cleaned_data["plantadas"]
            muertas = form.cleaned_data["muertas"]
            operador = obtener_operador(request)
            #Creamos la accion de Individualizacion
            accion = Accion(objeto=lote)
            accion.tipo = 'IND'
            accion.aclaracion = "Se crearon {0} Plantas, Se informaron {1} muertas. Plantadas en: {2}".format(str(plantadas), str(muertas), str(ubicacion))
            accion.operador = operador
            accion.save()
            #Registramos las plantas muertas
            accion = Accion(objeto=lote)
            accion.tipo = 'DEL'
            accion.cantidad = muertas
            accion.aclaracion = "Se informaron {0} plantas muertas al individualizarlas.".format(str(muertas), )
            accion.operador = operador
            accion.save()
            #Generamos contador para identificador de plantas
            contador = lote.hijos.order_by('identificador').last()
            if contador:
                contador = int(contador.identificador.split("-")[-1])
            else:
                contador = 0
            #Creamos las plantas
            for x in range(0,plantadas):#uno por cada planta
                contador += 1#para identificador
                #Creamos la planta:
                planta = Objeto()
                planta.plan_productivo = lote.plan_productivo
                planta.tipo = "P"
                planta.identificador = lote.identificador + "-" + str(contador)
                planta.save()
                #Agregamos semillas:
                for semilla in lote.semillas.all():
                    planta.semillas.add(semilla)
                #Agregamos padre
                planta.padres.add(lote)
                #Creamos la accion:
                accion = Accion(objeto=planta)
                accion.tipo = 'IND'
                accion.aclaracion = "Se creo planta desde Lote {0}.".format(str(lote))
                accion.operador = operador
                accion.save()
                #Le cargamos la ubicacion
                #Generamos UbicaciónLote (Falta signal)
                nueva_ubicacion = UbicacionHistorica(ubicacion=ubicacion)
                nueva_ubicacion.aclaracion = "Individualizacion de lote: " + str(lote)
                nueva_ubicacion.objeto = planta
                nueva_ubicacion.save()#La signal alimenta ubicacion_actual
            #Vemos si hay que cerrar el lote.
            if ubicacion_id:
                #Desactivamos ubicacion previa:
                ubicacion_actual = UbicacionHistorica.objects.get(pk=ubicacion_id)
                ubicacion_actual.activa = False
                ubicacion_actual.save()
            if form.cleaned_data["finalizar"]:
                lote.estado = 'T'
                lote.save()
            #vamos al visualizador
            return redirect('produccion:ver_objeto', objeto_id=lote.id)
    #inicializamos form:
    return render(request, "extras/generic_form.html", {'titulo': "Individualizar: " + str(lote), 'form': form, 'boton': "Confirmar", })

@permission_required('operadores.produccion_admin')
def eliminar_objeto(request, objeto_id):
    operador = obtener_operador(request)
    objeto = Objeto.objects.get(pk=objeto_id)
    if request.method == "POST":
        #Lo marcamos como eliminado
        objeto.estado = "E"
        objeto.activo = False
        objeto.save()
        #Generamos la accion
        accion = Accion(objeto=objeto)
        accion.tipo = 'DEL'
        accion.cantidad = objeto.cantidad_plantas()
        accion.aclaracion = "El operador " + str(operador) + " informo la eliminacion del " + objeto.get_tipo_display()
        accion.operador = operador
        accion.save()
        #vamos al visualizador
        return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #Pedimos confirmar la eliminacion
    return render(request, "extras/confirmar.html", {
            'titulo': "Eliminar " + str(objeto),
            'message': "Quedara registrado su operador en esta accion.",
            'has_form': True,
        }
    )

@permission_required('operadores.produccion_admin')
def del_accion(request, accion_id):
    accion = Accion.objects.get(pk=accion_id)
    objeto = accion.objeto
    if request.method == "POST":
        #eliminamos
        accion.delete()
        #Volvemos al lote
        return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #Pedimos confirmar la eliminacion
    return render(request, "extras/confirmar.html", {
            'titulo': "Eliminar Accion",
            'message': "Quedara registrado su operador en esta accion.",
            'has_form': True,
        }
    )

@permission_required('operadores.produccion_admin')
def del_ubicacion(request, ubicacion_id):
    ubicacion = UbicacionHistorica.objects.get(pk=ubicacion_id)
    objeto = ubicacion.objeto
    if request.method == "POST":
        #eliminamos
        ubicacion.delete()
        #Volvemos al lote
        return redirect('produccion:ver_objeto', objeto_id=objeto.id)
    #Pedimos confirmar la eliminacion
    return render(request, "extras/confirmar.html", {
            'titulo': "Eliminar Ubicacion Ocupada",
            'message': "Quedara registrado su operador en esta accion.",
            'has_form': True,
        }
    )

#REPORTES
@permission_required('operadores.reportes')
def lotes_sembrados(request):
    form = PeriodoForm()
    if request.method == 'POST':
        form = PeriodoForm(request.POST)
        if form.is_valid():
            #Obtenemos valores informados por usuario
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            #Obtenemos los objetos
            lotes = Objeto.objects.filter(
                tipo='L', 
                fase=0,
                fecha__date__range=(begda, endda),
            )
            lotes = optimizar_objetos(lotes)#Optimizamos DOH
            #Mandamos reporte:
            return render(request, "reportes/lotes_sembrados.html", {
                    'lotes': lotes,
                    'has_table': True,
                }
            )
    #inicializamos form:
    return render(request, "extras/generic_form.html", {
        'titulo': "Reporte de Lotes Sembrados",
        'form': form,
        'boton': "Buscar",
    })

@permission_required('operadores.reportes')
def lotes_vegetativos(request):
    form = PeriodoForm()
    if request.method == 'POST':
        form = PeriodoForm(request.POST)
        if form.is_valid():
            #Obtenemos valores informados por usuario
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            #Obtenemos los objetos
            lotes = Objeto.objects.filter(
                tipo='L', 
                fase=20,
                fecha__date__range=(begda, endda),
            )
            lotes = optimizar_objetos(lotes)#Optimizamos DOH
            #Mandamos reporte:
            return render(request, "reportes/lotes_vegetativos.html", {
                    'lotes': lotes,
                    'has_table': True,
                }
            )
    #inicializamos form:
    return render(request, "extras/generic_form.html", {
        'titulo': "Reporte de Lotes Vegetativos",
        'form': form,
        'boton': "Buscar",
    })

@permission_required('operadores.reportes')
def lotes_floracion(request):
    form = PeriodoForm()
    if request.method == 'POST':
        form = PeriodoForm(request.POST)
        if form.is_valid():
            # Obtenemos valores informados por usuario
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            # Obtenemos los objetos
            lotes = Objeto.objects.filter(
                tipo='L',
                fase=40,
                fecha__date__range=(begda, endda),
            )
            lotes = optimizar_objetos(lotes)  # Optimizamos DOH
            # Mandamos reporte:
            return render(request, "reportes/lotes_floracion.html", {
                'lotes': lotes,
                'has_table': True,
            }
                          )
    # inicializamos form:
    return render(request, "extras/generic_form.html", {
        'titulo': "Reporte de Lotes de Floración",
        'form': form,
        'boton': "Buscar",
    })


@permission_required('operadores.reportes')
def lotes_secado(request):
    form = PeriodoForm()
    if request.method == 'POST':
        form = PeriodoForm(request.POST)
        if form.is_valid():
            # Obtenemos valores informados por usuario
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            # Obtenemos los objetos
            lotes = Objeto.objects.filter(
                tipo='L',
                fase=50,
                fecha__date__range=(begda, endda),
            )
            lotes = optimizar_objetos(lotes)  # Optimizamos DOH
            # Mandamos reporte:
            return render(request, "reportes/lotes_secado.html", {
                'lotes': lotes,
                'has_table': True,
            }
                          )
    # inicializamos form:
    return render(request, "extras/generic_form.html", {
        'titulo': "Reporte de Lotes de Secado",
        'form': form,
        'boton': "Buscar",
    })


@permission_required('operadores.reportes')
def plantas_eliminadas(request):
    form = PeriodoForm()
    if request.method == 'POST':
        form = PeriodoForm(request.POST)
        if form.is_valid():
            #Obtenemos valores informados por usuario
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            #Obtenemos los objetos
            eliminaciones = Accion.objects.filter(tipo='DEL')
            eliminaciones = eliminaciones.filter(fecha__range=(begda,endda))#Filtramos por fecha de periodo
            #Optimizamos
            eliminaciones = eliminaciones.select_related('objeto', 'operador')
            #Mandamos reporte:
            return render(request, "reportes/plantas_eliminadas.html", {
                    'eliminaciones': eliminaciones,
                    'has_table': True,
                }
            )
    #inicializamos form:
    return render(request, "extras/generic_form.html", {
        'titulo': "Reporte de Plantas Eliminadas",
        'form': form,
        'boton': "Buscar",
    })
