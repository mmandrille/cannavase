#Imports de Django
from django.utils import timezone
from django.db import models
#Import Extras
from auditlog.registry import auditlog
from tinymce.models import HTMLField
#Imports del proyeecto
from cannavase.settings import LOADDATA
from core.choices import TIPO_MEDIDA
from operadores.models import Operador
from inventario.models import Item, EventoItem
from georef.models import Ubicacion
#Imports de la app
from .choices import FASE_PLAN
from .choices import TIPO_OBJETO, ESTADO_OBJETO, TIPO_ACCION, TIPO_CARACTERISTICA

# Create your models here.
class PlanProductivo(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    descripcion = HTMLField(null=True, blank=True)
    def __str__(self):
        return self.nombre

class EventoPlan(models.Model):
    plan = models.ForeignKey(PlanProductivo, on_delete=models.CASCADE, related_name="eventos")
    fase = models.SmallIntegerField('Fase del Proceso', choices=FASE_PLAN,  default=0)
    tiempo = models.SmallIntegerField('Dias desde Inicio', default=1)#Cuantos dias desde Germinacion
    nombre = models.CharField('Nombre', max_length=200, null=True, blank=True)
    tipo = models.CharField('Tipo', max_length=3, choices=TIPO_ACCION,  default='GER')
    aclaracion = models.CharField('Aclaracion', max_length=500, null=True, blank=True)
#Ciclo Industrial

#Modelos Primarios:
class Objeto(models.Model):
    semillas = models.ManyToManyField(Item, related_name="lotes")
    padres = models.ManyToManyField('Objeto', related_name="hijos")
    plan_productivo = models.ForeignKey(PlanProductivo, on_delete=models.SET_NULL, null=True, blank=True, related_name="objetos")
    tipo = models.CharField('Tipo', max_length=3, choices=TIPO_OBJETO,  default='L')
    fase = models.SmallIntegerField('Fase del Proceso', choices=FASE_PLAN,  default=0)
    identificador = models.CharField('identificador', max_length=50, unique=True)
    fecha = models.DateTimeField('Fecha de Creacion', default=timezone.now)
    estado = models.CharField('Estado', max_length=3, choices=ESTADO_OBJETO,  default='I')
    activo = models.BooleanField('Activa', default=True)
    #Es posible administrar las plantas generalizadamente
    def __str__(self):
        return self.get_tipo_display() + " " + self.identificador
    def ubicaciones_activas(self):
        return [u for u in self.ubicacioneshistoricas.all() if u.activa]
    def ubicacion_general(self):
        ubicaciones = self.ubicaciones_activas()
        if ubicaciones:
            ubicacion = ubicaciones[0].ubicacion
            while ubicacion.ubicacion:
                ubicacion = ubicacion.ubicacion
            return ubicacion
        return 'Imposible de determinar.'
    def cantidad_plantas(self):
        cants = [c for c in self.caracteristicas.all() if c.tipo == 'CNT']
        if cants:
            return cants[0].valor

#modelos hijos
class UbicacionHistorica(models.Model):
    objeto = models.ForeignKey(Objeto, on_delete=models.SET_NULL, null=True, blank=True, related_name="ubicacioneshistoricas")
    ubicacion = models.ForeignKey(Ubicacion, on_delete=models.SET_NULL, null=True, related_name="ubicacioneshistoricas")
    fecha = models.DateTimeField('Fecha del evento', default=timezone.now)
    aclaracion = models.CharField('Aclaracion', max_length=500, null=True, blank=True)
    activa = models.BooleanField('Activa', default=True)
    def __str__(self):
        return str(self.ubicacion)

class Caracteristica(models.Model):
    objeto = models.ForeignKey(Objeto, on_delete=models.SET_NULL, null=True, blank=True, related_name="caracteristicas")
    tipo = models.CharField('Tipo', max_length=3, choices=TIPO_CARACTERISTICA,  default='ALT')
    valor = models.DecimalField('valor', max_digits=8, decimal_places=4, default=0)
    medida = models.CharField('Tipo', max_length=3 ,choices=TIPO_MEDIDA, default='THC')
    aclaracion = models.CharField('Aclaracion', max_length=500, null=True, blank=True)
    archivo = models.FileField('Archivo', upload_to='lotes/', null=True, blank=True)
    fecha = models.DateTimeField('Fecha', default=timezone.now)
    activa = models.BooleanField('Activa', default=True)
    class Meta:
        unique_together = ('objeto', 'tipo')

class Accion(models.Model):
    #Padre
    objeto = models.ForeignKey(Objeto, on_delete=models.SET_NULL, null=True, blank=True, related_name="acciones")
    #Caracteristicas
    tipo = models.CharField('Tipo', max_length=3, choices=TIPO_ACCION,  default='REG')
    cantidad = models.DecimalField('Cantidad', max_digits=10, decimal_places=3, default=0)
    evento_item = models.ForeignKey(EventoItem, on_delete=models.SET_NULL, null=True, blank=True, related_name="acciones")
    aclaracion = models.CharField('Aclaracion', max_length=500, null=True, blank=True)
    fecha = models.DateTimeField('Fecha del evento', default=timezone.now)
    operador = models.ForeignKey(Operador, on_delete=models.CASCADE, related_name="accioneslote")
    def __str__(self):
        return self.get_tipo_display() + ": " + str(self.evento_item)


if not LOADDATA:
    #Auditoria
    auditlog.register(PlanProductivo)
    auditlog.register(EventoPlan)
    #Primarios
    auditlog.register(Objeto)
    #Secundarios
    auditlog.register(UbicacionHistorica)
    auditlog.register(Caracteristica)
    auditlog.register(Accion)
    