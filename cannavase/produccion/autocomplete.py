#Imports django
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Objeto

#Definimos nuestros autocompletes
class LotesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Objeto.objects.filter(tipo="L")
        #qs = qs.filter(vigiladores=None)
        if len(self.q) > 3:
            qs = qs.filter(identificador__icontains=self.q)
            qs = qs.distinct()
        return qs

#Definimos nuestros autocompletes
class PlantasAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Objeto.objects.filter(tipo="P")
        if self.q:
            qs = qs.filter(identificador__icontains=self.q)
        return qs