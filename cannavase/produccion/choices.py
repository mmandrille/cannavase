FASE_PLAN = (
    (0, 'Etapa de Germinación'),
    (20, 'Etapa Vegetativa'),
    (40, 'Etapa de Floración'),
    (50, 'Etapa de Secado'),
    (90, 'Etapa Final'),
)

TIPO_OBJETO = (
    ('L', 'Lote'),
    ('P', 'Planta'),
    ('E', 'Envase'),
)

ESTADO_OBJETO = (
    ('I', 'En Produccion'),
    ('T', 'Transformado/Finalizado'),
    ('E', 'Eliminado'),
)

TIPO_CARACTERISTICA = (
    ('CNT', 'Cantidad'),
    ('PES', 'Peso'),
    ('DEN', 'Densidad'),
    ('TAM', 'Tamaño'),
    ('ALT', 'Altura'),
    ('NFL', 'Nivel de Floracion'),
    ('EST', 'Estado(1-10)'),
    ('LGR', 'Longitud de Raiz'),
    #Mas caracteristicas
)

TIPO_ACCION = (
    ('GER', 'Germinacion'),#Registro Inicial de item semilla a lote
    ('EVO', 'Evolucionar'),
    ('TRP', 'Trasplante'),#cambiar la ubicacion
    ('REG', 'Regado'),
    ('APL', 'Aplicacion de Insumos'),
    ('IND', 'Individualizacion'),#automaticamente genera la cantidad de plantas que se informen
    ('POD', 'Poda'),
    ('TRB', 'Seleccion Para Madre'),#Movemos a Ubicacion de Madres, la quitamos del lote (#COMO HACEMOS????)
    ('COS', 'Cosecha'),#SECADO
    ('ENV', 'Envasado'),#A partir de un grupo Automaticamente genera frasco
    ('DEL', 'Eliminacion'),
)

TIPO_ENVASE = (
    ('FRA', 'Frasco'),
    ('VAC', 'Bolsa de Vacio'),
)