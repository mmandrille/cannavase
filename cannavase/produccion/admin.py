#Imports de Django
from django.contrib import admin
#Imports de la app
from .models import PlanProductivo, EventoPlan
from .models import Objeto
from .models import UbicacionHistorica, Caracteristica, Accion

# Register your models here.
# INLINES
class EventoPlanInline(admin.TabularInline):
    model = EventoPlan
    #Mostrar los eventos en orden incremental por tiempo
    ordering = ('fase', 'tiempo')
    extra = 0

class UbicacionHistoricaInline(admin.TabularInline):
    model = UbicacionHistorica
    ordering = ('-fecha', )
    extra = 0

class CaracteristicaInline(admin.TabularInline):
    model = Caracteristica
    ordering = ('-fecha', )
    extra = 0

class AccionInline(admin.TabularInline):
    model = Accion
    ordering = ('-fecha', )
    extra = 0

#ADMINS
class PlanProductivoAdmin(admin.ModelAdmin):
    model = PlanProductivo
    search_fields = ['nombre']
    inlines = [EventoPlanInline, ]

class LoteAdmin(admin.ModelAdmin):
    model = Objeto
    ordering = ('-fecha', )
    search_fields = ['identificador']
    list_filter = ['plan_productivo']
    inlines = [UbicacionHistoricaInline, CaracteristicaInline, AccionInline]
    def queryset(self, request):
        qs = super(LoteAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(tipo="L")

class PlantaAdmin(admin.ModelAdmin):
    model = Objeto
    ordering = ('-fecha', )
    search_fields = ['identificador']
    list_filter = ['lote', 'plan_productivo']
    inlines = [UbicacionHistoricaInline, CaracteristicaInline, AccionInline]
    def queryset(self, request):
        qs = super(PlantaAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(tipo="P")

class EnvaseAdmin(admin.ModelAdmin):
    model = Objeto
    search_fields = ['identificador']
    list_filter = ['tipo', 'lote']
    inlines = [UbicacionHistoricaInline, CaracteristicaInline, AccionInline]
    def queryset(self, request):
        qs = super(EnvaseAdmin, self).queryset(request)
        return qs.filter(tipo="E")

#Registramos
admin.site.register(PlanProductivo, PlanProductivoAdmin)
admin.site.register(Objeto, LoteAdmin)
#admin.site.register(Objeto, PlantaAdmin)
#admin.site.register(Objeto, EnvaseAdmin)