#Imports django
from django.utils import timezone
#Imports del proyecto
from inventario.models import EventoItem
#Imports de la app
from .choices import TIPO_ACCION

def optimizar_objetos(objetos):
    objetos = objetos.select_related("plan_productivo")
    objetos = objetos.prefetch_related("semillas", "padres") 
    objetos = objetos.prefetch_related("acciones")
    #objetos = objetos.prefetch_related("caracteristicas")
    objetos = objetos.prefetch_related("ubicacioneshistoricas", "ubicacioneshistoricas__ubicacion")
    objetos = objetos.prefetch_related("hijos")
    return objetos

def acciones_validas(objeto):#Estaria buenisimo mejorar
    acciones = []#Tiene que ser incremental
    if objeto.tipo == "L":
        acciones += [
            ('GER', 'Agregar Semillas', '/produccion/agregar/semillas/'+str(objeto.id)),#Agregar semillas
        ]
    if objeto.tipo in ("L", "P"):#Si es Lote o planta
        acciones += [
            ('TRP', 'Trasplantar', '/produccion/transplantar/'+str(objeto.id)),#cambiar la ubicacion
            ('REG', 'Regado', '/produccion/regar/'+str(objeto.id)),
            ('APL', 'Aplicacion de Insumos', '/produccion/aplicar/'+str(objeto.id)),
            ('IND', 'Individualizacion', '/produccion/individualizar/'+str(objeto.id)),#automaticamente genera la cantidad de plantas que se informen
            ('DEL', 'Eliminacion', '/produccion/eliminar/'+str(objeto.id)),
        ]
    if objeto.tipo == "P":#Si es Planta
        #Eliminamos individualizacion (No se puede individualizar una planta xD):
        acciones = [a for a in acciones if a[0] != 'IND']
        #Agregamos las otras
        acciones += [
            ('POD', 'Poda'),
            ('TRB', 'Seleccion Para Madre'),#Movemos a Ubicacion de Madres, la quitamos del lote (#COMO HACEMOS????)
            ('COS', 'Cosecha'),#SECADO
            ('ENV', 'Envasado'),#A partir de un grupo Automaticamente genera frasco
        ]
    if objeto.tipo == "E":#Si es Envase
        acciones = [
            #definir acciones para envase
        ] 
    return acciones