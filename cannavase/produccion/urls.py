#imports django
from django.conf.urls import url
from django.urls import path
#imports extra
from dal import autocomplete
#Import de modulos personales
from . import views
from . import autocomplete

#Definimos los paths de la app
app_name = 'produccion'
urlpatterns = [
    path('', views.menu, name='menu'),
    #Lotes:
    path('lista/<str:tipo>', views.lista_objetos, name='lista_objetos'),
    path('lista_germinacion/<str:tipo>', views.lista_germinacion, name='lista_germinacion'),
    path('lista_vegetacion/<str:tipo>', views.lista_vegetacion, name='lista_vegetacion'),
    path('lista_floracion/<str:tipo>', views.lista_floracion, name='lista_floracion'),
    path('lista_cosecha/<str:tipo>', views.lista_cosecha, name='lista_cosecha'),
    path('lista_finalizados/<str:tipo>', views.lista_finalizados, name='lista_finalizados'),
    path('ver/<int:objeto_id>', views.ver_objeto, name='ver_objeto'),
    path('mod/<int:objeto_id>', views.mod_objeto, name='mod_objeto'),
    #acciones
    path('germinar/<int:semilla_id>', views.germinar, name='germinar'),
    path('agregar/semillas/<int:objeto_id>', views.agregar_semillas, name='agregar_semillas'),
    path('aplicar/<int:objeto_id>', views.aplicar_insumo, name='aplicar_insumo'),
    path('regar/<int:objeto_id>', views.regar, name='regar'),
    path('evolucionar/<int:objeto_id>', views.evolucionar_objeto, name='evolucionar_objeto'),
    path('individualizar/<int:objeto_id>', views.individualizar, name='individualizar'),
    path('individualizar/<int:objeto_id>/ubicacion/<int:ubicacion_id>', views.individualizar, name='individualizar_ubicacion'),
    path('transplantar/<int:objeto_id>', views.trasplantar, name='trasplantar'),
    path('trasplantar/ubicacion/<int:ubicacion_id>', views.trasplantar_ubicacion, name='trasplantar_ubicacion'),
    path('eliminar/<int:objeto_id>', views.eliminar_objeto, name='eliminar_objeto'),
    path('eliminar/ubicacion/<int:ubicacion_id>', views.del_ubicacion, name='del_ubicacion'),
    path('eliminar/accion/<int:accion_id>', views.del_accion, name='del_accion'),
    #caracteristicas
    path('crear/caracteristica/<int:objeto_id>', views.cargar_caracteristica, name='cargar_caracteristica'),
    path('mod/caracteristica/<int:caracteristica_id>', views.cargar_caracteristica, name='mod_caracteristica'),
    #REPORTES
    path('rep/lotes/semb', views.lotes_sembrados, name='lotes_sembrados'),
    path('rep/lotes/veg', views.lotes_vegetativos, name='lotes_vegetativos'),
    path('rep/lotes/flo', views.lotes_floracion, name='lotes_floracion'),
    path('rep/lotes/sec', views.lotes_secado, name='lotes_secado'),
    path('rep/bajas', views.plantas_eliminadas, name='plantas_eliminadas'),
    #Autocompletes
    url(r'^lotes-autocomplete/$', autocomplete.LotesAutocomplete.as_view(), name='lotes-autocomplete',),
    url(r'^plantas-autocomplete/$', autocomplete.PlantasAutocomplete.as_view(), name='plantas-autocomplete',),
]
