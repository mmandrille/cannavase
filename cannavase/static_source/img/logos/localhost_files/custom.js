$(document).ready(function() {
    // Transition effect for navbar
    $(window).scroll(function() {
      // checks if window is scrolled more than 500px, adds/removes solid class
      if($(this).scrollTop() > 3) {
          if ($('#mainNav').hasClass('show')){
              $('.navbar').addClass('navbar-color-green');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          } else {
              $('.navbar').addClass('navbar-color-grey');
              $('#img-logo-cannava').attr("src","/static/img/logos/Logo_Cannava_Solo_800px.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Black.png");
          }


          $('.navbar').addClass('navbar-light');
          $('.navbar').removeClass('navbar-dark');

          //$('.navbar-toggler-icon').css({'background-image': 'url("data:image/svg+xml;charset=utf8,%3Csvg viewBox=\'0 0 32 32\' xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath stroke=\'rgba(0,0,0, 0.7)'});
      } else {
          if ($('#mainNav').hasClass('show')){
              $('.navbar').addClass('navbar-color-green');
              $('.navbar').addClass('navbar-light');
              $('.navbar').removeClass('navbar-dark');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          } else {
              $('.navbar').removeClass('navbar-color-grey');
              $('.navbar').addClass('navbar-dark');
              $('.navbar').removeClass('navbar-light');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          }



          //$('.navbar-toggler-icon').css({'background-image': 'url("data:image/svg+xml;charset=utf8,%3Csvg viewBox=\'0 0 32 32\' xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath stroke=\'rgba(255,255,255, 0.7)'});
      }
    });

    $('#carouselExample').carousel({
        interval: 3000
    });

    $('#carouselExample').on('slide.bs.carousel', function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('.carousel-item.mr-0').length;

        if (idx >= totalItems-(itemsPerSlide-1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i=0; i<it; i++) {
                // append slides to end
                if (e.direction=="left") {
                    $('.carousel-item.mr-0').eq(i).appendTo('.carousel-inner.row');
                }
                else {
                    $('.carousel-item.mr-0').eq(0).appendTo('.carousel-inner.row');
                }
            }
        }
    });


    $('#carouselMobile').carousel({
        interval: 3000
    });


    $('#mainNav').on('shown.bs.collapse', function (){
        $('.navbar').removeClass('navbar-color-grey');
        $('.navbar').addClass('navbar-color-green');
        $('.navbar').removeClass('navbar-dark');
        $('.navbar').addClass('navbar-light');
        $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
        $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
        $('.navbar').addClass('height-navbar');
    });

    $('#mainNav').on('hidden.bs.collapse', function (){

        if($(window).scrollTop() > 3) {
            $('.navbar').addClass('navbar-color-grey');
            $('.navbar').removeClass('navbar-color-green');
            $('.navbar').removeClass('navbar-dark');
            $('.navbar').addClass('navbar-light');
            $('#img-logo-cannava').attr("src","/static/img/logos/Logo_Cannava_Solo_800px.png");
          $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Black.png");
        } else {
            $('.navbar').removeClass('navbar-color-grey');
            $('.navbar').removeClass('navbar-color-green');
            $('.navbar').addClass('navbar-dark');
              $('.navbar').removeClass('navbar-light');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
        }
        $('.navbar').removeClass('height-navbar');
    });

    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

    $(document).on('click', function (e){
        /* bootstrap collapse js adds "in" class to your collapsible element*/
        var menu_opened = $('#mainNav').hasClass('in');

        if(!$(e.target).closest('#mainNav').length &&
            !$(e.target).is('#mainNav')){
                $(".navbar-collapse").collapse('hide');
        }

    });
});



$(document).ready(function() {
    // Transition effect for navbar
    $(window).scroll(function() {
      // checks if window is scrolled more than 500px, adds/removes solid class
      if($(this).scrollTop() > 3) {
          if ($('#mainNav').hasClass('show')){
              $('.navbar').addClass('navbar-color-green');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          } else {
              $('.navbar').addClass('navbar-color-grey');
              $('#img-logo-cannava').attr("src","/static/img/logos/Logo_Cannava_Solo_800px.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Black.png");
          }


          $('.navbar').addClass('navbar-light');
          $('.navbar').removeClass('navbar-dark');

          //$('.navbar-toggler-icon').css({'background-image': 'url("data:image/svg+xml;charset=utf8,%3Csvg viewBox=\'0 0 32 32\' xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath stroke=\'rgba(0,0,0, 0.7)'});
      } else {
          if ($('#mainNav').hasClass('show')){
              $('.navbar').addClass('navbar-color-green');
              $('.navbar').addClass('navbar-light');
              $('.navbar').removeClass('navbar-dark');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          } else {
              $('.navbar').removeClass('navbar-color-grey');
              $('.navbar').addClass('navbar-dark');
              $('.navbar').removeClass('navbar-light');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
          }



          //$('.navbar-toggler-icon').css({'background-image': 'url("data:image/svg+xml;charset=utf8,%3Csvg viewBox=\'0 0 32 32\' xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath stroke=\'rgba(255,255,255, 0.7)'});
      }
    });

    $('#carouselExample').carousel({
        interval: 3000
    });

    $('#carouselExample').on('slide.bs.carousel', function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('.carousel-item.mr-0').length;

        if (idx >= totalItems-(itemsPerSlide-1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i=0; i<it; i++) {
                // append slides to end
                if (e.direction=="left") {
                    $('.carousel-item.mr-0').eq(i).appendTo('.carousel-inner.row');
                }
                else {
                    $('.carousel-item.mr-0').eq(0).appendTo('.carousel-inner.row');
                }
            }
        }
    });


    $('#carouselMobile').carousel({
        interval: 3000
    });


    $('#mainNav').on('shown.bs.collapse', function (){
        $('.navbar').removeClass('navbar-color-grey');
        $('.navbar').addClass('navbar-color-green');
        $('.navbar').removeClass('navbar-dark');
        $('.navbar').addClass('navbar-light');
        $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
        $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
        $('.navbar').addClass('height-navbar');
    });

    $('#mainNav').on('hidden.bs.collapse', function (){

        if($(window).scrollTop() > 3) {
            $('.navbar').addClass('navbar-color-grey');
            $('.navbar').removeClass('navbar-color-green');
            $('.navbar').removeClass('navbar-dark');
            $('.navbar').addClass('navbar-light');
            $('#img-logo-cannava').attr("src","/static/img/logos/Logo_Cannava_Solo_800px.png");
          $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Black.png");
        } else {
            $('.navbar').removeClass('navbar-color-grey');
            $('.navbar').removeClass('navbar-color-green');
            $('.navbar').addClass('navbar-dark');
              $('.navbar').removeClass('navbar-light');
              $('#img-logo-cannava').attr("src","/static/img/Logo_Cannava_Solo_800px_White.png");
              $('#img-button-menu').attr("src","/static/img/icons/Menu_Icon_Grey.png");
        }
        $('.navbar').removeClass('height-navbar');
    });

    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

    $(document).on('click', function (e){
        /* bootstrap collapse js adds "in" class to your collapsible element*/
        var menu_opened = $('#mainNav').hasClass('in');

        if(!$(e.target).closest('#mainNav').length &&
            !$(e.target).is('#mainNav')){
                $(".navbar-collapse").collapse('hide');
        }

    });
});



