#Imports Django
import csv
from django.utils import timezone
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from cannavase.settings import GEOPOSITION_GOOGLE_MAPS_API_KEY
from core.forms import SearchForm
from operadores.functions import obtener_operador
#Imports app
from .models import Rubro, Almacen, Variedad
from .models import Item, CaracteristicaItem, EventoItem
from .forms import RubroForm
from .forms import ItemForm, ModItemForm, SemillaForm
from .forms import CaracteristicaItemForm
from .forms import EventoItemForm, TransferirForm
from .forms import VariedadForm, AlmacenForm

# Create your views here.
@permission_required('operadores.inventario_admin')
def menu(request):
    return render(request, 'menu_inventario.html', {})

#ABM
@permission_required('operadores.inventario_admin')
def crear_rubro(request, rubro_id=None):
    rubro = None
    if rubro_id:
        rubro = Rubro.objects.get(pk=rubro_id)
    form = RubroForm(instance=rubro)
    if request.method == 'POST':
        form = RubroForm(request.POST, instance=rubro)
        if form.is_valid():
            form.save()
            return render(request, "menu_inventario.html")
    return render(request, "extras/generic_form.html", {'titulo': "Cargar Rubro", 'form': form, 'boton': "Cargar", })

#ITEMS:
@permission_required('operadores.inventario_admin')
def lista_items(request):
    items = Item.objects.all().exclude(tipo='SEM')
    items = items.select_related('rubro')
    items = items.prefetch_related('eventos')
    return render(request, 'lista_items.html', {
        'items': items,
        'has_table': True,
    })

@permission_required('operadores.inventario_admin')
def ver_item(request, item_id):
    item = Item.objects.get(pk=item_id)
    return render(request, 'ver_item.html', {'item': item, })

@permission_required('operadores.inventario_admin')
def crear_item(request, item_id=None):
    item = None
    if item_id:
        item = Item.objects.get(pk=item_id)
        form = ModItemForm(instance=item)
    else:
        form = ItemForm()
    if request.method == "POST":
        if item_id:
            form = ModItemForm(request.POST, instance=item)
            if form.is_valid():
                item = form.save()
                return redirect('inventario:ver_item', item_id=item.id)
        else:
            form = ItemForm(request.POST)
            if form.is_valid():
                item = form.save()
                evento = EventoItem(item=item)
                evento.accion = 'O'
                evento.cantidad = form.cleaned_data['cantidad']
                evento.operador = obtener_operador(request)
                evento.aclaracion = 'Carga inicial en el Sistema'
                evento.save()
                return redirect('inventario:ver_item', item_id=item.id)
    return render(request, "extras/generic_form.html", {'titulo': "Crear Item", 'form': form, 'boton': "Agregar", })

@permission_required('operadores.inventario_admin')
def crear_semilla(request, item_id=None):
    item = None
    if item_id:
        item = Item.objects.get(pk=item_id)
        form = ModItemForm(instance=item)
    else:
        form = SemillaForm()
    if request.method == "POST":
        if item_id:
            form = ModItemForm(request.POST, instance=item)
            if form.is_valid():
                item = form.save()
                return redirect('inventario:ver_item', item_id=item.id)
        else:
            form = SemillaForm(request.POST)
            if form.is_valid():
                item = form.save(commit=False)
                item.rubro_id = 1
                item.tipo = 'SEM'
                item.save()
                evento = EventoItem(item=item)
                evento.accion = 'O'
                evento.cantidad = form.cleaned_data['cantidad']
                evento.operador = obtener_operador(request)
                evento.aclaracion = 'Carga inicial en el Sistema'
                evento.save()
                return redirect('inventario:ver_item', item_id=item.id)
    return render(request, "extras/generic_form.html", {'titulo': "Cargar Semillas", 'form': form, 'boton': "Agregar", })

@permission_required('operadores.inventario_admin')
def clonar_item(request, item_id):
    #Obtenemos item
    old_item = Item.objects.get(pk=item_id)
    form = ItemForm(instance=old_item)
    #Si nos responde:
    if request.method == "POST":
        form = ModItemForm(request.POST, instance=old_item)
        if form.is_valid():
            new_item = form.save(commit=False)
            new_item.id = None
            #guardamos el item
            new_item.save()
            #Generamos el evento de origen:
            evento = EventoItem(item=new_item)
            evento.accion = 'O'
            evento.cantidad = old_item.cantidad
            evento.operador = obtener_operador(request)
            evento.aclaracion = 'Clonacion desde Objeto: ' + str(old_item)
            evento.save()
            #copiamos sus caracteristicas
            old_item = Item.objects.get(pk=item_id)
            for car in old_item.caracteristicas.all():
                car.pk = None
                car.item = new_item
                car.save()
            #Vamos a ver el item
            return redirect('inventario:ver_item', item_id=new_item.id)
    #Mostramos form
    return render(request, "extras/generic_form.html", {'titulo': "Clonar Item", 'form': form, 'boton': "Clonar", })

@permission_required('operadores.inventario_admin')
def eliminar_item(request, item_id=None):
    item = Item.objects.get(pk=item_id)
    if request.method == "POST":
        item.delete()
        return redirect('inventario:lista_items')
    #Pedimos confirmar la eliminacion
    return render(request, "extras/confirmar.html", {
            'titulo': "Eliminar " + str(item),
            'message': "Quedara registrado su operador en esta accion.",
            'has_form': True,
        }
    )

#Administracion de items
@permission_required('operadores.inventario_admin')
def cargar_caracteristica(request, item_id=None, caracteristica_id=None):
    #obtentemos item y generamos titulo
    if caracteristica_id:
        caracteristica = CaracteristicaItem.objects.get(pk=caracteristica_id)
        item = caracteristica.item
        titulo= "Modificar Caracteristica a " + str(item)
    else:
        caracteristica = None
        item = Item.objects.get(pk=item_id)
        titulo= "Cargar Caracteristica a " + str(item)
    #generamos form
    form = CaracteristicaItemForm(instance=caracteristica)
    #Si envio info
    if request.method == "POST":
        form = CaracteristicaItemForm(request.POST, instance=caracteristica)
        if form.is_valid():
            caracteristica = form.save(commit=False)
            caracteristica.item = item
            caracteristica.save()
            return redirect('inventario:ver_item', item_id=item.id)
    return render(request, "extras/generic_form.html", {'titulo': titulo, 'form': form, 'boton': "Agregar", })

@permission_required('operadores.inventario_admin')
def crear_evento(request, item_id):
    item = Item.objects.get(pk=item_id)
    form = EventoItemForm()
    if request.method == "POST":
        form = EventoItemForm(request.POST)
        if form.is_valid():
            evento = form.save(commit=False)
            evento.item = item
            evento.save()
            return redirect('inventario:ver_item', item_id=item.id)
    return render(request, "extras/generic_form.html", {'titulo': "Crear Evento del Item", 'form': form, 'boton': "Agregar", })

@permission_required('operadores.inventario_admin')
def devolver_item(request, evento_id):
    evento = EventoItem.objects.get(pk=evento_id)
    evento.devuelto = True
    evento.save()
    devuelto = evento
    devuelto.id = None
    devuelto.accion = 'I'
    devuelto.fecha = timezone.now()
    devuelto.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@permission_required('operadores.inventario_admin')
def transferir_item(request, item_id):
    item = Item.objects.get(pk=item_id)
    form = TransferirForm()
    if request.method == "POST":
        form = TransferirForm(request.POST)
        if form.is_valid():
            operador = obtener_operador(request)
            #Retiramos el item
            evento_retiro = EventoItem()
            evento_retiro.item = item
            evento_retiro.accion = 'R'
            evento_retiro.cantidad = form.cleaned_data['cantidad']
            evento_retiro.aclaracion = form.cleaned_data['aclaracion'] + " A " + str(form.cleaned_data['destino'])
            evento_retiro.operador = operador
            evento_retiro.save()
            #Ingresamos el item
            evento_ingreso = EventoItem()
            evento_ingreso.item = form.cleaned_data['destino']
            evento_ingreso.accion = 'I'
            evento_ingreso.cantidad = form.cleaned_data['cantidad']
            evento_ingreso.aclaracion = form.cleaned_data['aclaracion'] + " Deste " + str(item)
            evento_ingreso.operador = operador
            evento_ingreso.save()
            return redirect('inventario:ver_item', item_id=evento_ingreso.item.id)
    return render(request, "extras/generic_form.html", {'titulo': "Transferir "+str(item), 'form': form, 'boton': "Transferir", })

@permission_required('operadores.inventario_admin')
def del_evento(request, evento_id):
    evento = EventoItem.objects.get(pk=evento_id)
    item = evento.item
    evento.delete()
    return redirect('inventario:ver_item', item_id=item.id)

#Reporte:
@permission_required('operadores.reportes')
def stock_semillas(request):
    semillas = Item.objects.filter(tipo='SEM')
    #Optimizamos:
    semillas = semillas.prefetch_related('eventos', 'caracteristicas')
    #Cargamos valores:

    #Lanzamos reporte:
    return render(request, "reportes/stock_semillas.html", {
        'semillas': semillas,
        'has_table': True,
        }
    )

#Semillas
@permission_required('operadores.inventario_admin')
def menu_semillas(request):
    return render(request, 'semillas/menu_semillas.html', {})

@permission_required('operadores.inventario_admin')
def lista_semillas(request):
    semillas = Item.objects.filter(tipo='SEM')
    return render(request, 'semillas/lista_semillas.html', {
        'items':semillas,
        'has_table': True,
        })

@permission_required('operadores.inventario_admin')
def cargar_semilla(request, semilla_id=None):
    if semilla_id:
        semilla = Variedad.objects.get(pk=semilla_id)
        form = VariedadForm(instance=semilla)
    else:
        semilla = None
        form = VariedadForm()
    if request.method == "POST":
        form = VariedadForm(request.POST, instance=semilla)
        if form.is_valid():
            form.save()
            return redirect('inventario:lista_semillas')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Cargar Semillas", 'form': form, 'boton': "Guardar", })


@permission_required('operadores.inventario_admin')
def detalle_semilla(request, semilla_id):
    semilla = Variedad.objects.get(pk=semilla_id)
    return render(request, 'semillas/detalle_semillas.html', {'semilla': semilla})


@permission_required('operadores.inventario_admin')
def baja_semilla(request, semilla_id):
    semilla = Variedad.objects.get(pk=semilla_id)
    semilla.activo = False
    semilla.save()
    return redirect('inventario:lista_semillas')

#Semillas (Almacen)
@permission_required('operadores.inventario_admin')
def p_stock_semillas(request):
    stock = Almacen.objects.filter(activo=True)
    return render(request, 'semillas/stock_semillas.html', {'stock': stock})

@permission_required('operadores.inventario_admin')
def ingreso(request):
    form = AlmacenForm()
    if request.method == "POST":
        form = AlmacenForm(request.POST, request.FILES)
        if form.is_valid():
            peso = form.save(commit=False)
            peso.peso_disponible = peso.peso
            peso.save()
            return redirect('inventario:stock_semillas')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Stock", 'form': form, 'boton': "Guardar", })

@permission_required('operadores.inventario_admin')
def detalle_stock(request, stock_id):
    stock = Almacen.objects.get(pk=stock_id)
    return render(request, 'semillas/detalle_stock.html', {'stock': stock})

@permission_required('operadores.inventario_admin')
def mod_stock(request, stock_id):
    stock = Almacen.objects.get(pk=stock_id)
    form = AlmacenForm(instance=stock)
    if request.method == 'POST':
        form = AlmacenForm(request.POST, request.FILES, instance=stock)
        if form.is_valid():
            form.save()
            return redirect('inventario:stock_semillas')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Editar Stock", 'form': form, 'boton': "Guardar", })

@permission_required('operadores.inventario_admin')
def baja_stock(request, stock_id):
    stock = Almacen.objects.get(pk=stock_id)
    stock.activo = False
    stock.save()
    return redirect('inventario:stock_semillas')