#Imports Python
#Imports Django
from django import forms
#Imports extra
from dal import autocomplete
from tinymce.widgets import TinyMCE
#Imports del proyecto
#Imports de la app
from .models import Rubro
from .models import Item, CaracteristicaItem, EventoItem
from .models import Proveedor
from .models import Variedad, Almacen

#Definimos nuestros forms
class RubroForm(forms.ModelForm):
    class Meta:
        model = Rubro
        fields= '__all__'

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        widgets = {
            'responsable' : autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
        }

class SemillaForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        exclude = ('rubro', 'tipo',)
        widgets = {
            'responsable': autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
        }

class CaracteristicaItemForm(forms.ModelForm):
    class Meta:
        model = CaracteristicaItem
        fields= '__all__'
        exclude = ('item', )

class ModItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields= '__all__'
        widgets = {
            'responsable' : autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
        }

class EventoItemForm(forms.ModelForm):
    class Meta:
        model = EventoItem
        fields= '__all__'
        exclude = ('item', 'fecha', )
        widgets = {
            'operador' : autocomplete.ModelSelect2(url='operadores:operadores-autocomplete'),
        }

class TransferirForm(forms.Form):
    destino = forms.ModelChoiceField(
        queryset=Item.objects.all(),
        widget=autocomplete.ModelSelect2(url='inventario:items-autocomplete'),
        required=True,
    )
    cantidad = forms.DecimalField(label="Cantidad")
    aclaracion = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))


class VariedadForm(forms.ModelForm):
    class Meta:
        model = Variedad
        fields = '__all__'
        exclude = ('activo',)


class AlmacenForm(forms.ModelForm):
    class Meta:
        model = Almacen
        fields = '__all__'
        exclude = ('peso_disponible', 'peso_utilizado', 'activo',)