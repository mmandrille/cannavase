#Imports Django
from django.contrib import admin
#Imports extras

#Imports de la app
from .models import Rubro
from .models import Item, EventoItem
from .models import Variedad, Almacen

#Definimos los inlines:
class ItemInline(admin.TabularInline):
    model = Item
    fk_name = 'rubro'
    extra = 0

class EventoItemInline(admin.TabularInline):
    model = EventoItem
    fk_name = 'item'
    readonly_fields = ('item',)
    extra = 0

#Definimos nuestros modelos administrables:
class RubroAdmin(admin.ModelAdmin):
    model = Rubro
    search_fields = ['nombre']
    inlines = [ItemInline]

class ItemAdmin(admin.ModelAdmin):
    model = Item
    search_fields = ['nombre', 'responsable']
    inlines = [EventoItemInline]

# Register your models here.
admin.site.register(Rubro, RubroAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Variedad)
admin.site.register(Almacen)