#Choice Fields
TIPO_ITEM = (
    ('SAC', 'Sin Acciones'),
    ('SEM', 'Semilla'),
    ('APL', 'Aplicable'),
)

TIPO_CARACTERISTICA = (
    ('THC', 'Porcentaje THC'),
    ('CBD', 'Porcentaje CBD'),
    ('PGR', 'Poder Germinativo'),
    ('MRF', 'Morfología de la planta'),
    ('ALT', 'Altura promedio'),
    ('DFL', 'Dias a floración'),
    ('CFT', 'Certificado fitosanitario'),
    ('GEN', 'Genotipo '),
    ('FEN', 'Fenotipo '),
    ('PXS', 'Peso 100 Semillas'),
)

TIPO_EVENTO_ITEM = (
    ('O', 'Origen'),
    ('I', 'Ingreso'),
    ('R', 'Retiro'),
)

SEXO = (
    ('M', 'Macho'),
    ('H', 'Hembra'),
)