#Import standard de Djagno
from django.db import models
from django.utils import timezone
#Import Extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Imports del proyecto
from cannavase.settings import LOADDATA
from core.choices import TIPO_MEDIDA
from operadores.models import Operador
from georef.models import Ubicacion
from georef.choices import TIPO_UBICACION
from produccion.choices import TIPO_ENVASE
from comercial.models import Proveedor
#Imports de la app
from .choices import TIPO_ITEM, TIPO_CARACTERISTICA, TIPO_EVENTO_ITEM, SEXO

# Create your models here.
class Rubro(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    class Meta:
        ordering = ['nombre', ]
    def __str__(self):
        return self.nombre

class Item(models.Model):
    rubro = models.ForeignKey(Rubro, on_delete=models.CASCADE, related_name="items")
    tipo = models.CharField('Tipo', max_length=3 ,choices=TIPO_ITEM, default='SAC')
    origen = models.ForeignKey(Proveedor, on_delete=models.CASCADE, related_name="items")
    nombre = models.CharField('Nombre', max_length=200)
    identificador = models.CharField('identificador', max_length=50, null=True, blank=True)
    ubicacion = models.ForeignKey(Ubicacion, on_delete=models.CASCADE, related_name="items")
    detalle = models.CharField('detalle', max_length=400, null=True, blank=True)
    cantidad = models.DecimalField('Cantidad Inicial', max_digits=10, decimal_places=3, default=0)
    medida = models.CharField('Tipo', max_length=3 ,choices=TIPO_MEDIDA, default='GR')
    responsable = models.ForeignKey(Operador, on_delete=models.CASCADE, related_name="items")
    class Meta:
        ordering = ['rubro', 'nombre', ]
        unique_together = ['tipo', 'identificador']
    def __str__(self):
        return self.nombre + ": " + str(self.identificador)
    def creacion(self):
        origenes = [e for e in self.eventos.all() if e.accion=="O"]
        if origenes:
            return origenes[0]
    def cantidad_distribuida(self):
        return sum([e.cantidad for e in self.eventos.all() if e.accion=="R"])
    def cantidad_disponible(self):
        #No tener en cuenta la accion inicial
        return self.cantidad + sum([e.cantidad for e in self.eventos.all() if e.accion=="I"]) - sum([e.cantidad for e in self.eventos.all() if e.accion=="R"])

class CaracteristicaItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="caracteristicas")
    tipo = models.CharField('Tipo', max_length=3 ,choices=TIPO_CARACTERISTICA, default='')
    valor = models.DecimalField('valor', max_digits=8, decimal_places=4, default=0)
    medida = models.CharField('Medida', max_length=3 ,choices=TIPO_MEDIDA, default='THC')
    aclaracion = models.CharField('aclaracion', max_length=200, null=True, blank=True)

class EventoItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="eventos")
    accion = models.CharField('Accion', max_length=1 ,choices=TIPO_EVENTO_ITEM, default='I')
    cantidad = models.DecimalField('Cantidad', max_digits=10, decimal_places=3, default=0)
    aclaracion = models.CharField('aclaracion', max_length=400)
    fecha = models.DateTimeField('Fecha del evento', default=timezone.now)
    operador = models.ForeignKey(Operador, on_delete=models.CASCADE, related_name="eventositems")
    def __str__(self):
        return str(self.item) + ' (' + self.get_accion_display() + ": " + str(self.cantidad) + self.item.get_medida_display() + ")"

# Modelos para la nueva seccion de Semillas
class Proveedor(models.Model):
    nombre = models.CharField('Nombre', max_length=60)
    observaciones = models.CharField('Observaciones', max_length=400)
    def __str__(self):
        return str(self.nombre)

#Clases para seccion Semillas
class Variedad(models.Model):
    raza = models.CharField('Raza', max_length=100)
    origen = models.ForeignKey(Proveedor, on_delete=models.CASCADE, related_name='proveedores')
    poder_germinativo = models.DecimalField('Poder Germinativo', decimal_places=3, max_digits=10)
    thc = models.DecimalField('THC', decimal_places=3, max_digits=10)
    cbd = models.DecimalField('CBD', decimal_places=3, max_digits=10)
    morfologia = models.CharField('Morfología', max_length=100)
    sexo = models.CharField('Sexo', max_length=1, choices=SEXO, default='M')
    genotipo = models.CharField('Genotipo', max_length=100)
    dias_floracion = models.IntegerField('Dias de Floración')
    rendimiento = models.DecimalField('Rendimiento Esperado', decimal_places=3, max_digits=10)
    altura = models.DecimalField('Altura promedio', decimal_places=3, max_digits=10)
    observaciones = models.CharField('Observaciones', max_length=400)
    fecha_importacion = models.DateField('Fecha de Importación', default=timezone.now)
    fecha_carga = models.DateField('Fecha de Carga', default=timezone.now)
    activo = models.BooleanField('Activo', default=True)
    def __str__(self):
        return self.raza


class Almacen(models.Model):
    variedad = models.ForeignKey(Variedad, on_delete=models.CASCADE, related_name='variedades')
    codigo = models.CharField('Código del Paquete', max_length=100)
    cant_semillas = models.IntegerField('Cantidad de Semillas')
    peso = models.DecimalField('Peso en Gramos', decimal_places=3, max_digits=10)
    peso_utilizado = models.DecimalField('Peso Utilizado', decimal_places=3, max_digits=10, default=0)
    peso_disponible = models.DecimalField('Peso Disponible', decimal_places=3, max_digits=10, default=0)
    tipo_envase = models.CharField('Tipo de Envase', choices=TIPO_ENVASE, max_length=3)
    sububicacion = models.ForeignKey(Ubicacion, on_delete=models.CASCADE)
    certificado_importacion = models.ImageField(upload_to='../archivos/certificados/')
    observaciones = HTMLField()
    contenedor = models.CharField('Tipo de Contenedor', choices=TIPO_UBICACION, max_length=3)
    certificado_tecnico = models.ImageField(upload_to='../archivos/cetificados/')
    fecha_alta = models.DateField('Fecha de Alta', default=timezone.now)
    fecha_carga = models.DateField('Fecha de Carga', default=timezone.now)
    activo = models.BooleanField('Activo', default=True)



if not LOADDATA:
    #Auditoria
    auditlog.register(Item)
    auditlog.register(EventoItem)