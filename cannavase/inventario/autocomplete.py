#Imports django
from django.db.models import Q
from django.contrib.auth.models import User
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Rubro, Item

class RubrosAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Rubro.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs

class ItemsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Item.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs