#imports django
from django.conf.urls import url
from django.urls import path
#imports extra
from dal import autocomplete
#Import de modulos personales
from . import views
from . import autocomplete

#Definimos los paths de la app
app_name = 'inventario'
urlpatterns = [
    path('', views.menu, name='menu'),
    #Rubro:
    path('crear/rubro', views.crear_rubro, name='crear_rubro'),
    path('mod/rubro/<int:rubro_id>', views.crear_rubro, name='mod_rubro'),
    #Basico Items
    path('lista', views.lista_items, name='lista_items'),
    path('ver/<int:item_id>', views.ver_item, name='ver_item'),
    path('crear', views.crear_item, name='crear_item'),
    path('mod/<int:item_id>', views.crear_item, name='mod_item'),
    path('clonar/<int:item_id>', views.clonar_item, name='clonar_item'),
    path('del/<int:item_id>', views.eliminar_item, name='eliminar_item'),
    #caracteristica
    path('crear/caracteristica/<int:item_id>', views.cargar_caracteristica, name='cargar_caracteristica'),
    path('mod/caracteristica/<int:caracteristica_id>', views.cargar_caracteristica, name='mod_caracteristica'),
    #eventos:
    path('crear/evento/<int:item_id>', views.crear_evento, name='crear_evento'),
    path('devolver/<int:evento_id>', views.devolver_item, name='devolver'),
    path('transferir/<int:item_id>', views.transferir_item, name='transferir_item'),
    path('del/evento/<int:evento_id>', views.del_evento, name='del_evento'),

    #Reportes
    path('stock_semillas', views.stock_semillas, name='stock_semillas'),

    #Semillas
    path('menu_semillas', views.menu_semillas, name='menu_semillas'),
    path('lista_semillas', views.lista_semillas, name='lista_semillas'),
    path('cargar_semillas', views.crear_semilla, name='cargar_semillas'),
    path('mod_semillas/<int:semilla_id>', views.cargar_semilla, name='mod_semillas'),
    path('detalle_semilla/<int:semilla_id>', views.detalle_semilla, name='detalle_semilla'),
    path('baja_semilla/<int:semilla_id>', views.baja_semilla, name='baja_semilla'),

    #Semillas (Almacenes)
    path('p/stock_semillas', views.p_stock_semillas, name='p_stock_semillas'),
    path('p/stock_ingresar', views.ingreso, name='p_stock_ingresar'),
    path('p/stock_detalles/<int:stock_id>', views.detalle_stock, name='p_stock_detalles'),
    path('p/stock_mod/<int:stock_id>', views.mod_stock, name='p_stock_mod'),
    path('p/stock_baja/<int:stock_id>', views.baja_stock, name='p_stock_baja'),

    #Autocomplete
    url(r'^rubros-autocomplete/$', autocomplete.RubrosAutocomplete.as_view(), name='rubros-autocomplete',),
    url(r'^items-autocomplete/$', autocomplete.ItemsAutocomplete.as_view(), name='items-autocomplete',),
]