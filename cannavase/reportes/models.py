from django.db import models
from datetime import datetime
from django.utils import timezone


class Fechas(models.Model):
    aux = models.CharField(max_length=1, default='a')
    fechaDesde = models.DateField('Fecha desde', default=datetime(2019, 1, 1).strftime('%Y-%m-%d'))
    fechaHasta = models.DateField('Fecha hasta', default=timezone.now().strftime('%Y-%m-%d'))
