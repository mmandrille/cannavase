#Imports Django
from datetime import datetime
from datetime import date
from time import strptime
from django.core.files.storage import FileSystemStorage
from django.template.loader import render_to_string
from django.utils import timezone
import ssl
from weasyprint import HTML, CSS

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponse
from django.views import generic
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required

# Create your views here.
from cultivo.models import *
from .forms import InformeFechasForm
from .models import Fechas


@permission_required('operadores.reportes')
def menu(request):
    return render(request, 'menu_reportes.html', {})


class Informe(PermissionRequiredMixin, generic.View):
    permission_required = 'operadores.reportes'

    def getObjectsByDateRange(self, start, end):
        lotes = []
        semillas = LoteSemilla.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        germinacion = LoteGerminacion.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        vegetativo = LoteVegetativo.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        floracion = LoteFloracion.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        madres = LoteMadre.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        material = LoteCosechaMaterial.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        secado = LoteSecado.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        lotes.append(semillas)
        lotes.append(germinacion)
        lotes.append(vegetativo)
        lotes.append(floracion)
        lotes.append(madres)
        lotes.append(secado)
        lotes.append(material)
        return lotes

    def get(self, request):
        fechas = Fechas.objects.get(aux='a')
        fechaDesde = fechas.fechaDesde
        fechaHasta = fechas.fechaHasta
        lotes = self.getObjectsByDateRange(fechaDesde, fechaHasta)
        form = InformeFechasForm(initial={
            'fechaDesde': str(fechaDesde),
            'fechaHasta': str(fechaHasta),
        })
        context = {
            'has_table': True,
            'lotes': lotes,
            'form': form
        }
        globals().update({'fechas': None})
        return render(request, 'informe.html', context)

    def post(self, request):
        fechas = Fechas.objects.get(aux='a')
        fechaDesde = request.POST['fechaDesde']
        fechaHasta = request.POST['fechaHasta']
        try:
            fechaDesde = fechaDesde.split('/')
            fechaDesde = fechaDesde[2] + '-' + fechaDesde[1] + '-' + fechaDesde[0]
        except Exception:
            fechaDesde = fechas.fechaDesde
        try:
            fechaHasta = fechaHasta.split('/')
            fechaHasta = fechaHasta[2] + '-' + fechaHasta[1] + '-' + fechaHasta[0]
        except Exception:
            fechaHasta = fechas.fechaHasta
        lotes = self.getObjectsByDateRange(str(fechaDesde), str(fechaHasta))
        fechas.fechaDesde = str(fechaDesde)
        fechas.fechaHasta = str(fechaHasta)
        fechas.save()
        form = InformeFechasForm(initial={
            'fechaDesde': str(fechaDesde),
            'fechaHasta': str(fechaHasta),
        })
        context = {
            'has_table': True,
            'lotes': lotes,
            'form': form
        }
        return render(request, 'informe.html', context)


class GenerarPDF(generic.View):

    def getObjectsByDateRange(self, start, end):
        lotes = [[], []]
        semillas = LoteSemilla.objects.all().order_by('lote__fechaMovimiento')
        germinacion = LoteGerminacion.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        vegetativo = LoteVegetativo.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        floracion = LoteFloracion.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        madres = LoteMadre.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        material = LoteCosechaMaterial.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        secado = LoteSecado.objects.filter(lote__fechaMovimiento__range=[start, end]).order_by('lote__fechaMovimiento')
        destruccion = DestruccionPlantas.objects.filter(fecha__range=[start, end]).order_by('fecha')
        lotes[0].append(semillas)
        lotes[0].append(germinacion)
        lotes[0].append(vegetativo)
        lotes[0].append(floracion)
        lotes[0].append(madres)
        lotes[0].append(secado)
        lotes[0].append(material)
        lotes[1].append(destruccion)
        return lotes

    def get(self, request):
        # try:
        #     fecha = str(globals()['fechas'].POST['fechaHasta'])
        #     fechaFin = list(globals()['fechas'].POST['fechaHasta'].split('/'))
        #     fechaFin = fechaFin[2] + '-' + fechaFin[1] + '-' + fechaFin[0]
        # except:
        #     fecha = datetime.today().strftime('%d/%m/%Y')
        #     fechaFin = datetime.today().strftime('%Y-%m-%d')
        # try:
        #     fechaDesde = list(globals()['fechas'].POST['fechaDesde'].split('/'))
        #     fechaDesde = fechaDesde[2] + '-' + fechaDesde[1] + '-' + fechaDesde[0]
        #     fechaIni = str(globals()['fechas'].POST['fechaDesde'])
        # except:
        #     fechaDesde = '2019-01-01'
        #     fechaIni = '01/01/2019'
        # try:
        #     fechaHasta = list(globals()['fechas'].POST['fechaHasta'].split('/'))
        #     fechaHasta = fechaHasta[2] + '-' + fechaHasta[1] + '-' + fechaHasta[0]
        # except:
        #     fechaHasta = datetime.today().strftime('%Y-%m-%d')
        fechas = Fechas.objects.get(aux='a')
        fecha = str(fechas.fechaHasta).split('-')
        fecha = fecha[2] + '/' + fecha[1] + '/' + fecha[0]
        fechaIni = str(fechas.fechaDesde).split('-')
        fechaIni = fechaIni[2] + '/' + fechaIni[1] + '/' + fechaIni[0]
        lotes = self.getObjectsByDateRange(fechas.fechaDesde, fechas.fechaHasta)
        semillaTotalCant = 0
        semillaCantUtilizada = 0
        semillaCantRemanente = 0
        germinacionGramos = 0
        germinacionCantidad = 0
        vegetacionTotal = 0
        vegetacionActual = 0
        floracionTotal = 0
        floracionActual = 0
        madresTotal = 0
        madresActual = 0
        secadoTotal = 0
        secadoActual = 0
        materialTotal = 0
        materialSeco = 0
        destruccionTotal = 0
        cantSemillas = 0
        cantGerminacion = 0
        cantVegetacion = 0
        cantFloracion = 0
        cantMadres = 0
        cantSecado = 0
        cantMaterial = 0
        cantDestruccion = 0
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Semillas':
                    semillaTotalCant = semillaTotalCant + i.pesoInicial
                    semillaCantRemanente = semillaCantRemanente + i.pesoActual
                    semillaCantUtilizada = semillaCantUtilizada + (i.pesoInicial - i.pesoActual)
                    cantSemillas = cantSemillas + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Germinacion':
                    germinacionGramos = germinacionGramos + i.cantidadGramos
                    germinacionCantidad = germinacionCantidad + i.totalSemillas
                    cantGerminacion = cantGerminacion + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Vegetacion':
                    vegetacionTotal = vegetacionTotal + i.totalPlantas
                    vegetacionActual = vegetacionActual + i.stockActual
                    cantVegetacion = cantVegetacion + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Floracion':
                    floracionTotal = floracionTotal + i.totalPlantas
                    floracionActual = floracionActual + i.stockActual
                    cantFloracion = cantFloracion + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Plantas Madre':
                    madresTotal = madresTotal + i.totalPlantas
                    madresActual = madresActual + i.stockActual
                    cantMadres = cantMadres + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Secado':
                    secadoTotal = secadoTotal + i.totalPlantas
                    secadoActual = secadoActual + i.stockActual
                    cantSecado = cantSecado + 1
        for lote in lotes[0]:
            for i in lote:
                if i.lote.tipo == 'Lote de Flores Deshidratadas':
                    materialTotal = materialTotal + i.totalPlantas
                    materialSeco = materialSeco + i.pesoSeco
                    cantMaterial = cantMaterial + 1
        for i in lotes[1]:
            for j in i:
                destruccionTotal = destruccionTotal + j.cantidad
                cantDestruccion = cantDestruccion + 1
        contexto = {
            'fecha': fecha,
            'lotes': lotes[0],
            'semillaTotalCant': semillaTotalCant,
            'semillaCantUtilizada': semillaCantUtilizada,
            'semillaCantRemanente': semillaCantRemanente,
            'germinacionGramos': germinacionGramos,
            'germinacionCantidad': germinacionCantidad,
            'vegetacionTotal': vegetacionTotal,
            'vegetacionActual': vegetacionActual,
            'floracionTotal': floracionTotal,
            'floracionActual': floracionActual,
            'madresTotal': madresTotal,
            'madresActual': madresActual,
            'secadoTotal': secadoTotal,
            'secadoActual': secadoActual,
            'materialTotal': materialTotal,
            'materialSeco': materialSeco,
            'destrucciones': lotes[1],
            'destruccionTotal': destruccionTotal,
            'semillas': cantSemillas,
            'germinacion': cantGerminacion,
            'vegetacion': cantVegetacion,
            'floracion': cantFloracion,
            'madres': cantMadres,
            'secado': cantSecado,
            'material': cantMaterial,
            'destruccion': cantDestruccion,
            'fechaDesde': fechaIni,
        }
        ssl._create_default_https_context = ssl._create_unverified_context
        html_string = render_to_string('InformePDF.html', contexto)
        html = HTML(string=html_string, base_url=request.build_absolute_uri())

        target = '/tmp/Informe_' + str(fechas.fechaHasta) + '.pdf'
        name = 'Informe_' + str(fechas.fechaHasta) + '.pdf'
        html.write_pdf(target=target)
        fs = FileSystemStorage('/tmp')
        with fs.open(name) as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=' + name
            return response

