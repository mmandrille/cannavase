﻿from datetime import datetime

from django.utils import timezone
from .models import Fechas
from django import forms
from core.widgets import XDSoftDatePickerInput


class InformeFechasForm(forms.ModelForm):
    class Meta:
        model = Fechas
        fields = '__all__'
        exclude = ['aux']
        widgets = {
            'fechaDesde': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
            'fechaHasta': XDSoftDatePickerInput(attrs={'autocomplete': 'off'}),
        }
