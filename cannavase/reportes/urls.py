#imports django
from django.conf.urls import url
from django.urls import path
#imports extra
from dal import autocomplete
#Import de modulos personales
from . import views

#Definimos los paths de la app
app_name = 'reportes'
urlpatterns = [
    path('', views.menu, name='menu'),
    path('informe', views.Informe.as_view(), name='informe'),
    path('pdf', views.GenerarPDF.as_view(), name='pdf'),
]
