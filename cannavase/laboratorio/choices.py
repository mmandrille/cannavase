# Choices
ESTADO = (
    (0, 'Cuarentena'),
    (1, 'Liberada'),
    (2, 'Rechazada'),
    (3, 'Desechada'),
    (4, 'Devuelta'),
)

TIPO = (
    ('MV', 'Material Vegetal'),
    ('FO', 'Formulacion'),
)

TIPO_FORM = (
    ('AL', 'Alcohol'),
    ('HI', 'Hielo'),
)