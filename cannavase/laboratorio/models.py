from django.db import models
from django.utils import timezone

from .choices import ESTADO, TIPO_FORM, TIPO
from comercial.models import Proveedor
from produccion.models import Objeto


class MateriaPrima(models.Model):
    estado = models.IntegerField('Estado', choices=ESTADO, default=0)
    tipo = models.CharField('Tipo',choices=TIPO ,max_length=50, default='MV')
    objeto = models.ForeignKey(Objeto, on_delete=models.CASCADE, null=True, blank=True)
    formTipo = models.CharField('Tipo', choices=TIPO_FORM, default='AL', max_length=2)
    fechaVenc = models.DateTimeField('Fecha de Vencimiento', default=timezone.now)
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE, default='', null=True)
    cant = models.DecimalField('Cantidad', decimal_places=3, max_digits=10, null=False, default=0)
    codigo = models.CharField('Codigo', max_length=100, default='')