from django.apps import AppConfig
from core.functions import agregar_menu


class LaboratorioConfig(AppConfig):
    name = 'laboratorio'
    verbose_name = 'laboratorio'

    # def ready(self):
    #     agregar_menu(self)
