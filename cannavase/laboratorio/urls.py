from django.conf.urls import url
from django.urls import path

from . import views, autocomplete

app_name = 'laboratorio'
urlpatterns = [
    path('', views.menu, name='menu'),
    path('lab_list', views.MateriaPrimaList, name='lista'),
    path('lab_add', views.createMP, name='crear'),
#Autocompletes
    url(r'^objetos-autocomplete/$', autocomplete.ObjetosAutocomplete.as_view(), name='objetos-autocomplete',),
    url(r'^proveedor-autocomplete/$', autocomplete.ProveedoresAutocomplete.as_view(), name='proveedores-autocomplete',),

#API
    url(r'^api/MP_list$', views.MP_list),
    url(r'^api/MP$', views.MPAPIview.as_view()),
    url(r'^api/MP_proveedores$', views.MP_proveedores),
    url(r'^api/MP_vegetal$', views.MP_vegetal),
    path('api/MP_list/<str:codigo>', views.MP_edit),
]
