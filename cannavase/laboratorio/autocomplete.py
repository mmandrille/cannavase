﻿from dal import autocomplete
from produccion.models import Objeto
from comercial.models import Proveedor


class ObjetosAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Objeto.objects.filter(fase=90)
        if self.q:
            qs = qs.filter(identificador__icontains=self.q)
        return qs


class ProveedoresAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Proveedor.objects.all()
        if self.q:
            qs = qs.filter(identificador__icontains=self.q)
        return qs