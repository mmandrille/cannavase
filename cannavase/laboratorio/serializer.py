from rest_framework import serializers
from .models import MateriaPrima


class MateriaPrimaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MateriaPrima
        fields = '__all__'

    def create(self, validated_data):
        instance = MateriaPrima.objects.create(**validated_data)
        return instance
