from dal import autocomplete
from django import forms
from .models import MateriaPrima


class MPForm(forms.ModelForm):
    class Meta:
        model = MateriaPrima
        fields = '__all__'
        widgets = {
            'objeto': autocomplete.ModelSelect2(url='laboratorio:objetos-autocomplete'),
            'proveedor': autocomplete.ModelSelect2(url='laboratorio:proveedores-autocomplete'),
        }
