from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.defaulttags import csrf_token
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from rest_framework import mixins, generics, status
from rest_framework.decorators import api_view

from .choices import TIPO
from .forms import MPForm
from .models import MateriaPrima
from .serializer import MateriaPrimaSerializer
from comercial.models import Proveedor
from produccion.models import Objeto


def menu(request):
    return render(request, 'menu_laboratorio.html', {})


def MateriaPrimaList(request):
    materias = MateriaPrima.objects.all()
    for i in materias:
        for j in TIPO:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'materiaPrima/lista_materia.html', {
        "materias": materias,
        "has_table": True,
    })


def createMP(request):
    form = MPForm()
    if request.method == "POST":
        form = MPForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('laboratorio:lista')
    return render(request, "extras/generic_form.html", {
        'titulo': 'Ingresar Materia Prima',
        'form': form,
        'boton': 'Guardar',
    })

@api_view(['GET', 'POST', 'DELETE'])
def MP_list(request):
    materias = MateriaPrima.objects.all().values()
    return JsonResponse({'materiasPrimas': list(materias)})


class MPAPIview(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = "pk"
    serializer_class = MateriaPrimaSerializer

    def get_queryset(self):
        qs = MateriaPrima.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                Q(pk=query)
            ).distinct()
        return qs

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


@api_view(['GET', 'POST', 'DELETE'])
def MP_proveedores(request):
    proveedores = Proveedor.objects.all().values()
    return JsonResponse({'proveedores': list(proveedores)})


@api_view(['GET', 'POST', 'DELETE'])
def MP_vegetal(request):
    vegetales = Objeto.objects.filter(fase=90).values()
    return JsonResponse({'vegetales': list(vegetales)})

@api_view(['GET', 'PUT', 'DELETE'])
def MP_edit(request, codigo):
    try:
        if request.method == 'GET':
            materia = MateriaPrima.objects.filter(codigo=codigo).values()
            return JsonResponse({'materia': list(materia)})
        elif request.method == 'PUT':
            materia = MateriaPrima.objects.filter(codigo=codigo).values()
            selializer = MateriaPrimaSerializer(data=materia)
            if selializer.is_valid():
                selializer.save()
                return JsonResponse({'message': 'La materia prima se actualizo exitosamente!'}, status=status.HTTP_201_CREATED)
    except MateriaPrima.DoesNotExist:
        return JsonResponse({'message': 'La materia prima no existe'}, status=status.HTTP_404_NOT_FOUND)