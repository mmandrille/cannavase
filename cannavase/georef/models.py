# Realizamos imports de Django
from django.db import models
from django.utils import timezone
# Imports de paquetes extras
from auditlog.registry import auditlog
from tinymce.models import HTMLField
# Imports del proyecto
from cannavase.settings import LOADDATA
from core.choices import TIPO_MEDIDA
# Imports de la app
from .choices import *


# Create your models here.
class Nacionalidad(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    contacto = HTMLField(null=True, blank=True)

    class Meta:
        ordering = ['nombre', ]
        verbose_name_plural = 'Nacionalidades'

    def __str__(self):
        return self.nombre

    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "contacto": self.contacto,
        }


class Provincia(models.Model):
    nacion = models.ForeignKey(Nacionalidad, on_delete=models.CASCADE, related_name="provincias")
    nombre = models.CharField('Nombre', max_length=100, unique=True)
    id_infragob = models.CharField('id from infra.datos.gob.ar', max_length=20, unique=True, null=True, blank=True)

    class Meta:
        ordering = ['nombre', ]
        verbose_name_plural = 'Provincias'

    def __str__(self):
        return self.nombre

    def as_dict(self):
        return {
            "id": self.id,
            "provincia_id": self.id,
            "nombre": self.nombre,
        }


class Departamento(models.Model):  # Departamento
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE, related_name="departamentos")
    nombre = models.CharField('Nombre', max_length=100)
    id_infragob = models.CharField('id from infra.datos.gob.ar', max_length=20, unique=True, null=True, blank=True)

    class Meta:
        ordering = ['nombre', ]
        verbose_name_plural = 'Departamentos'
        unique_together = ('provincia', 'nombre')

    def __str__(self):
        return self.nombre

    def as_dict(self):
        return {
            "id": self.id,
            "provincia_id": self.provincia.id,
            "departamento_id": self.id,
            "nombre": self.nombre,
        }


class Localidad(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, related_name="localidades")
    nombre = models.CharField('Nombre', max_length=100)
    codigo_postal = models.CharField('Codigo Postal', max_length=100, blank=True, null=True)
    id_infragob = models.CharField('id from infra.datos.gob.ar', max_length=20, unique=True, null=True, blank=True)
    latitud = models.DecimalField('latitud', max_digits=12, decimal_places=10, null=True)
    longitud = models.DecimalField('longitud', max_digits=12, decimal_places=10, null=True)

    class Meta:
        ordering = ['nombre', ]
        verbose_name_plural = 'Localidades'
        unique_together = ('departamento', 'nombre')

    def __str__(self):
        return self.nombre + ' (' + str(self.departamento.provincia.nombre) + ')'

    def as_dict(self):
        return {
            "id": self.id,
            "departamento_id": self.departamento.id,
            "localidad_id": self.id,
            "nombre": self.nombre,
            "codigo_postal": self.codigo_postal,
        }


class Barrio(models.Model):
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE, related_name="barrios")
    nombre = models.CharField('Nombre', max_length=100)
    id_infragob = models.CharField('id from infra.datos.gob.ar', max_length=20, unique=True, null=True, blank=True)

    class Meta:
        ordering = ['nombre', ]
        unique_together = ('localidad', 'nombre')
        verbose_name_plural = 'Barrios'

    def __str__(self):
        return self.nombre

    def as_dict(self):
        return {
            "id": self.id,
            "localidad_id": self.localidad.id,
            "barrio_id": self.id,
            "nombre": self.nombre,
        }


# Particularidades:
class Ubicacion(models.Model):
    ubicacion = models.ForeignKey('Ubicacion', on_delete=models.SET_NULL, related_name="ubicaciones", null=True,
                                  blank=True)
    tipo = models.CharField('Tipo de Recipiente', max_length=3, choices=TIPO_SUBUBICACION, default='BDJ')
    # Basico
    identificador = models.CharField('Identificador - Nombre', max_length=100, unique=True)
    modelo = models.CharField('Modelo', max_length=100, blank=True, null=True)
    marca = models.CharField('Marca', max_length=100, blank=True, null=True)
    aclaracion = HTMLField(null=True, blank=True)
    capacidadLitros = models.DecimalField('Capacidad (Litros)', max_digits=10, decimal_places=3, default=0.000,
                                          null=True)
    capacidad = models.IntegerField('Capacidad (Plantas / Si corresponde)', default=0, null=True, blank=True)
    # Localizacion_
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE, related_name="ubicaciones", null=True,
                                  blank=True)
    barrio = models.ForeignKey(Barrio, on_delete=models.SET_NULL, related_name="ubicaciones", null=True, blank=True)
    calle = models.CharField('Calle', max_length=200, null=True, blank=True)
    numero = models.CharField('Numero', max_length=100, null=True, blank=True)
    telefono = models.CharField('Telefono', max_length=50, default='+549388', null=True, blank=True)
    latitud = models.DecimalField('latitud', max_digits=12, decimal_places=10, null=True)
    longitud = models.DecimalField('longitud', max_digits=12, decimal_places=10, null=True)

    def __str__(self):
        return self.get_tipo_display() + ': ' + self.identificador

    def ocupaciones(self):
        return sum([1 for u in self.ubicacioneshistoricas.all() if u.activa])

    def ocupada(self):
        if self.ocupaciones():
            return True

    def situacion(self):
        if self.ocupada():
            return "(Ocupada)"
        else:
            return "(Libre)"

    def disponible(self):
        if self.capacidad > self.ocupaciones():
            return True


class Condicion(models.Model):
    ubicacion = models.ForeignKey(Ubicacion, on_delete=models.CASCADE, related_name="condiciones")
    tipo = models.CharField('Tipo', max_length=3, choices=TIPO_CONDICION, default='TMP')
    valor = models.DecimalField('valor', max_digits=8, decimal_places=4)
    medida = models.CharField('Tipo', max_length=3, choices=TIPO_MEDIDA, default='UN')
    fecha = models.DateTimeField('Fecha del evento', default=timezone.now)
    origen = models.CharField('Origen Dato', max_length=100)


# Nuevas Ubicaciones
class Predio(models.Model):
    codigo = models.CharField("Identificador", max_length=20, null=False, blank=False, unique=True)
    nombre = models.CharField("Nombre", max_length=50, null=False, blank=False)
    tipo = models.CharField("Tipo", max_length=3, choices=TIPO_PREDIO)
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE)
    direccion = models.CharField("Direccion", max_length=50, null=False, blank=False)
    # georef = models.Q()
    observaciones = HTMLField(null=True, blank=True)

    def __str__(self):
        return self.tipo + ' - ' + self.nombre


class Instalacion(models.Model):
    predio = models.ForeignKey(Predio, on_delete=models.CASCADE)
    codigo = models.CharField("Identificador", max_length=20, null=False, blank=False, unique=True)
    nombre = models.CharField("Nombre", max_length=50, null=False, blank=False)
    tipo = models.CharField("Tipo", max_length=3, choices=TIPO_INSTALACION)
    observaciones = HTMLField(null=True, blank=True)

    def __str__(self):
        return self.tipo + ' - ' + self.nombre


class Division(models.Model):
    intalacion = models.ForeignKey(Instalacion, on_delete=models.CASCADE)
    codigo = models.CharField("Identificador", max_length=20, null=False, blank=False, unique=True)
    nombre = models.CharField("Nombre", max_length=50, null=False, blank=False)
    tipo = models.CharField("Tipo", max_length=3, choices=TIPO_DIVISION)
    observaciones = HTMLField(null=True, blank=True)

    def __str__(self):
        return self.tipo + ' - ' + self.nombre


class Almacen(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE)
    codigo = models.CharField("Identificador", max_length=20, null=False, blank=False, unique=True)
    nombre = models.CharField("Nombre", max_length=50, null=False, blank=False)
    tipo = models.CharField("Tipo", max_length=3, choices=TIPO_ALMACEN)
    observaciones = HTMLField(null=True, blank=True)

    def __str__(self):
        return self.tipo + ' - ' + self.nombre


class Recipiente(models.Model):
    codigo = models.CharField("Identificador", max_length=20, null=False, blank=False, unique=True)
    nombre = models.CharField("Nombre", max_length=50, null=False, blank=False)
    tipo = models.CharField("Tipo", max_length=3, choices=TIPO_RECIPIENTE)
    modelo = models.CharField('Modelo', max_length=100, blank=True, null=True)
    marca = models.CharField('Marca', max_length=100, blank=True, null=True)
    litros = models.DecimalField('Capacidad (Litros)', max_digits=10, decimal_places=3, default=0.000,
                                 null=True)
    cant = models.IntegerField('Capacidad (Plantas / Si corresponde)', default=0, null=True, blank=True)
    observaciones = HTMLField(null=True, blank=True)

    def __str__(self):
        return self.tipo + ' - ' + self.nombre


if not LOADDATA:
    # Auditoria
    auditlog.register(Nacionalidad)
    auditlog.register(Provincia)
    auditlog.register(Departamento)
    auditlog.register(Localidad)
    auditlog.register(Barrio)
    auditlog.register(Ubicacion)
