#Imports Django
from django.core.cache import cache
#Imports de la app
from .models import Nacionalidad, Ubicacion

#Definimos funciones
def obtener_argentina():
    argentina = cache.get("argentina")
    if not argentina:
        argentina = Nacionalidad.objects.filter(nombre__icontains="Argentina").first()
        if not argentina:
            argentina = Nacionalidad(nombre="Argentina")
            argentina.save()
        cache.set("argentina", argentina)
    return argentina

def germinadores_disponibles():
    try:#Protegemos de posibles errores para migrations
        #Definimos tipos plantables:
        germinadores = ['BDJ', 'BDM', 'MAC']
        ubicaciones = Ubicacion.objects.filter(tipo__in=germinadores)
        #Excluimos las que estan en uso:
        ubicaciones = ubicaciones.exclude(ubicacioneshistoricas__activa=True)
        #Debemos devolver tipo choice
        return [(u.id,u.get_tipo_display() +': '+u.identificador) for u in ubicaciones]
    except:
        return []

def ubicaciones_plantables():
    try:#Protegemos de posibles errores para migrations
        #Definimos tipos plantables:
        plantables = ['BDJ', 'BDM', 'MAC', 'INV', 'MAC', 'IND', 'OUT', 'PAR']
        ubicaciones = Ubicacion.objects.filter(tipo__in=plantables)
        #Excluimos las que estan en uso:
        #ubicaciones = ubicaciones.exclude(ubicacioneshistoricas__activa=True)
        #Debemos devolver tipo choice
        return [(u.id,u.get_tipo_display() +': '+u.identificador+' '+u.situacion()) for u in ubicaciones]
    except:
        return []