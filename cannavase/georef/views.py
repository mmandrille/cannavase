# Import Python Standard
# Imports de Django
from django.db.models import Sum
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
# Imports del proyecto
from cannavase.settings import GEOPOSITION_GOOGLE_MAPS_API_KEY
from core.forms import UploadCsvWithPass
from core.functions import is_related
# Imports de la app
from .choices import tipos_ubicaciones, tipos_sububicaciones
from .models import *
from .forms import *
# Import API
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializer import *


# Create your views here.
@permission_required('operadores.georef_admin')
def menu(request):
    return render(request, 'menu_georef.html', {})


# Predio

@permission_required('operadores.georef_admin')
def lista_predio(request):
    predios = Predio.objects.all()
    for i in predios:
        for j in TIPO_PREDIO:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'lista_predio.html', {
        'predios': predios,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_predio(request, predio_id=None):
    predio = None
    if predio_id:
        predio = Predio.objects.get(pk=predio_id)
    form = PredioForm(instance=predio)
    if request.method == "POST":
        form = PredioForm(request.POST, instance=predio)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_predio')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Establecimiento / Predio", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_predio(request, predio_id):
    predio = Predio.objects.get(pk=predio_id)
    predio.delete()
    return redirect('georef:lista_predio')


# Instalaciones

@permission_required('operadores.georef_admin')
def lista_instalaciones(request):
    instalaciones = Instalacion.objects.all()
    for i in instalaciones:
        for j in TIPO_INSTALACION:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'lista_instalaciones.html', {
        'instalaciones': instalaciones,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_instalacion(request, instalacion_id=None):
    instalacion = None
    if instalacion_id:
        instalacion = Instalacion.objects.get(pk=instalacion_id)
    form = InstalacionForm(instance=instalacion)
    if request.method == "POST":
        form = InstalacionForm(request.POST, instance=instalacion)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_instalaciones')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Instalacion", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_instalacion(request, instalacion_id):
    instalacion = Instalacion.objects.get(pk=instalacion_id)
    instalacion.delete()
    return redirect('georef:lista_instalaciones')


# Divisiones

@permission_required('operadores.georef_admin')
def lista_divisiones(request):
    divisiones = Division.objects.all()
    for i in divisiones:
        for j in TIPO_DIVISION:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'lista_divisiones.html', {
        'divisiones': divisiones,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_division(request, division_id=None):
    division = None
    if division_id:
        division = Division.objects.get(pk=division_id)
    form = DivisionForm(instance=division)
    if request.method == "POST":
        form = DivisionForm(request.POST, instance=division)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_divisiones')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Sub-Instalacion / Division", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_division(request, division_id):
    division = Division.objects.get(pk=division_id)
    division.delete()
    return redirect('georef:lista_divisiones')


# Almacenes

@permission_required('operadores.georef_admin')
def lista_almacenes(request):
    almacenes = Almacen.objects.all()
    for i in almacenes:
        for j in TIPO_ALMACEN:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'lista_almacenes.html', {
        'almacenes': almacenes,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_almacen(request, almacen_id=None):
    almacen = None
    if almacen_id:
        almacen = Almacen.objects.get(pk=almacen_id)
    form = AlmacenForm(instance=almacen)
    if request.method == "POST":
        form = AlmacenForm(request.POST, instance=almacen)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_almacenes')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Almacen", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_almacen(request, almacen_id):
    almacen = Almacen.objects.get(pk=almacen_id)
    almacen.delete()
    return redirect('georef:lista_almacenes')


# Recipientes

@permission_required('operadores.georef_admin')
def lista_recipientes(request):
    recipientes = Recipiente.objects.all()
    for i in recipientes:
        for j in TIPO_RECIPIENTE:
            if i.tipo == j[0]:
                i.tipo = j[1]
    return render(request, 'lista_recipientes.html', {
        'recipientes': recipientes,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_recipiente(request, recipiente_id=None):
    recipiente = None
    if recipiente_id:
        recipiente = Recipiente.objects.get(pk=recipiente_id)
    form = RecipienteForm(instance=recipiente)
    if request.method == "POST":
        form = RecipienteForm(request.POST, instance=recipiente)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_recipientes')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Recipiente", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_recipiente(request, recipiente_id):
    recipiente = Recipiente.objects.get(pk=recipiente_id)
    recipiente.delete()
    return redirect('georef:lista_recipientes')


# Vistas para API
# Predio
class PredioList(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PredioSerializer

    def get_queryset(self):
        queryset = Predio.objects.all()
        codigo = self.request.query_params.get('codigo')
        id = self.request.query_params.get('id')
        if codigo is not None:
            queryset = queryset.filter(codigo=codigo)
        if id is not None:
            queryset = queryset.filter(id=id)
        return queryset


class PredioCreate(generics.CreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Predio.objects.all()
    serializer_class = PredioSerializer


class PredioUpdate(generics.UpdateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PredioSerializer

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Predio.objects.get(id=id)
        except Predio.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        snippet = self.get_object()
        serializer = PredioSerializer(snippet)
        return Response(serializer.data)

    def put(self, request):
        snippet = self.get_object()
        serializer = PredioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PredioDelete(generics.DestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PredioSerializer

    def delete(self, request, format=None):
        predio = self.get_object()
        predio.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Predio.objects.get(id=id)
        except Predio.DoesNotExist:
            raise Http404


# Instalaciones
class InstalacionList(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = InstalacionSerializer

    def get_queryset(self):
        queryset = Instalacion.objects.all()
        codigo = self.request.query_params.get('codigo')
        if codigo is not None:
            queryset = queryset.filter(codigo=codigo)
        return queryset


class InstalacionCreate(generics.CreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Instalacion.objects.all()
    serializer_class = InstalacionSerializer


class InstalacionUpdate(generics.UpdateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = InstalacionSerializer

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Instalacion.objects.get(id=id)
        except Instalacion.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        snippet = self.get_object()
        serializer = InstalacionSerializer(snippet)
        return Response(serializer.data)

    def put(self, request):
        snippet = self.get_object()
        serializer = InstalacionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InstalacionDelete(generics.DestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = InstalacionSerializer

    def delete(self, request, format=None):
        instalacion = self.get_object()
        instalacion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Instalacion.objects.get(id=id)
        except Instalacion.DoesNotExist:
            raise Http404


# Divisiones
class DivisionList(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DivisionSerializer

    def get_queryset(self):
        queryset = Division.objects.all()
        codigo = self.request.query_params.get('codigo')
        if codigo is not None:
            queryset = queryset.filter(codigo=codigo)
        return queryset


class DivisionCreate(generics.CreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer


class DivisionUpdate(generics.UpdateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DivisionSerializer

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Division.objects.get(id=id)
        except Division.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        snippet = self.get_object()
        serializer = DivisionSerializer(snippet)
        return Response(serializer.data)

    def put(self, request):
        snippet = self.get_object()
        serializer = DivisionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DivisionDelete(generics.DestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DivisionSerializer

    def delete(self, request, format=None):
        division = self.get_object()
        division.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Division.objects.get(id=id)
        except Division.DoesNotExist:
            raise Http404


# Almacenes
class AlmacenList(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AlmacenSerializer

    def get_queryset(self):
        queryset = Almacen.objects.all()
        codigo = self.request.query_params.get('codigo')
        if codigo is not None:
            queryset = queryset.filter(codigo=codigo)
        return queryset


class AlmacenCreate(generics.CreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Almacen.objects.all()
    serializer_class = AlmacenSerializer


class AlmacenUpdate(generics.UpdateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AlmacenSerializer

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Almacen.objects.get(id=id)
        except Almacen.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        snippet = self.get_object()
        serializer = AlmacenSerializer(snippet)
        return Response(serializer.data)

    def put(self, request):
        snippet = self.get_object()
        serializer = AlmacenSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AlmacenDelete(generics.DestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AlmacenSerializer

    def delete(self, request, format=None):
        almacen = self.get_object()
        almacen.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Almacen.objects.get(id=id)
        except Almacen.DoesNotExist:
            raise Http404


# Recipientes
class RecipienteList(generics.ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = RecipienteSerializer

    def get_queryset(self):
        queryset = Recipiente.objects.all()
        codigo = self.request.query_params.get('codigo')
        if codigo is not None:
            queryset = queryset.filter(codigo=codigo)
        return queryset


class RecipienteCreate(generics.CreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Recipiente.objects.all()
    serializer_class = RecipienteSerializer


class RecipienteUpdate(generics.UpdateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = RecipienteSerializer

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Recipiente.objects.get(id=id)
        except Recipiente.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        snippet = self.get_object()
        serializer = RecipienteSerializer(snippet)
        return Response(serializer.data)

    def put(self, request):
        snippet = self.get_object()
        serializer = RecipienteSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RecipienteDelete(generics.DestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = RecipienteSerializer

    def delete(self, request, format=None):
        recipiente = self.get_object()
        recipiente.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        try:
            id = self.request.query_params.get('id')
            return Recipiente.objects.get(id=id)
        except Recipiente.DoesNotExist:
            raise Http404









# Vistas obsoletas

@permission_required('operadores.georef_admin')
def submenu(request):
    return render(request, 'submenu_georef.html', {})


# Nacionalidades
@permission_required('operadores.georef_admin')
def lista_nacionalidades(request):
    nacionalidades = Nacionalidad.objects.all()
    return render(request, 'lista_nacionalidades.html', {
        'nacionalidades': nacionalidades,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_nacionalidad(request, nacionalidad_id=None):
    nacionalidad = None
    if nacionalidad_id:
        nacionalidad = Nacionalidad.objects.get(pk=nacionalidad_id)
    form = NacionalidadForm(instance=nacionalidad)
    if request.method == "POST":
        form = NacionalidadForm(request.POST, instance=nacionalidad)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_nacionalidades')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Crear Nacionalidad", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_nacionalidad(request, nacionalidad_id):
    nacionalidad = Nacionalidad.objects.get(pk=nacionalidad_id)
    if is_related(nacionalidad):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Nacionalidad',
            'error': "La nacionalidad no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        nacionalidad.delete()
    return redirect('georef:lista_nacionalidades')


# Provincias
@permission_required('operadores.georef_admin')
def lista_provincias(request):
    provincias = Provincia.objects.all()
    provincias = provincias.select_related('nacion')
    return render(request, 'lista_provincias.html', {
        'provincias': provincias,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_provincia(request, provincia_id=None):
    provincia = None
    if provincia_id:
        provincia = Provincia.objects.get(pk=provincia_id)
    form = ProvinciaForm(instance=provincia)
    if request.method == "POST":
        form = ProvinciaForm(request.POST, instance=provincia)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_provincias')
    return render(request, "extras/generic_form.html", {'titulo': "Crear Provincia", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_provincia(request, provincia_id):
    provincia = Provincia.objects.get(pk=provincia_id)
    if is_related(provincia):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Provincia',
            'error': "La Provincia no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        provincia.delete()
    return redirect('georef:lista_provincias')


# Departamentos
@permission_required('operadores.georef_admin')
def lista_departamentos(request):
    departamentos = Departamento.objects.all()
    departamentos = departamentos.select_related('provincia', 'provincia__nacion')
    return render(request, 'lista_departamentos.html', {
        'departamentos': departamentos,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_departamento(request, departamento_id=None):
    departamento = None
    if departamento_id:
        departamento = Departamento.objects.get(pk=departamento_id)
    form = DepartamentoForm(instance=departamento)
    if request.method == "POST":
        form = DepartamentoForm(request.POST, instance=departamento)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_departamentos')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Crear Departamento", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_departamento(request, departamento_id):
    departamento = Departamento.objects.get(pk=departamento_id)
    if is_related(departamento):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Departamento',
            'error': "El Departamento no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        departamento.delete()
    return redirect('georef:lista_departamentos')


# Localidad
@permission_required('operadores.georef_admin')
def lista_localidades(request):
    localidades = Localidad.objects.all()
    localidades = localidades.select_related('departamento', 'departamento__provincia',
                                             'departamento__provincia__nacion')
    return render(request, 'lista_localidades.html', {
        'localidades': localidades,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_localidad(request, localidad_id=None):
    localidad = None
    if localidad_id:
        localidad = Localidad.objects.get(pk=localidad_id)
    form = LocalidadForm(instance=localidad)
    if request.method == "POST":
        form = LocalidadForm(request.POST, instance=localidad)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_localidades')
    return render(request, "extras/generic_form.html", {'titulo': "Crear Localidad", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_localidad(request, localidad_id):
    localidad = Localidad.objects.get(pk=localidad_id)
    if is_related(localidad):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Localidad',
            'error': "La Localidad no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        localidad.delete()
    return redirect('georef:lista_localidades')


# Barrios
@permission_required('operadores.georef_admin')
def lista_barrios(request):
    barrios = Barrio.objects.all()
    barrios = barrios.select_related(
        'localidad', 'localidad__departamento',
        'localidad__departamento__provincia',
        'localidad__departamento__provincia__nacion')
    return render(request, 'lista_barrios.html', {
        'barrios': barrios,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def crear_barrio(request, barrio_id=None):
    barrio = None
    if barrio_id:
        barrio = Barrio.objects.get(pk=barrio_id)
    form = BarrioForm(instance=barrio)
    if request.method == "POST":
        form = BarrioForm(request.POST, instance=barrio)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_barrios')
    return render(request, "extras/generic_form.html", {'titulo': "Crear Barrio", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def delete_barrio(request, barrio_id):
    barrio = Barrio.objects.get(pk=barrio_id)
    if is_related(barrio):
        return render(request, 'extras/error.html', {
            'titulo': 'Eliminar Barrio',
            'error': "El Barrio no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
        })
    else:
        barrio.delete()
    return redirect('georef:lista_barrios')


# Ubicaciones
@permission_required('operadores.georef_admin')
def lista_ubicaciones(request, tipo=None):
    # Definimos variables necesarias
    ubicaciones = Ubicacion.objects.all()
    ubicaciones = ubicaciones.filter(tipo__in=tipos_ubicaciones)
    # Filtramos
    if tipo:
        ubicaciones = ubicaciones.filter(tipo=tipo)
    # Optimizamos
    ubicaciones = ubicaciones.select_related('localidad', 'ubicacion')
    # Lanzamos listado
    return render(request, 'lista_ubicaciones.html', {
        'ubicaciones': ubicaciones,
        'has_table': True,
        'tipo': tipo,
    })


@permission_required('operadores.georef_admin')
def lista_sububicaciones(request, tipo=None):
    # Definimos variables necesarias
    ubicaciones = Ubicacion.objects.all()
    ubicaciones = ubicaciones.filter(tipo__in=tipos_sububicaciones)
    # Filtramos
    if tipo:
        ubicaciones = ubicaciones.filter(tipo=tipo)
    # Optimizamos
    ubicaciones = ubicaciones.select_related('localidad', 'ubicacion')
    # Lanzamos listado
    return render(request, 'lista_sububicaciones.html', {
        'ubicaciones': ubicaciones,
        'has_table': True,
        'tipo': tipo,
    })


@permission_required('operadores.georef_admin')
def crear_ubicacion(request, ubicacion_id=None):
    ubicacion = None
    if ubicacion_id:
        ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
    form = UbicacionForm(instance=ubicacion)
    if request.method == "POST":
        form = UbicacionForm(request.POST, instance=ubicacion)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_ubicaciones')
    return render(request, "extras/generic_form.html", {'titulo': "Crear Ubicacion", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def crear_sububicacion(request, ubicacion_id=None):
    ubicacion = None
    if ubicacion_id:
        ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
    form = SubUbicacionForm(instance=ubicacion)
    if request.method == "POST":
        form = SubUbicacionForm(request.POST, instance=ubicacion)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_sububicaciones')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Ingresar Recipiente", 'form': form, 'boton': "Crear", })


@permission_required('operadores.georef_admin')
def ver_ubicacion(request, ubicacion_id=None):
    # Optimizamos
    ubicacion = Ubicacion.objects.select_related('localidad', 'barrio')
    # Traemos la correspondiente
    ubicacion = ubicacion.get(pk=ubicacion_id)
    # Mostramos ubicacion:
    return render(request, 'ver_ubicacion.html', {
        'ubicacion': ubicacion,
        'has_table': True,
    })


@permission_required('operadores.georef_admin')
def copiar_ubicacion(request, ubicacion_id):
    # Obtenemos ubicacion y blanqueamos datos necesarios
    ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
    ubicacion.id = None
    # Generamos form
    form = UbicacionForm(instance=ubicacion)
    if request.method == "POST":
        form = UbicacionForm(request.POST, instance=ubicacion)
        if form.is_valid():
            form.save()
            return redirect('georef:lista_ubicaciones')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Copiar Ubicacion", 'form': form, 'boton': "Copiar", })


@permission_required('operadores.georef_admin')
def delete_ubicacion(request, ubicacion_id):
    ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
    # if is_related(ubicacion):
    #     return render(request, 'extras/error.html', {
    #         'titulo': 'Eliminar Ubicacion',
    #         'error': "La Ubicacion no puede ser borrada pues es Clave de Otros Registros, Contacte al Administrador.",
    #     })
    # else:
    ubicacion.delete()
    return redirect('georef:lista_ubicaciones')


@permission_required('operadores.georef_admin')
def geopos_ubicacion(request, ubicacion_id):
    ubicacion = Ubicacion.objects.get(pk=ubicacion_id)
    if request.method == "POST":
        # cargamos los datos del form:
        ubicacion.latitud = request.POST['latitud']
        ubicacion.longitud = request.POST['longitud']
        ubicacion.save()
        return redirect('georef:lista_ubicaciones')
    return render(request, "extras/gmap_form.html", {
        'objetivo': ubicacion,
        'gkey': GEOPOSITION_GOOGLE_MAPS_API_KEY,
    })