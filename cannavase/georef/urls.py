# imports django
from django.conf.urls import url
from django.urls import path
# Import de modulos personales
from . import views
from . import autocomplete

# Definimos nuestros paths
app_name = 'georef'
urlpatterns = [
    path('', views.menu, name='menu'),
    path('submenu', views.submenu, name='submenu_georef'),
    # Administracion
    # Nacionalidad
    # path('nacionalidades', views.lista_nacionalidades, name='lista_nacionalidades'),
    # path('nacionalidad/crear', views.crear_nacionalidad, name='crear_nacionalidad'),
    # path('nacionalidad/mod/<int:nacionalidad_id>', views.crear_nacionalidad, name='mod_nacionalidad'),
    # path('nacionalidad/del/<int:nacionalidad_id>', views.delete_nacionalidad, name='delete_nacionalidad'),
    #
    # path('provincias', views.lista_provincias, name='lista_provincias'),
    # path('provincia/crear', views.crear_provincia, name='crear_provincia'),
    # path('provincia/mod/<int:provincia_id>', views.crear_provincia, name='mod_provincia'),
    # path('provincia/del/<int:provincia_id>', views.delete_provincia, name='delete_provincia'),
    #
    # path('departamentos', views.lista_departamentos, name='lista_departamentos'),
    # path('departamento/crear', views.crear_departamento, name='crear_departamento'),
    # path('departamento/mod/<int:departamento_id>', views.crear_departamento, name='mod_departamento'),
    # path('departamento/del/<int:departamento_id>', views.delete_departamento, name='delete_departamento'),
    #
    # path('localidades', views.lista_localidades, name='lista_localidades'),
    # path('localidad/crear', views.crear_localidad, name='crear_localidad'),
    # path('localidad/mod/<int:localidad_id>', views.crear_localidad, name='mod_localidad'),
    # path('localidad/del/<int:localidad_id>', views.delete_localidad, name='delete_localidad'),
    #
    # path('barrios', views.lista_barrios, name='lista_barrios'),
    # path('barrio/crear', views.crear_barrio, name='crear_barrio'),
    # path('barrio/mod/<int:barrio_id>', views.crear_barrio, name='mod_barrio'),
    # path('barrio/del/<int:barrio_id>', views.delete_barrio, name='delete_barrio'),
    #
    # path('ubicaciones', views.lista_ubicaciones, name='lista_ubicaciones'),
    # path('ubicaciones/tipo/<str:tipo>', views.lista_ubicaciones, name='lista_ubicaciones_filtradas'),
    # path('ubicacion/crear', views.crear_ubicacion, name='crear_ubicacion'),
    # path('ubicacion/mod/<int:ubicacion_id>', views.crear_ubicacion, name='mod_ubicacion'),
    # path('ubicacion/copy/<int:ubicacion_id>', views.copiar_ubicacion, name='copiar_ubicacion'),
    # path('ubicacion/del/<int:ubicacion_id>', views.delete_ubicacion, name='delete_ubicacion'),
    # path('ubicacion/geopos/<int:ubicacion_id>', views.geopos_ubicacion, name='geopos_ubicacion'),
    # path('ubicacion/ver/<int:ubicacion_id>', views.ver_ubicacion, name='ver_ubicacion'),
    #
    # path('sububicaciones', views.lista_sububicaciones, name='lista_sububicaciones'),
    # path('sububicacion/crear', views.crear_sububicacion, name='crear_sububicacion'),
    # path('sububicacion/mod/<int:ubicacion_id>', views.crear_sububicacion, name='mod_sububicacion'),
    #
    # Autocompleteviews
    url(r'^nacionalidad-autocomplete/$', autocomplete.NacionalidadAutocomplete.as_view(),
        name='nacionalidad-autocomplete', ),
    url(r'^provincia-autocomplete/$', autocomplete.ProvinciaAutocomplete.as_view(), name='provincia-autocomplete', ),
    url(r'^departamento-autocomplete/$', autocomplete.DepartamentoAutocomplete.as_view(),
        name='departamento-autocomplete', ),
    url(r'^localidad-autocomplete/$', autocomplete.LocalidadAutocomplete.as_view(), name='localidad-autocomplete', ),
    url(r'^barrio-autocomplete/$', autocomplete.BarrioAutocomplete.as_view(), name='barrio-autocomplete', ),
    url(r'^predio-autocomplete/$', autocomplete.PredioAutocomplete.as_view(), name='predioAutocomplete', ),
    url(r'^instalacion-autocomplete/$', autocomplete.InstalacionAutocomplete.as_view(), name='instalacionAutocomplete', ),
    url(r'^division-autocomplete/$', autocomplete.DivisionAutocomplete.as_view(), name='divisionAutocomplete', ),
    url(r'^almacen-autocomplete/$', autocomplete.AlmacenAutocomplete.as_view(), name='almacenAutocomplete', ),
    url(r'^recipiente-autocomplete/$', autocomplete.RecipienteAutocomplete.as_view(), name='recipienteAutocomplete', ),

    # Predio
    path('predios', views.lista_predio, name='lista_predio'),
    path('predios/crear', views.crear_predio, name='crear_predio'),
    path('predios/mod/<int:predio_id>', views.crear_predio, name='mod_predio'),
    path('predios/del/<int:predio_id>', views.delete_predio, name='delete_predio'),

    # Instalaciones
    path('instalaciones', views.lista_instalaciones, name='lista_instalaciones'),
    path('instalaciones/crear', views.crear_instalacion, name='crear_instalacion'),
    path('instalaciones/mod/<int:instalacion_id>', views.crear_instalacion, name='mod_instalacion'),
    path('instalaciones/del/<int:instalacion_id>', views.delete_instalacion, name='delete_instalacion'),

    # Divisiones
    path('divisiones', views.lista_divisiones, name='lista_divisiones'),
    path('divisiones/crear', views.crear_division, name='crear_division'),
    path('divisiones/mod/<int:division_id>', views.crear_division, name='mod_division'),
    path('divisiones/del/<int:division_id>', views.delete_division, name='delete_division'),

    # Almacenes
    path('almacenes', views.lista_almacenes, name='lista_almacenes'),
    path('almacenes/crear', views.crear_almacen, name='crear_almacen'),
    path('almacenes/mod/<int:almacen_id>', views.crear_almacen, name='mod_almacen'),
    path('almacenes/del/<int:almacen_id>', views.delete_almacen, name='delete_almacen'),

    # Recipientes
    path('recipientes', views.lista_recipientes, name='lista_recipientes'),
    path('recipientes/crear', views.crear_recipiente, name='crear_recipiente'),
    path('recipientes/mod/<int:recipiente_id>', views.crear_recipiente, name='mod_recipiente'),
    path('recipientes/del/<int:recipiente_id>', views.delete_recipiente, name='delete_recipiente'),
]
