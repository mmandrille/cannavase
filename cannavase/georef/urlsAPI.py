﻿from django.urls import path
from .views import *

urlpatterns = [
    # Predio
    path('getPrediosList', PredioList.as_view()),
    path('postPredio', PredioCreate.as_view()),
    path('putPredio', PredioUpdate.as_view()),
    path('deletePredio', PredioDelete.as_view()),

    # Instalaciones
    path('getInstalacionesList', InstalacionList.as_view()),
    path('postInstalacion', InstalacionCreate.as_view()),
    path('putInstalacion', InstalacionUpdate.as_view()),
    path('deleteInstalacion', InstalacionDelete.as_view()),

    # Divisiones
    path('getDivisionesList', DivisionList.as_view()),
    path('postDivision', DivisionCreate.as_view()),
    path('putDivision', DivisionUpdate.as_view()),
    path('deleteDivision', DivisionDelete.as_view()),

    # Almacenes
    path('getAlmacenesList', AlmacenList.as_view()),
    path('postAlmacen', AlmacenCreate.as_view()),
    path('putAlmacen', AlmacenUpdate.as_view()),
    path('deleteAlmacen', AlmacenDelete.as_view()),

    # Recipientes
    path('getRecipientesList', RecipienteList.as_view()),
    path('postRecipiente', RecipienteCreate.as_view()),
    path('putRecipiente', RecipienteUpdate.as_view()),
    path('deleteRecipiente', RecipienteDelete.as_view()),
]
