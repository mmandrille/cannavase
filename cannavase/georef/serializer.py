﻿from rest_framework import serializers
from .models import Predio, Instalacion, Division, Almacen, Recipiente


class PredioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Predio
        fields = '__all__'


class InstalacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instalacion
        fields = '__all__'


class DivisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Division
        fields = '__all__'


class AlmacenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Almacen
        fields = '__all__'


class RecipienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipiente
        fields = '__all__'
