TIPO_UBICACION = (
    ('FIN', 'Finca'),
    ('LAB', 'Laboratorio'),
    ('OFI', 'Oficinas'),
    ('DEP', 'Deposito'),
    ('CON', 'Contenedor'),
    #Plantables:
    ('BDJ', 'Bandeja - Germinacion'),
    ('BDM', 'Bandeja / Maceta'),
    ('MAC', 'Maceta'),
    ('INV', 'Invernadero'),
    ('IND', 'Suelo - Indoor'),
    ('OUT', 'Suelo - OutDoor'),
    ('PAR', 'Parcela'),
)

TIPO_SUBUBICACION = (
('BDJ', 'Bandeja - Germinacion'),
    ('BDM', 'Bandeja / Maceta'),
    ('MAC', 'Maceta'),
    ('DEP', 'Deposito'),
    ('INV', 'Invernadero'),
    ('IND', 'Suelo - Indoor'),
    ('OUT', 'Suelo - OutDoor'),
    ('PAR', 'Parcela')
)

tipos_ubicaciones = ['FIN', 'LAB', 'OFI', 'DEP', 'CON']
tipos_sububicaciones = ['BDJ', 'BDM', 'MAC', 'INV', 'IND', 'OUT', 'PAR']

TIPO_CONDICION = (
    ('TMP', 'Temperatura'),
    ('LUM', 'Luminocidad'),
    ('HUM', 'Humedad'),
    ('PRE', 'Presion Atmosferica'),
    ('ALT', 'Altitud'),
)

TIPO_PREDIO = (
    ('FIN', 'Finca'),
)

TIPO_INSTALACION = (
    ('LAB', 'Laboratorio'),
    ('OFI', 'Oficinas'),
    ('INV', 'Invernadero'),
    ('CAM', 'Campo'),
)

TIPO_DIVISION = (
    ('DEP', 'Deposito'),
    ('IND', 'Suelo - Indoor'),
    ('OUT', 'Suelo - OutDoor'),
    ('PAR', 'Parcela'),
    ('LCA', 'Lote de Campo'),
    ('HAB', 'Habitación / Habiente'),
    ('SAL', 'Sala'),
    ('SEC', 'Seccion'),
)

TIPO_ALMACEN = (
    ('HEL', 'Heladera'),
)

TIPO_RECIPIENTE = (
    ('BDJ', 'Bandeja - Germinacion'),
    ('BDM', 'Bandeja / Maceta'),
    ('COT', 'Contenedor'),
    ('COH', 'Contenedor hermetico'),
    ('MAC', 'Maceta'),
    ('VAC', 'Bolsa al Vacio')
)
