#Imports django
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import *

class NacionalidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Nacionalidad.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs

class ProvinciaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Provincia.objects.all()
        #Si hay nacion en el form, usamos filtro:
        nacion = self.forwarded.get('nacion', None)
        if nacion:
            qs = qs.filter(nacion=nacion)
        #Usamos texto
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs

class DepartamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Departamento.objects.all()
        #Si hay localidad en el form, usamos filtro:
        provincia = self.forwarded.get('provincia', None)
        if provincia:
            qs = qs.filter(provincia=provincia)
        #Usamos texto
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs

class LocalidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Localidad.objects.all()
        #Si hay localidad en el form, usamos filtro:
        departamento = self.forwarded.get('departamento', None)
        if departamento:
            qs = qs.filter(departamento=departamento)
        #Usamos texto
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs

class BarrioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Barrio.objects.all()
        localidad = self.forwarded.get('localidad', None)
        #Si hay localidad en el form, usamos filtro:
        if localidad:
            qs = qs.filter(localidad=localidad)
        #Usamos texto
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs


class PredioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Predio.objects.all()
        qs = qs.order_by('nombre')
        return qs


class InstalacionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Instalacion.objects.all()
        predio = self.forwarded.get('predio', None)
        if predio:
            qs = qs.filter(predio=predio)
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs


class DivisionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Division.objects.all()
        instalacion = self.forwarded.get('instalacion', None)
        if instalacion:
            qs = qs.filter(instalacion=instalacion)
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs


class AlmacenAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Almacen.objects.all()
        division = self.forwarded.get('division', None)
        if division:
            qs = qs.filter(division=division)
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        qs = qs.order_by('nombre')
        return qs


class RecipienteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Recipiente.objects.all()
        qs = qs.order_by('nombre')
        return qs